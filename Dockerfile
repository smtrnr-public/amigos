# todo: optimize the final size of the image
# https://phoenixnap.com/kb/docker-image-size

# https://github.com/tianon/docker-brew-ubuntu-core/blob/74249faf47098bef2cedad89696bfd1ed521e019/bionic/Dockerfile
# https://hub.docker.com/_/ubuntu?tab=description&page=1&ordering=last_updated

# Ubuntu, 18.04 Bionic Beaver, match lab machine version
FROM ubuntu:18.04

# author(s)
MAINTAINER Kevin and Sunny

# install additional dependencies
RUN apt-get update && apt-get install -y \ 

# git, gcc, g++ for recompiling OS repository at runtime
    git gcc g++ gcc-7-multilib g++-7-multilib time \

# wget, xz-utils, pkg-config required for downloading QEMU source code
    wget xz-utils \

# more dependencies for QEMU build script
   pkg-config libglib2.0-dev libpixman-1-dev \

# python3 and pip required for compiling QEMU source code
    python3 python3-pip && pip3 install ninja \

# clean up cache
&& rm -rf /var/lib/apt/lists/*

# install QEMU 5.1.0 from source, since Linux 18.04 is prepackaged only with version 2.11.1
# https://www.qemu.org/download/#source
# keep as separate cache because this LITERALLY TAKES TWO AND A HALF HOURS.

RUN wget https://download.qemu.org/qemu-5.1.0-rc3.tar.xz && \
    tar xvJf qemu-5.1.0-rc3.tar.xz && \
    cd qemu-5.1.0-rc3 && \
    ./configure --target-list=i386-softmmu && \
    make install

# add QEMU 5.1.0 installation to path
RUN export PATH=$PATH:/qemu-5.1.0-rc3/i386-softmmu/

CMD ["/bin/bash"]