#include "libc.h"

void one(int fd) {
    printf("*** fd = %d\n",fd);
    printf("*** len = %d\n",len(fd));

    cp(fd,2);
}

int main(int argc, char** argv) {
    // Opening the file and getting data
    printf("*** Testing basic overwriting:\n");
    int fd = open("/etc/data.txt",0);
    uint32_t data_len = len(fd);

    //Reading what's already there
    char* buffer = (char*)malloc(data_len + 1);
    buffer[data_len + 1] = '\0';
    read(fd, buffer, data_len);
    printf("Starting file:\n%s", buffer);
    seek(fd, 35);

    // Modifying the end
    char* tmp = "6 days!";
    write(fd, tmp, 7);
    seek(fd, 0);

    // Modifying the start
    read(fd, buffer, data_len);
    printf("*** Rewritten file (1):\n%s", buffer);
    tmp = "Shingeki no Kyojin";
    seek(fd, 4);
    write(fd, tmp, 18);
    seek(fd, 0);
    read(fd, buffer, data_len);
    printf("*** Rewritten file (2):\n%s", buffer);
    unlink("../etc/data.txt");
    fd = open("/etc/data.txt",0);
    printf("Should be -1: %d\n", fd);
    shutdown();
    return 0;
}
