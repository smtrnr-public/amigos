# cs439_f20_final_project
Final Project Repo for CS439: Principles of Computer Systems Fall 2020

### Sockets

If you're using sockets, here's some information for you about the API the networking team constructed.

Firstly, we used the following system calls:
* [socket](https://man7.org/linux/man-pages/man2/socket.2.html)
* [connect](https://man7.org/linux/man-pages/man2/connect.2.html)
* [accept](https://man7.org/linux/man-pages/man2/accept.2.html)
* [sendto](https://man7.org/linux/man-pages/man2/sendto.2.html)
* [recvfrom](https://man7.org/linux/man-pages/man2/recvfrom.2.html)
* [bind](https://man7.org/linux/man-pages/man2/bind.2.html)
* [listen](https://man7.org/linux/man-pages/man2/listen.2.html)
* getMAC (pass a m_addr buffer as a parameter and call to be filled with the 6 hexadecimal mac address values)

**Note that other functions in other standard libraries are not implemented.** More information on these syscalls can be found on the excel spreadsheet in the Google Drive.

### Servers & Client
This part is a little bit different from what we initially planned, but it doesn't change too much stuff. Since the lab machines don't give us sudo access, we'll have use LAN networks aka ethernet. For consistency, we've been saying that the server's mac address will always be located at 52:54:00:12:34:51. Therefore, if we were on the client side, we would do something like this (you can look into the "sockets.dir/sbin/init.c" file for more details):
```c
// metadata information that the socket syscalls need
struct sockaddr {
   unsigned short sa_family;
   char sa_data[14];
};

// easy way to view mac addresses without dealing with endianness
struct m_addr {
    uint8_t smac_addr[6];
};

// specific type of sockaddr being a mac address
struct sockaddr_mac {
    int16_t smac_family;
    uint16_t smac_port;
    struct m_addr smac_addr;
    uint8_t smac_zero[6];
};

int main(int argc, char** argv) {
    // this isn't used but in case you need it
    //getMAC(&my_mac);	// gets the entire mac address
    //uint8_t client_id = my_mac.smac_addr[5] & 0xf;	// gets client id (basically the number you put in the terminal)

    char* str = "hello world!";
    int sock = socket(0,0,0);   // get a socket descriptor	
    struct sockaddr_mac* addr = textToMac("52:54:00:12:34:51");    //mac address of the server
    int len = strlen(str);  // get the length of the string so we know how many bytes this "text message" was and then we can malloc the right amount in the server

    // 4 bytes for the length of the message + the message itself
    struct message* msg = (struct message*) malloc(len + sizeof(uint32_t));
    memcpy(&msg->message,str, len + 1);     // copy the message
    msg->len = strlen(str) + 1;             // + 1 is very important!!! need the null terminating character!!!

    // finally, send the information over
    sendto(sock, (void*) msg, strlen(str) + 1 + 4, 0, (struct sockaddr*) addr, 0);
}
```
In terms of the server code, an example would be the following:
```c
int main(int argc, char** argv) {
    int sock = socket(0,0,0);   // get a socket descriptor
    struct sockaddr_mac* addr = textToMac("52:54:00:12:34:52");    // mac address of the client we wanna get information from
    bind(sock, (const struct sockaddr*) addr, 0);  // says yo homie we want information from this mac address

    char* buffer = (char*) malloc(4);                   // make a buffer that's four bytes long
    recvfrom(sock, (void*) buffer, 4, 0, NULL, 0);      // read the length data that we sent in

    int len = *(int*)buffer;    // the length of the text message that was sent 
    char* buffer2 = (char*) malloc(len - 4);    // malloc enough space  
    recvfrom(sock, (void*) buffer2, len - 4, 0, NULL, 0);   // get the rest of the text message

    printf("buffer (%d): %s\n", len - 4, buffer2);
}
```


If you would like to run the server, you can do the following:
```
$ VM_ID=1 ./run_qemu server
```


If you would like to run any other client, you can do the following:
```
$ VM_ID=SOME_NUMBER_OTHER_THAN_1 ./run_qemu sockets
```
and the client will be located at the mac address 52:54:00:12:34:5<SOME_NUMBER_OTHER_THAN_1>. If you're running this locally, you can simply call on these two commands in two different terminals. If you're working on a lab machine, unfortunately, you will have to have at least two different directories (for examples, "amigos", "amigos2", "amigos3", etc where the first command is run for the amigos and the others are all clients with different mac addresses).

Just like our last project, don't forget to recompile your user directory if you make any changes! If you would like a shortcut, you can the following command:
```
$ cd <testname>.dir/sbin && make && cd ../.. && VM_ID=<ID> ./run_qemu <testname>
```

If you would like examples of the code being used in the server and client side, please view the "server.dir/sbin/init.c" and "sockets.dir/sbin/init.c" files respectively. Let us know if you have any questions and don't forget to be awesome!
