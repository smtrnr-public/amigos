#include "libc.h"
#include "sys.h"

#define NULL 0

int strlen(const char* str) {
    int len = 0;
    while (str[len]) {
        len++;
    }
    return len;
}

uint8_t getByte(char* text) {
    uint8_t res = 0;
    if ('0' <= text[0] && text[0] <= '9') {
        res = text[0] - 0x30;
    } else if ('A' <= text[0] && text[0] <= 'F') {
        res = text[0] - 0x37;
    } else if ('a' <= text[0] && text[0] <= 'f') {
        res = text[0] - 0x57;
    } else {
        printf("invalid MAC address\n");
    }
    res <<= 4;
    if ('0' <= text[1] && text[1] <= '9') {
        res |= text[1] - 0x30;
    } else if ('A' <= text[1] && text[1] <= 'F') {
        res |= text[1] - 0x37;
    } else if ('a' <= text[1] && text[1] <= 'f') {
        res |= text[1] - 0x57;
    } else {
        printf("invalid MAC address\n");
    }
    return res;
}

struct sockaddr_mac* textToMac(char* text) {
    struct sockaddr_mac* res = (struct sockaddr_mac*) malloc(sizeof(struct sockaddr_mac));
    int i = 0;
    int j = 0;
    while (i < 6) {
        res->smac_addr.smac_addr[i] = getByte(text + j);
        i++;
        j+=2;
        if (text[j] == ':') {
            j++;
        } else if (i != 6 && !text[j]) {
            printf("invalid MAC address\n");
        }
    }
    if (text[j]) {
        uint16_t port = getByte(text + j);
        port <<= 8;
        port |= getByte(text + j + 2);
        res->smac_port = port;
    } else {
        res->smac_port = 0;
    }
    return res;
}

struct message {
    uint32_t len;
    char message[];
} __attribute__((packed));

int main(int argc, char** argv) {
    int is_server = 0;

    char* str = "hello world!";

    if (is_server) {
        int sock = socket(0,0,0);
        struct sockaddr_mac* addr;
        // char mac[6] = {0x52,0x54,0x00,0x12,0x34,0x52};
        addr = textToMac("52:54:00:12:34:52");
        //memcpy(&addr.smac_addr, mac, 6);
        bind(sock, (const struct sockaddr*) &addr, 0);
        //while (1) {
            //int toExpect = 0;
            //int num = recvfrom(sock, (void*)&toExpect, 4, 0, (struct sockaddr*) &addr, 0);

            //printf("num: %d toExpect: %x\n", num, toExpect);
            //if (num == 4 && toExpect > 0) {
                //printf("in if statement\n");
                //char* buf = (char*) malloc(toExpect);
                //recvfrom(sock, buf, toExpect, 0, (struct sockaddr*) &addr, 0);
                //printf("buffer: %s\n", buf);
            //}

        //}
        char* buffer = (char*) malloc(strlen(str) + 1 + 4);
        recvfrom(sock, (void*) buffer, strlen(str) + 1 + 4, 0, NULL, 0);
        int len = *(int*)buffer;
        printf("buffer (%d): %s\n", len, buffer + 4);
        for (int i = 0; i < strlen(str) + 1 + 4; i++) {
            printf("%x ", buffer[i]);
        }
        printf("\n");
    } else {
        int sock = socket(0,0,0);
        struct sockaddr_mac* addr;
        //char mac[6] = {0x52,0x54,0x00,0x12,0x34,0x51};
        addr = textToMac("52:54:00:12:34:51");
        //memcpy(&addr.smac_addr, mac, 6);
        // printf("addr port: %d\n", addr->smac_port);
        //char* str = "This is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please workThis is a test packet please work";
        int len = strlen(str);
        // char* buf = malloc(len + 4);
        // *((int*) buf) = len;

        struct message* msg = (struct message*) malloc(len + sizeof(uint32_t));
        memcpy(&msg->message,str, len + 1);
        msg->len = strlen(str) + 1;
        // msg->len = ~0;

        for (int i =0 ; i < 10; i++) {
            printf("%x ", ((char*)msg)[i]);
        }
        printf("\n");

        printf("string bytes %u\n", strlen(str));
        sendto(sock, (void*) msg, strlen(str) + 1 + 4, 0, (struct sockaddr*)addr, 0);
        // sendto(sock, buffer, len, 0, (struct sockaddr*) &addr, 0);
        //buffer = "goodbye world. this is a very long string. this sentence is making it even longe";
        //sendto(sock, buffer, 81, 0, (struct sockaddr*) &addr, 0);

        while (1);
    }

    shutdown();
}
