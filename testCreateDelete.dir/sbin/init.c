#include "libc.h"

void createCopy(const char* from, const char* to) {
    int fd = open(to, 0);
    printf("*** File fd before creation: %d\n", fd);
    // create file
    fd = open(to, 1);
    printf("*** File fd after creation: %d\n", fd);
    printf("*** Before writing to new file:\n");
    printf("*** The file is : %d bytes long\n", len(fd));
    // open file to read from
    int read_fd = open(from, 0);
    int read_len = len(read_fd);
    // copy file to the new file
    char* buffer = (char*)malloc(read_len);
    read(read_fd, buffer, read_len);
    write(fd, buffer, read_len);
    close(fd);
    // reopen the file (without creation flag)
    open(to, 0);
    printf("*** After writing to new file:\n");
    printf("*** The file is : %d bytes long\n", len(fd));
    free(buffer);
    cp(fd, 2);
}

void delete(const char* path) {
    // open file
    int fd = open(path, 0);
    printf("*** Before deleting:\n");
    printf("*** The file's fd is : %d\n", fd);
    // delete file
    unlink(path);
    close(fd);
    // try to reopen deleted file (should fail)
    fd = open(path, 0);
    printf("*** After deleting:\n");
    printf("*** The file's fd is : %d\n", fd);
}

int main(int argc, char** argv) {
    printf("*** TEST 1: Short Filename\n");
    // create file (short filename) with relative path
    createCopy("/etc/panic.txt", "../etc/newFile.txt");
    // delete file with absolute path
    delete("/etc/newFile.txt");
    printf("*** TEST 2: Long Filename\n");
    // create file (long filename) with absolute path
    createCopy("../etc/data.txt", "/qaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaz.txt");
    // delete file (long filename) with relative path
    delete("../qaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaz.txt");
    printf("*** Done!\n");
    shutdown();
    return 0;
}
