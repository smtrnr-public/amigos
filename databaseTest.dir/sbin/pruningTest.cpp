#include "database.h"
extern "C" {
    #include "libc.h"
}

// Shaharyar

// I think this method should be right, but not used rn

// bool writeToDb(bool stored, database::Database db, bool deep) {
//     using namespace database;
//     stored =  db.store('T', (char*) "Alice", (char*) "Hey Bob!");
//     stored &= db.store('T', (char*) "Bob",   (char*) "Hey Alice! How's it going?!");
//     stored &= db.store('T', (char*) "Alice", (char*) "Nothin much just chillin.");
//     stored &= db.store('T', (char*) "Bob",   (char*) "Omg I wish bruh I got so much stuff to do.");
//     stored &= db.store('T', (char*) "Alice", (char*) "Its ight just finish the semester strong bro.");
//     stored &= db.store('T', (char*) "Bob",   (char*) "I'm trying.");
//     stored &= db.store('T', (char*) "Alice", (char*) "Hey! We'll be done with OS so soon!");
//     if (deep) {
//         stored &= db.store('I', (char*) "Bob", (char*) "/files/img123.jpeg");
//     }
//     stored &= db.store('T', (char*) "Bob",   (char*) "Thank goodness Gheith taught well or I'd have to retake it.");
//     stored &= db.store('T', (char*) "Alice", (char*) "I know bruh he's so good! Imma miss him.");
//     return stored;

// }

extern "C" int main (int argc, char **argv) {
    using namespace database;

    // Database
    Database db{"/files/pruneTest.db"};

    // 0 messages
    uint32_t numPruned = db.prune(20, false);
    numPruned |= db.prune(20, true); // deep prune

    if (numPruned != 0) {
        printf("*** Failed empty database pruning test:\nExpected: %d\nActual: %d\n", 0, numPruned);
    }
    else
        printf("Passed empty database pruning test!\n");

    // less messages

    // bool stored = true;
    // stored = writeToDb(stored, db, false);

    bool stored = db.store('T', (char*) "Alice", (char*) "Hey Bob!");
    stored &= db.store('T', (char*) "Bob",   (char*) "Hey Alice! How's it going?!");
    stored &= db.store('T', (char*) "Alice", (char*) "Nothin much just chillin.");
    stored &= db.store('T', (char*) "Bob",   (char*) "Omg I wish bruh I got so much stuff to do.");
    stored &= db.store('T', (char*) "Alice", (char*) "Its ight just finish the semester strong bro.");
    stored &= db.store('T', (char*) "Bob",   (char*) "I'm trying.");
    stored &= db.store('T', (char*) "Alice", (char*) "Hey! We'll be done with OS so soon!");
    stored &= db.store('T', (char*) "Bob",   (char*) "Thank goodness Gheith taught well or I'd have to retake it.");
    stored &= db.store('T', (char*) "Alice", (char*) "I know bruh he's so good! Imma miss him.");

    if (!stored) {
        printf("*** Failed storing messages test.\n");
    }

    numPruned = db.prune(20, false);
    // change according to num messages placed above
    if (numPruned != 9) {
        printf("*** Failed pruning more messages than database has test:\nExpected: %d\nActual: %d\n", 9, numPruned);
    } 

    printf("Passed pruning more messages than in database test!\n");

    // exact # of messages
    stored = true;

    // stored = writeToDb(stored, db, false);

    stored  = db.store('T', (char*) "Alice", (char*) "Hey Bob!");
    stored &= db.store('T', (char*) "Bob",   (char*) "Hey Alice! How's it going?!");
    stored &= db.store('T', (char*) "Alice", (char*) "Nothin much just chillin.");
    stored &= db.store('T', (char*) "Bob",   (char*) "Omg I wish bruh I got so much stuff to do.");
    stored &= db.store('T', (char*) "Alice", (char*) "Its ight just finish the semester strong bro.");
    stored &= db.store('T', (char*) "Bob",   (char*) "I'm trying.");
    stored &= db.store('T', (char*) "Alice", (char*) "Hey! We'll be done with OS so soon!");
    stored &= db.store('T', (char*) "Bob",   (char*) "Thank goodness Gheith taught well or I'd have to retake it.");
    stored &= db.store('T', (char*) "Alice", (char*) "I know bruh he's so good! Imma miss him.");

    numPruned = db.prune(9, false);

    if (!stored || numPruned != 9) {
        printf("*** Failed pruning exactly the number of messages in DB:\nExpected: %d\nActual: %d", 9, numPruned);
    }

    printf("Passed pruning exactly the # of messages than in database test!\n");

    // more messages
    stored = true;

    // stored = writeToDb(stored, db, false);

    stored  = db.store('T', (char*) "Alice", (char*) "Hey Bob!");
    stored &= db.store('T', (char*) "Bob",   (char*) "Hey Alice! How's it going?!");
    stored &= db.store('T', (char*) "Alice", (char*) "Nothin much just chillin.");
    stored &= db.store('T', (char*) "Bob",   (char*) "Omg I wish bruh I got so much stuff to do.");
    stored &= db.store('T', (char*) "Alice", (char*) "Its ight just finish the semester strong bro.");
    stored &= db.store('T', (char*) "Bob",   (char*) "I'm trying.");
    stored &= db.store('T', (char*) "Alice", (char*) "Hey! We'll be done with OS so soon!");
    stored &= db.store('T', (char*) "Bob",   (char*) "Thank goodness Gheith taught well or I'd have to retake it.");
    stored &= db.store('T', (char*) "Alice", (char*) "I know bruh he's so good! Imma miss him.");

    if (!stored) {
        printf("*** Failed storing messages test: possibly non-text messages.\n");
    }

    numPruned = db.prune(5, true);
    // change according to num messages placed above
    if (numPruned != 5) {
        printf("*** Failed pruning less messages than in DB:\nExpected: %d\nActual: %d\n", 5, numPruned);
    }
    else
        printf("Passed pruning less messages than in database test!\n");

    // deep prune - cache - print instead of remove right now
        // rewrite to db
    stored = true;

    // stored = writeToDb(stored, db, true);

    //make an image ooboy
    int imgFD = open("/files/img123.jpeg", 1); //the following line is 256 bytes of glorious bark
    const char *IMAGE = "barkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbarkbark";
    write(imgFD, (char*)IMAGE, 256);
    close(imgFD);


    stored  = db.store('T', (char*) "Alice", (char*) "Hey Bob!");
    stored &= db.store('T', (char*) "Bob",   (char*) "Hey Alice! How's it going?!");
    stored &= db.store('T', (char*) "Alice", (char*) "Nothin much just chillin.");
    stored &= db.store('T', (char*) "Bob",   (char*) "Omg I wish bruh I got so much stuff to do.");
    stored &= db.store('T', (char*) "Alice", (char*) "Its ight just finish the semester strong bro.");
    stored &= db.store('T', (char*) "Bob",   (char*) "I'm trying.");
    stored &= db.store('T', (char*) "Alice", (char*) "Hey! We'll be done with OS so soon!");
    stored &= db.store('I', (char*) "Bob",   (char*) "/files/img123.jpeg");
    stored &= db.store('T', (char*) "Bob",   (char*) "Thank goodness Gheith taught well or I'd have to retake it.");
    stored &= db.store('T', (char*) "Alice", (char*) "I know bruh he's so good! Imma miss him.");

    if (!stored) {
        printf("*** Failed storing messages test: possibly non-text messages.\n");
    }

    numPruned = db.prune(20, true);

    // change according to num messages placed above
    if (numPruned != 14) { // add deepprune line
        printf("*** Failed deep pruning test (possibly cache error):\nExpected: %d\nActual: %d\n", 14, numPruned);
    }
    puts("End of pruning test - if you didn't see any \"fail\" messages you passed! uwu\n");
    return 0;
}