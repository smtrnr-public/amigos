#include "database.h"
extern "C" {
    #include "libc.h"
}

/*
 * This tests the constructor and destructor of the Database class
 * 
 * @author Colette
 */

#define HALF_SIZE 1000
#define FULL_SIZE 2000

const char* magicString = "DATABASES_ARE_COOL";

void readHeader(const char* path) {
    using namespace database;
    int fd = open(path, 0);
    db_data header {}; 
    read(fd, &header, DATA_SZ);
    printf("HEADER for %s (fd = %d): size = %u, offset = %u\n", path, fd, header.size, header.time);
    close(fd);
}

bool makeDBs(const char* path, const char* user, const char* msg, int n) { //pls dont pass 1000 ty
    using namespace database;
    Database db{path};
    for (int i = 0; i < n; ++i) {
        char dummyMsg[25];
        memcpy(dummyMsg, (char*)msg, 21);
        dummyMsg[21] = (char)(48 + i / 100);
        dummyMsg[22] = (char)(48 + (i / 10) % 10);
        dummyMsg[23] = (char)(48 + i % 10);
        dummyMsg[24] = '\0';
        if(!db.store('T', (char*)user, dummyMsg))
            return false;
    }
    return true;
}

bool store(const char* path, const char* user, const char* msg) {
    using namespace database;
    Database db{path};
    // printf("address of db is at %p, path is %s\n", &db, db.getPath());
    if(!db.store('T', (char*)user, (char*)msg)) {
        printf(" ^^^^ Error when trying to write to %s\n", path);
        return false;
    }
    return true;
}

void openAndClose(const char* path) {
    using namespace database;
    volatile Database db{path};
}

void getLatestMsg(const char* path) {
    using namespace database;
    Database db{path};
    auto msg = db.queryLatest(1);
    for(uint32_t i = 0; i < msg[0].size; i++)
        putchar(msg[0].message[i]);
    puts("\n");
    free(msg);
}


// Will return a database if the path and file are valid, otherwise calls exit
int open_db(const char* path) {
    openAndClose(path);
    auto db_file = open(path, 0);
    char buf[19];
    seek(db_file, 1);
    read(db_file, buf, 19);
    if (strcmp(buf, magicString) != 0) {
        // printf("CMP: %d\n", strcmp(buf, magicString)); // If needed
        printf("FAILED - db header was incorrect in open db :c\n");
        for (int i = 0; i < 19; ++i) {
            putchar(buf[i]);
        }
        printf("\n");
        exit(-1);
    }
    seek(db_file, 0);
    return db_file;
}


extern "C" int main (int argc, char **argv) {
    using namespace database;
    
    // *** CONSTRUCTOR TESTS ***
    printf("*** Starting constructor tests\n");

    // printf("*** Test 0: Missing file test\n");

    // Open an empty database file
    printf("*** Test 1: Empty database test ... ");
    int db_1 = open_db("/files/empty.db");
    db_data header_1{}; 
    read(db_1, &header_1, DATA_SZ);

    if (header_1.size != 0) { // make sure num_entries (size) is 0
        printf("FAILED - size of file is incorrect :c size was %d\n", header_1.size);
        exit(-1);
    }
    if (header_1.time != DATA_SZ) { // make sure offset (time) is at DATA_SZ
        printf("FAILED - offset is incorrect :c offset = %d\n", header_1.time);
        exit(-1);
    }
    printf("PASSED c:\n");
    close(db_1);

    // // Open a partially full database file
    printf("*** Test 2: Half full database test ... ");
    if(!makeDBs("/files/half.db", "dog", "barkabarkakaark awooo", HALF_SIZE)) // <-------
        puts("FAILED to make half.db\n");
    // if(!makeDBs("/files/half.db", "rat", "pspspspspspssppsp reee", 250)) // <-------
    //     puts("FAILED to make half.db\n");
    int db_2 = open_db("/files/half.db");
    
    db_data header_2{}; 
    read(db_2, &header_2, DATA_SZ);

    if (header_2.size != HALF_SIZE) { // make sure num_entries (size) is 1000
        printf("FAILED - size of file is incorrect :c size was %d\n", header_2.size);
        exit(-1);
    }
    // is this right?
    if (header_2.time != DATA_SZ) { // make sure offset (time) is at DATA_SZ
        printf("FAILED - offset is incorrect :c offset was %d\n", header_2.time);
        exit(-1);
    }

    printf("PASSED c:\n");
    close(db_2);

    // Open a completely full database file
    printf("*** Test 3: Completely full database test ... ");
    if(!makeDBs("/files/full.db", "cat", "mewoomewoewoemw mewwww", FULL_SIZE))
        puts("FAILED to make full.db\n");
    int db_3 = open_db("/files/full.db");
    db_data header_3 {}; 
    read(db_3, &header_3, DATA_SZ);

    if (header_3.size != FULL_SIZE) { // make sure num_entries (size) is 2000
        printf("FAILED - size of file is incorrect :c size was %d\n", header_3.size);
        exit(-1);
    }
    // is this right?
    if (header_3.time != DATA_SZ) { // make sure offset (time) is at DATA_SZ
        printf("FAILED - offset is incorrect :c offset was %d\n", header_3.time);
        exit(-1);
    }
    printf("PASSED c:\n");
    close(db_3);

    // Try to open a normal file as a database
    // printf("*** Test 4: dumby file test ... ");
    // openAndClose("/files/dumby.db"); // exits if wrong
    // printf("PASSED c:\n");

    // Modify each database
    // readHeader("/files/empty.db");
    // readHeader("/files/half.db");
    // readHeader("/files/full.db");
    if (!store("/files/half.db", "dog", "barkabarkakaark awooo")) {
        printf("couldnt write a new thing to half.db :c\n");
    }
    // readHeader("/files/empty.db");
    // readHeader("/files/half.db");
    // readHeader("/files/full.db");
    if (!store("/files/full.db", "cat", "emowmeowmeowmew mewww")) {
        printf("couldnt write a new thing to full.db :c\n");
    }
    // readHeader("/files/empty.db");
    // readHeader("/files/half.db");
    // readHeader("/files/full.db");
    if (!store("/files/empty.db", "car", "vroooooooooooom zooom")) {
        printf("couldnt write a new thing to empty.db :c\n");
    }
    // readHeader("/files/empty.db");
    // readHeader("/files/half.db");
    // readHeader("/files/full.db");

    // *** DESTRUCTOR TESTS ***
    printf("*** Starting destructor tests\n");

    // Close an empty database file
    printf("*** Test 5: empty database test ... ");
    // reopen database and compare meta data to make sure it changed
    int db_5 = open_db("/files/empty.db");
    db_data header_5 {}; 
    read(db_5, &header_5, DATA_SZ);

    if (header_5.size == 0) { // num_entries
        printf("FAILED - size of file wasn't saved properly :c\n");
        exit(-1);
    }
    if (header_5.time != DATA_SZ) { // offset
        printf("FAILED - offset wasn't saved properly :c offset was %d\n", header_5.time);
        exit(-1);
    }
    printf("PASSED c:\n");
    close(db_5);

    // Close a half full database file
    printf("*** Test 6: half full database test ... ");

    // int halfDB = open("/files/half.db", 0);
    // db_data halfHeader{};
    // seek(halfDB, 0);
    // printf("DATA_SZ is %d\n", DATA_SZ);
    // printf("I read %d bytes\n", read(halfDB, &halfHeader, DATA_SZ));
    // printf("num messages is %d\n", halfHeader.size);
    // seek(halfDB, (DATA_SZ)); // +192 for 1000
    // printf("I read %d bytes\n", read(halfDB, &halfHeader, DATA_SZ));
    // close(halfDB);
    // printf("printing message which is %u characters long: ", halfHeader.size);
    // uint32_t theIndex = 0;
    // for(uint32_t i = 0; i < 300; i++) {
    //     putchar(halfHeader.message[i]);
    //     if(theIndex == 0 && halfHeader.message[i] == 'T')
    //         theIndex = i;
    // }
    // puts("\n");
    // printf("The index of T was %u\n", theIndex);
    // getLatestMsg("/files/half.db"); // <--------
    int db_6 = open_db("/files/half.db");
    db_data header_6 {}; 
    read(db_6, &header_6, DATA_SZ);
    // printf("half size: %d\n", header_6.size);
    // printf("offset:    %d\n", header_6.time);
    
    if (header_6.size != HALF_SIZE + 1) {
        printf("FAILED - size of file wasn't saved properly :c size was %d\n", header_6.size);
        exit(-1);
    }
    if (header_6.time != DATA_SZ) {
        printf("FAILED - offset wasn't saved properly :c offset was %d\n", header_6.time);
        exit(-1);
    }
    printf("PASSED c:\n");
    close(db_6);

    // Close a full database file
    printf("*** Test 7: completely full database test ... ");
    // getLatestMsg("/files/full.db"); // <--------
    int db_7 = open_db("/files/full.db");
    db_data header_7{}; 
    read(db_7, &header_7, DATA_SZ);

    if (header_7.size != 1501) { // 1501 because of prune
        printf("FAILED - size of file wasn't correct :c size was %d\n", header_7.size);
        exit(-1);
    }
    // if (header_7.time == DATA_SZ) { // i think it would wrap back around? im not sure
    //     printf("FAILED - offset wasn't saved properly :c\n");
    //     exit(-1);
    // }
    printf("PASSED c:\n");
    close(db_7);

    unlink("/files/empty.db");
    unlink("/files/half.db");
    unlink("/files/full.db");
    
    return 0;
}