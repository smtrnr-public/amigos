#include "database.h"
extern "C" {
    #include "libc.h"
}

/**
 * Tests reading from database; assumes store() and time() works.
 * 
 * @param argc argc
 * @param argv argv
 * 
 * @author Wentao
 */
extern "C" int main (int argc, char **argv) {
    using namespace database;
    printf("\n");
    printf("*** Test readingTest.cpp started. Will take >7 seconds.\n");

    bool containsError = false; // Checks if all tests passed

    // Database is here
    Database db{"/files/readTest.db"};

    // Stores time at certain points
    time_t *t1 = (time_t*) malloc(sizeof(time_t));
    time_t *t2 = (time_t*) malloc(sizeof(time_t));
    time_t *t3 = (time_t*) malloc(sizeof(time_t));

    // Users
    char alice[] = "Alice\0";
    char bob[] = "Bob\0";

    // Messages
    char m1[] = "Hi, I'm Alice!\0";
    char m2[] = "Hi Alice, I'm Bob!\0";
    char m3[] = "Hi Bob, how are you!\0";
    char m4[] = "Terrible! I just failed OS!\0";
    char m5[] = "Oh, no! Maybe you should've listened to me throughout the semester!\0";
    char m6[] = "*sad_noises*\0";

    // Create database and set time points
    time_t t = time(t1);
    while (time(nullptr) == *t1) { // Makes sure that the time for each message is different
        asm volatile ("pause");
    }

    bool status = db.store('T', alice, m1);
    while (time(nullptr) == (*t1) + 1) {
        asm volatile ("pause");
    }

    status &= db.store('T', bob, m2);
    while (time(nullptr) == (*t1) + 2) {
        asm volatile ("pause");
    }

    status &= db.store('T', alice, m3);
    while (time(nullptr) == (*t1) + 3) {
        asm volatile ("pause");
    }

    t |= time(t2);
    while (time(nullptr) == (*t1) + 4) {
        asm volatile ("pause");
    }

    status &= db.store('T', bob, m4);
    while (time(nullptr) == (*t1) + 5) {
        asm volatile ("pause");
    }

    status &= db.store('T', alice, m5);
    while (time(nullptr) == (*t1) + 6) {
        asm volatile ("pause");
    }

    status &= db.store('T', bob, m6);
    while (time(nullptr) == (*t1) + 7) {
        asm volatile ("pause");
    }

    t |= time(t3);

    if (!status || t < 0) { // Checks store() and time()
        containsError = true;
        printf("*** ERROR: Storing or the time system call failed!\n");
    }

    // queryLatest(1)
    db_data* query = db.queryLatest(1);
    if (query->type != 'T' || strcmp(query->user, "Bob") != 0 || strcmp(query->message, "*sad_noises*") != 0) {
        containsError = true;
        printf("*** ERROR: queryLatest(1): Wrong information\n");
    }
    query++; // Should be null terminator
    if (query->type != '\0') {
        containsError = true;
        printf("*** ERROR: queryLatest(1): Null terminator\n");
    }

    // queryLatest(10)
    query = db.queryLatest(10);
    if (query->type != 'T' || strcmp(query->user, "Alice") != 0 || strcmp(query->message, "Hi, I'm Alice!") != 0) {
        containsError = true;
        printf("*** ERROR: queryLatest(10): Wrong information, earliest message\n");
    }
    query+=5; // Should be latest message
    if (query->type != 'T' || strcmp(query->user, "Bob") != 0 || strcmp(query->message, "*sad_noises*") != 0) {
        containsError = true;
        printf("*** ERROR: queryLatest(10): Wrong information, latest message\n");
    }
    query++; // Should be null terminator
    if (query->type != '\0') {
        containsError = true;
        printf("*** ERROR: queryLatest(10): Null terminator\n");
    }

    // queryFrom(*t1, 1)
    query = db.queryFrom(*t1, 1);
    if (query->type != 'T' || strcmp(query->user, "Alice") != 0 || strcmp(query->message, "Hi, I'm Alice!") != 0) {
        containsError = true;
        printf("*** ERROR: queryFrom(*t1, 1): Wrong information\n");
    }
    query++; // Should be null terminator
    if (query->type != '\0') {
        containsError = true;
        printf("*** ERROR: queryFrom(*t1, 1): Null terminator\n");
    }

    // queryFrom(*t1, 10)
    query = db.queryFrom(*t1, 10);
    if (query->type != 'T' || strcmp(query->user, "Alice") != 0 || strcmp(query->message, "Hi, I'm Alice!") != 0) {
        containsError = true;
        printf("*** ERROR: queryFrom(*t1, 10): Wrong information, earliest message\n");
    }
    query+=5; // Should be latest message
    if (query->type != 'T' || strcmp(query->user, "Bob") != 0 || strcmp(query->message, "*sad_noises*") != 0) {
        containsError = true;
        printf("*** ERROR: queryFrom(*t1, 10): Wrong information, latest message\n");
    }
    query++; // Should be null terminator
    if (query->type != '\0') {
        containsError = true;
        printf("*** ERROR: queryFrom(*t1, 10): Null terminator\n");
    }

    // queryFrom(*t2, 1)
    query = db.queryFrom(*t2, 1);
    if (query->type != 'T' || strcmp(query->user, "Bob") != 0 || strcmp(query->message, "Terrible! I just failed OS!") != 0) {
        containsError = true;
        printf("*** ERROR: queryFrom(*t2, 1): Wrong information\n");
    }
    query++; // Should be null terminator
    if (query->type != '\0') {
        containsError = true;
        printf("*** ERROR: queryFrom(*t2, 1): Null terminator\n");
    }

    // queryFrom(t2, 2)
    query = db.queryFrom(*t2, 2);
    if (query->type != 'T' || strcmp(query->user, "Bob") != 0 || strcmp(query->message, "Terrible! I just failed OS!") != 0) {
        containsError = true;
        printf("*** ERROR: queryFrom(*t2, 2): Wrong information, earliest message\n");
    }
    query++;
    if (query->type != 'T' || strcmp(query->user, "Alice") != 0 || strcmp(query->message, "Oh, no! Maybe you should've listened to me throughout the semester!") != 0) {
        containsError = true;
        printf("*** ERROR: queryFrom(*t2, 2): Wrong information, latest message\n");
    }
    query++; // Should be null terminator
    if (query->type != '\0') {
        containsError = true;
        printf("*** ERROR: queryFrom(*t2, 2): Null terminator\n");
    }

    // queryFrom(*t3, 1)
    query = db.queryFrom(*t3, 1);
    if (query->type != '\0') { // Should be null terminator
        containsError = true;
        printf("*** ERROR: queryFrom(*t3, 1): Null terminator\n");
    }

    // Free heap memory used
    free(t1);
    free(t2);
    free(t3);

    t1 = nullptr;
    t2 = nullptr;
    t3 = nullptr;

    // Output
    printf("*** Test readingTest.cpp finished. ");\
    printf(containsError ? "FIX DIS >:C\n" : "No errors were found.\n");

    db.prune(500, false); // Prunes all

    return 0;
}
