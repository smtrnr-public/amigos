extern "C" {
    #include "libc.h"
}

// extern "C" int main(int argc, char** argv);

extern "C" int main(int argc, char** argv) {
    
    //test runner - please replace with server code by server team after pb presentations


    // //make the stinky string
    // int writeFD = open("/files/stress.txt", 0);
    // const char *msg = "barkarkarkarkarkarkarkarkarkbarkarkarkarkarkarkarkarkarkbarkarkarkarkarkarkarkarkarkbarkarkarkarkarkarkarkarkarkbarkarkarkarkarkarkarkarkarkbarkarkarkarkarkarka";
    // for (int i = 0; i < 100; ++i) {
    //     char dummyMsg[164];
    //     seek(writeFD, i * 164);
    //     memcpy(dummyMsg, (char*)msg, 160);
    //     dummyMsg[160] = (char)(48 + i / 100);
    //     dummyMsg[161] = (char)(48 + (i / 10) % 10);
    //     dummyMsg[162] = (char)(48 + i % 10);
    //     dummyMsg[163] = '\0';
    //     auto bytesWritten = write(writeFD, dummyMsg, 164);
    //     if (bytesWritten != 164) {
    //         printf("REEEEEEEE bytes written was %d\n", bytesWritten);
    //     }
    // }
    // //print the entire file :) oh god
    // char ohNoPlsNoDearGodNo[16400];
    // seek(writeFD, 0);
    // read(writeFD, ohNoPlsNoDearGodNo, 16400);
    // for(int i = 0; i < 16400; i++) {
    //     putchar(ohNoPlsNoDearGodNo[i]);
    // }
    // puts("\n");
    
    int id = fork();

    if (id < 0) {
        printf("fork failed");
    } else if (id == 0) {
        /* child */
        printf("*** in child\n");
        int rc = execl("/sbin/constructorTest","constructorTest",0);
        printf("*** execl failed, rc = %d\n",rc);
        return 0;
    } else {
        /* parent */
        uint32_t status = 42;
        wait(id,&status);
        printf("*** back from wait %ld\n",status);
    }

    id = fork();

    if (id < 0) {
        printf("fork failed");
    } else if (id == 0) {
        /* child */
        printf("*** in child\n");
        int rc = execl("/sbin/readingTest","readingTest",0);
        printf("*** execl failed, rc = %d\n",rc);
        return 0;
    } else {
        /* parent */
        uint32_t status = 42;
        wait(id,&status);
        printf("*** back from wait %ld\n",status);
    }

    id = fork();

    if (id < 0) {
        printf("fork failed");
    } else if (id == 0) {
        /* child */
        printf("*** in child\n");
        int rc = execl("/sbin/storingTest","storingTest",0);
        printf("*** execl failed, rc = %d\n",rc);
        return 0;
    } else {
        /* parent */
        uint32_t status = 42;
        wait(id,&status);
        printf("*** back from wait %ld\n",status);
    }

    id = fork();

    if (id < 0) {
        printf("fork failed");
    } else if (id == 0) {
        /* child */
        printf("*** in child\n");
        int rc = execl("/sbin/pruningTest","pruningTest",0);
        printf("*** execl failed, rc = %d\n",rc);
        return 0;
    } else {
        /* parent */
        uint32_t status = 42;
        wait(id,&status);
        printf("*** back from wait %ld\n",status);
    }

    shutdown();
    return 0;
}
