#ifndef _DATABASE_H_
#define _DATABASE_H_

extern "C" {
    #include "libc.h"
}

// --------
// Database
// --------

namespace database {

    /**
     * Database data entry
     *
     * For the header, which does NOT count as an entry:
     * - user contains magic string "DATABASES_ARE_COOL" with null terminator
     * - time contains offset of starting position
     * - size contains num_entries of this database file
     * - message contains bitmap of file types (1 = file, 0 = text)
     */
    struct db_data {
        char     type;         // 'T' for text; 'A' for audio; 'I' for image
        char     user[19];     // Username
        uint32_t time;         // Message received time
        uint32_t size;         // Size of message
        char     message[300]; // Message
    };

    const size_t DATA_SZ = sizeof(db_data);
    // const uint32_t END_OF_DB = (MAX_DB_ENTRIES + 1) * DATA_SZ; // Only full database

    /**
     * Class for manipulating a database file
     */
    class Database {
    private:
        uint32_t    offset;           // Start of the database
        uint32_t    num_entries;      // Number of entries in the database
        char        files[300] = {0}; // Cache of file types in the database header
        const char* myPath;           // Path of database file

        /**
         * Saves the database's information to header before closing database.
         * Updates the dbHeader's time and size fields only; assumes other parts of header are correct.
         * 
         * @return True iff success
         * 
         * @author Orion
         */
        bool save();

        /**
         * Helper method to return offset of n-th entry, skipping metadata header
         * 
         * Note: Entry is capped to num_entries, which is where the next entry would be inserted
         * 
         * @param entry Entry number
         * @return      Offset of n-th entry
         * 
         * @author Orion
         */
        uint32_t getOffset(uint32_t entry);
        
        /**
         * Helper method to return whether entry n is a file or not based on the cache
         * 
         * @param n Entry number
         * @return  True if entry n is a file; false otherwise
         * 
         * @author Orion
         */
        bool isFile(uint32_t n);

        /**
         * Helper method that updates entry n's status as a file
         * 
         * @param n       Entry number
         * @param new_bit New status/bit for n-th entry
         * 
         * @author Orion
         */
        void setFile(uint32_t n, bool new_bit);

        /**
         * Stores the n-th db_data in buffer, m
         * 
         * @param m Buffer where the db_data will be stored
         * @param n n-th db_data to be retrieved
         * @return  false if failed; true if success
         * 
         * @author Wentao
         */
        bool store_nth_message_in_buffer(db_data* m, uint32_t n);

        /**
         * Binary search for entry number of first message after datetime
         * 
         * @param l Leftmost index of array
         * @param r Rightmost index of array
         * @param d Datetime in uint32_t
         * @return  First message's entry number after datetime
         * 
         * @author Wentao
         */
        uint32_t binarySearch(uint32_t l, uint32_t r, uint32_t d);

    public:
        const char TEXT    = 'T';
        const char AUDIO   = 'A';
        const char IMAGE   = 'I';
        const char DB_META = 'D';
        const char* const magicString = "DATABASES_ARE_COOL";
        const uint32_t MAX_MSG_SZ     = 300;
        const uint32_t MAX_DB_ENTRIES = 2000;

        /**
        * Constructor; opens file at path, creates file if does not exist
        * Path should always start with /files/
        * 
        * @param path Path to the database file
        * 
        * @author Orion
        */
        Database(const char *path);

        /**
         * Destructor; Closes the database connection, updating database header in file.
         * 
         * @author Orion
         */ 
        ~Database();

        /**
         * Queries latest num_messages messages.
         * Earliest message comes first in the array.
         * End of array is marked by db_data struct with type == '\0'.
         * 
         * @param num_messages Number of messages returned
         * @return             Array of db_data (messages)
         * 
         * @author Colette
         */
        db_data* queryLatest(uint32_t num_messages);

        /**
         * Queries first num_messages after datetime.
         * Earlier message comes first in array.
         * End of array is marked by db_data struct with type == '\0'.
         * 
         * Note: datetime is in Epoch format and can be obtained by the time
         * system call.
         * 
         * @param datetime     Message[s] after datetime is returned
         * @param num_messages Number of messages returned
         * @return             Array of db_data (messages)
         * 
         * @author Wentao
         */
        db_data* queryFrom(uint32_t datetime, uint32_t num_messages);

        /**
         * Prunes the oldest n messages from database and updates offset, num_entries accordingly.
         * Also deletes the files for audio and image entries from disk if requested.
         * Will not prune database metadata (offset 0, inclusive, to DATA_SZ, exclusive).
         * 
         * Note: Automatically prunes first 1/4 of the database when writing to a full database.
         *
         * @param numMessages   The number of entries to prune, starting at offset
         * @param deepPrune     True iff we should also delete image/audio file entries from disk
         * @return              The number of entries actually pruned
         * 
         * @author Shaharyar
         */
         uint32_t prune(uint32_t numMessages, bool deepPrune);

        /**
         * Attempts to store a message in the database.
         * 
         * Types of files:
         * 'T' for text
         * 'A' for audio
         * 'I' for image
         *
         * We do not check for file existence for audio/images. Please ensure
         * that the path is correct before storing so that no check is needed
         * when retrieving it.
         * 
         * Note: Automatically prunes first 1/4 of the database when writing to a full database.
         * 
         * @param type    Type of file (look above)
         * @param user    Username (18 chars/19 bytes maximum - fails to store if too big)
         * @param message Message if text; path if audio/image (cuts off at 300 chars)
         * @return        True if success; false if failure
         * 
         * @author Orion
         */
        bool store(char type, char* user, char* message);

        /**
         * Returns the path of the database file of this Database class
         * 
         * @return Path of database file
         * 
         * @author Orion
         */
        const char* getPath();
    };
};

#endif
