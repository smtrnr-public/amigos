#include "database.h"

using namespace database;

bool Database::save() {
    db_data myHeader{};

    // Stores fields as appropriate
    myHeader.type = 'D';
    memcpy(myHeader.user, (char*) "DATABASES_ARE_COOL", 19);
    myHeader.time = offset;
    myHeader.size = num_entries;
    memcpy(myHeader.message, files, 300);

    // Seeks database
    int db_fd = open(myPath, 0);
    seek(db_fd, 0);

    // Write the fields to database header
    auto writeResult = write(db_fd, &myHeader, DATA_SZ);
    close(db_fd);

    if (writeResult != DATA_SZ) { // Error
        printf("Save did a stinky write of %d bytes\n", writeResult);
        return false;
    }
        
    return true;
}

uint32_t Database::getOffset(uint32_t entry) {
    entry = entry > num_entries ? num_entries : entry;
    uint32_t pos = (offset + entry * DATA_SZ) % (MAX_DB_ENTRIES * DATA_SZ);

    if(pos == 0) {//this is metadata, let's skip it
        pos += DATA_SZ;
        // printf("offset for entry %u calculated, skipped to %u\n", entry, pos);
    } else
        // printf("offset for entry %u is %u\n", entry, pos);


    if(pos % DATA_SZ != 0) {
        puts("Y U NO ALIGN\n");
        exit(-4);
    }
    return pos;
}

bool Database::isFile(uint32_t n) {
    uint32_t pos = n / 8; // pos-th char
    uint32_t bit = n % 8; // Bit in char

    return ((files[pos] & (1 << (7 - bit))) != 0);
}

void Database::setFile(uint32_t n, bool new_bit) {
    uint32_t pos = n / 8; // pos-th char
    uint32_t bit = n % 8; // Bit in char
    uint32_t mask = (1 << (7 - bit));

    /**
     * Example of below algorithm: 
     * 
     * 1101 - Set to 1
     * 0010
     * 
     * 1111 - Set to 0
     * 1101
     */
    if (new_bit) { // Set bit to one
        files[pos] |= mask;
    } else { // Set bit to zero
        files[pos] &= (0xFF ^ mask); // we are kings and queen uwu
    }
}

bool Database::store_nth_message_in_buffer(db_data* m, uint32_t n) {
    // Pre-condition: Make sure n is inbounds
    if (n >= num_entries) {
        return false;
    }

    int db_fd = open(myPath, 0);

    // Seek to offset
    auto seek_output = seek(db_fd, getOffset(n));
    if (seek_output < 0) { // Error
        return false;
    }

    // Read message from offset
    auto read_output = read(db_fd, m, DATA_SZ);
    close(db_fd);

    if (read_output < 0) { // Error
        return false;
    }

    return true;
}

uint32_t Database::binarySearch(uint32_t l, uint32_t r, uint32_t d) {
    if (r >= l) {
        int m = l + (r - l) / 2; // Middle

        // Get m-th message
        db_data curr_message{};
        bool report = store_nth_message_in_buffer(&curr_message, m);
        if (!report) { // Error
            return -1;
        }

        // Checks if the entry that we want
        if (curr_message.time == d) { // We found a message sent in d
            int last = m;
            while (--last >= 0) { // Gets to first message sent in d second 
                report = store_nth_message_in_buffer(&curr_message, last);
                if (curr_message.time != d) {
                    break;
                }
            }
            return last + 1;
        }

        if (curr_message.time > d) {
            if (m == 0) { // First message, return m
                return m;
            }

            // Checks if curr_message is first message after d
            db_data prev_message{};
            bool prev_report = store_nth_message_in_buffer(&prev_message, m - 1);
            if (!prev_report) { // Error
                return -1;
            }

            if (prev_message.time < d) { // curr_message is first message after d
                return m;
            } else { // Search left
                return binarySearch(l, m - 1, d);
            }
        }
        // Search right
        return binarySearch(m + 1, r, d); 
    }
    return -1; // Not found
}

db_data* Database::queryFrom(uint32_t datetime, uint32_t num_messages) {
    // Get entry number of the first message after datetime
    uint32_t first_entry_wanted = binarySearch(0, num_entries, datetime);
    if (first_entry_wanted < 0) { // Error or no files after datetime
        db_data *temp = (db_data*) malloc(sizeof(db_data));
        temp[0].type = '\0';
        return temp;
    }

    // If num_messages is too large, resize appropriately
    num_messages = num_messages > (num_entries - first_entry_wanted) ? (num_entries - first_entry_wanted) : num_messages;

    // Return num_messages messages after datetime
    db_data *arr = (db_data*) malloc(sizeof(db_data) * (num_messages + 1));
    for (uint32_t i = 0; i < num_messages; i++) {
        uint32_t entryNum = first_entry_wanted + i;

        // Store message
        bool output = store_nth_message_in_buffer(&arr[i], entryNum);
        if (!output) { // Error
            free(arr);
            db_data * temp = (db_data*) malloc(sizeof(db_data));
            temp[0].type = '\0';
            return temp;
        }
    }

    arr[num_messages].type = '\0'; // Signifies end of actual array
    return arr;
}

Database::Database(const char *path) : offset(0), num_entries(0), myPath(path) { // opening the server
    // Open a file with creation flag
    int db_fd = open(path, 1); // with creation flag
    if (db_fd < 0) { // open failed
        printf("*Database open failed %s\n", path);
        exit(-1);
    }

    if (len(db_fd) == 0) { // new database!!!1!!1!
        db_data dbHeader{};
        dbHeader.type = DB_META;
        memcpy(dbHeader.user, (void*) magicString, 19);
        dbHeader.time = DATA_SZ; // offset
        dbHeader.size = 0; // num entries
        seek(db_fd, 0);
        auto numBytesWritten = write(db_fd, &dbHeader, DATA_SZ);
        if(numBytesWritten != DATA_SZ) {
            printf("Only wrote %d bytes, should have been %d\n", numBytesWritten, DATA_SZ);
            exit(-1);
        }
        offset      = DATA_SZ;
        num_entries = 0;
                    
    } else { // old database
        // make sure file has magic header
        db_data dbHeader{};
        seek(db_fd, 0);
        uint32_t bytes_read = read(db_fd, &dbHeader, DATA_SZ);
        if (bytes_read != DATA_SZ || strcmp(dbHeader.user, magicString)) {
            printf("only read %d bytes, and magic string was %s\n", bytes_read, dbHeader.user);
            printf("\n");
            char* blah = (char*)&dbHeader;
            for (int i = 0; i < 328; ++i) {
                putchar(blah[i]);
            }
            printf("\n");
            exit(-1); // bad file >:C
        }
        
        // read in offset and num entries
        offset      = dbHeader.time;
        num_entries = dbHeader.size;

        // load in file type array 
        seek(db_fd, 28);
        read(db_fd, files, 300);
    }
    close(db_fd);
}

Database::~Database() {
    // printf("Database for %s is exiting\n", myPath);
    bool b = save();

    if (!b) { // Didn't save properly
        exit(-1);
    }
}

db_data* Database::queryLatest(uint32_t num_messages) {
    // Find first message wanted
    if (num_messages == 0) {
        return nullptr;
    }
    uint32_t first_message = num_entries < num_messages ? 0 : num_entries - num_messages;

    // seek to that message
    // auto seek_output = seek(db_fd, getOffset(first_message));
    // if (seek_output < 0) { // Error
    //     return nullptr;
    // }

    // store each message into the array until the most recent message
    num_messages = num_messages > num_entries ? num_entries : num_messages;
    db_data *arr = (db_data*) malloc(sizeof(db_data) * (num_messages + 1));

    for (uint32_t i = 0; i < num_messages; i++) {
        uint32_t entryNum = first_message + i;

        bool output = store_nth_message_in_buffer(&arr[i], entryNum);
        if (!output) { // Error
            free(arr);
            return nullptr;
        }
    }

    arr[num_messages].type = '\0';
    return arr;
}

uint32_t Database::prune(uint32_t numMessages, bool deepPrune) { 
    //printf("num messages is %d and num_entries is %d\n", numMessages, num_entries);
    //printf("offset is %d\n", offset);
    // shouldnt need to go and delete the actual data if not deepPrune / not !TEXT
    if (numMessages > num_entries) { 
        numMessages = num_entries;
    }
    uint32_t new_offset = getOffset(numMessages);
    if (deepPrune) {
        // go thru each element and delete if audio/image
        for (uint32_t i = 0; i < numMessages; i++) {
            if (!isFile(i)) {
                continue;
            }
            db_data cur_message{};
            bool report = store_nth_message_in_buffer(&cur_message, i);
            if (!report || cur_message.size >= 300) {
                return i;
            }
            char *path = cur_message.message;
            if (unlink(path) == -1) {
                printf("*** FAILED ");
            }
            printf("unlinking file %s\n", path);
        }
    }
    num_entries -= numMessages;
    offset = new_offset;
    return numMessages; // right?
}

bool Database::store(char type, char* user, char* message) { 
    auto timestamp = time(nullptr); //get time of receipt
    auto userLen = strlen(user);
    if(userLen > 18) { //store error: username invalid
        printf("invalid username, username len was %d\n", userLen);
        return false;
    }
    else if(!(type == TEXT || type == AUDIO || type == IMAGE)) { //store error: type invalid
        printf("invalid type, type was %c\n", type);
        return false;
    }
    auto msgLen = strlen(message);
    if(msgLen >= 300 && (type == AUDIO || type == IMAGE)) {
        printf("error: tried to store file of path size %d (max is 299)\n", msgLen);
        return false;
    }
    msgLen = msgLen > MAX_MSG_SZ ? MAX_MSG_SZ : msgLen; //only keep up to MAX_MSG_SZ chars
    // printf("message length is %d\n", msgLen);
    if(num_entries >= MAX_DB_ENTRIES) {//full, let's prune 1/4 of the database
        // printf("hi im prune with %d entires\n", num_entries);
        prune(num_entries / 4, true);
    }
    
    db_data entry{};
    entry.type = type;
    memcpy(entry.user, user, userLen + 1); //username has null terminator...
    entry.time = timestamp;
    entry.size = msgLen;
    memcpy(entry.message, message, msgLen); //...while message does not
    //now write to file - assuming that caller synchronizes access
    setFile(num_entries, type != TEXT);
    int db_fd = open(myPath, 0);
    auto place = getOffset(num_entries++);
    if(seek(db_fd, place) < 0) { //seek error
        printf("seek error\n");
        return false;
    }
    // printf("did a seek to %d (modulo result: %d)\n", place, place % DATA_SZ);

    // uint32_t sizeOfWrite = DATA_SZ - MAX_MSG_SZ + msgLen;
    auto oldLen = len(db_fd);
    auto writeResult = write(db_fd, &entry, DATA_SZ);
    // Cutoff here
    auto theLen = len(db_fd);
    close(db_fd);
    if((uint32_t) writeResult != DATA_SZ){
        printf("offset = %d, oldLen = %d and len after write = %d\n", getOffset(num_entries), oldLen, theLen);
        printf("write result: %d\n", writeResult);
        // printf("SizeOfWrite:  %d\n", sizeOfWrite);
        num_entries--;
        return false;
    }
    // printf("write result: %d\n", writeResult);
    return true;
}

const char* Database::getPath() {
    return myPath;
}
