#include "database.h"
extern "C" {
    #include "libc.h"
}


//by Orion
extern "C" int main(int argc, char **argv) {
    printf("*** in storing/time test\n");
    using namespace database;
    
    //test time_t time(time_t *pointer)
    uint32_t times = 5;
    time_t theTime = 0;
    time_t lastTime = time(nullptr);
    while(times > 0) {
        if(time(&theTime) - lastTime >= 1) {
            times--;
            lastTime = theTime;
            printf("The time is now %d seconds since epoch\n", lastTime);
        } // this result should be verified online
    }

    //test bool store(char type, char* user, char* message)
    Database db{"/files/storeTest.db"};
    
    const char *username = "perrywinklesssssssss";
    const char *msg = "hello";
    puts("Storing message with invalid username (over 18 characters)...");
    bool result = db.store('T', (char*)username, (char*)msg);
    if(!result)
        puts("pass\n");
    else
        puts("*** failed!\n");
    
    username = "platypus";
    puts("Testing valid store...");
    result = db.store('T', (char*)username, (char*)msg);
    if(result)
        puts("pass\n");
    else
        puts("*** failed!\n");

    puts("Storing message with invalid type ('C')...");
    result = db.store('C', (char*)username, (char*)msg);
    if(!result)
        puts("pass\n");
    else
        puts("*** failed!\n");
    puts("Now we will test with a 350+ character message (our max is 300), which should cutoff at an exclamation mark\n");
    const char *myStr = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbcccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccddddddddddddddddddddd hey it's char 300! abcdefghijklmnopqrstuvwxyz pls end this message kthxbai";
    result = db.store('T', (char*)username, (char*)myStr);
    if(!result)
        printf("*** Error in storing - failed!\n");
    else {
        db_data* out = db.queryLatest(1);
        if(out->message[out->size - 1] == '!')
            printf("Last char matched!\n");
        else
            printf("*** Error - the last char was %c\n", out->message[out->size - 1]);
        free(out);
    }
    puts("*** end of storing/time test\n");
    return 0;
}
