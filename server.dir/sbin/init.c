#include "libc.h"
#include "sys.h"

int strlen(const char* str) {
    int len = 0;
    while (str[len]) {
        len++;
    }
    return len;
}

uint8_t getByte(char* text) {
    uint8_t res = 0;
    if ('0' <= text[0] && text[0] <= '9') {
        res = text[0] - 0x30;
    } else if ('A' <= text[0] && text[0] <= 'F') {
        res = text[0] - 0x37;
    } else if ('a' <= text[0] && text[0] <= 'f') {
        res = text[0] - 0x57;
    } else {
        printf("invalid MAC address\n");
    }
    res <<= 4;
    if ('0' <= text[1] && text[1] <= '9') {
        res |= text[1] - 0x30;
    } else if ('A' <= text[1] && text[1] <= 'F') {
        res |= text[1] - 0x37;
    } else if ('a' <= text[1] && text[1] <= 'f') {
        res |= text[1] - 0x57;
    } else {
        printf("invalid MAC address\n");
    }
    return res;
}

struct sockaddr_mac* textToMac(char* text) {
    struct sockaddr_mac* res = (struct sockaddr_mac*) malloc(sizeof(struct m_addr));
    int i = 0;
    int j = 0;
    while (i < 6) {
        res->smac_addr.smac_addr[i] = getByte(text + j);
        i++;
        j+=2;
        if (text[j] == ':') {
            j++;
        } else if (i != 6 && !text[j]) {
            printf("invalid MAC address\n");
        }
    }
    if (text[j]) {
        uint16_t port = getByte(text + j);
        port <<= 8;
        port |= getByte(text + j + 2);
        res->smac_port = port;
    } else {
        res->smac_port = 0;
    }
    return res;
}

struct message {
    uint32_t len;
    char message[];
} __attribute__((packed));

int main(int argc, char** argv) {
    printf("server MAC address: ");
    struct m_addr mac;
    getMAC(&mac);
    for (int i = 0; i < 5; i++) {
        printf("%02x:", mac.smac_addr[i]);
    }
    printf("%02x\n", mac.smac_addr[5]);

    int client1_sock = socket(0,0,0);
    int client2_sock = socket(0,0,0);

    struct sockaddr_mac* addr1 = textToMac("52:54:00:12:34:52");
    bind(client1_sock, (const struct sockaddr*) addr1, 0);

    struct sockaddr_mac* addr2 = textToMac("52:54:00:12:34:53");
    bind(client2_sock, (const struct sockaddr*) addr2, 0);

    char* buffer1 = (char*) malloc(100);
    recvfrom(client1_sock, buffer1, 20, 0, (struct sockaddr*) &addr1, 0);
    printf("--- client 1: %s\n\n", buffer1);
    recvfrom(client1_sock, buffer1, 20, 0, (struct sockaddr*) &addr1, 0);
    printf("--- client 1: %s\n\n", buffer1);
    recvfrom(client1_sock, buffer1, 20, 0, (struct sockaddr*) &addr1, 0);
    printf("--- client 1: %s\n\n", buffer1);
    recvfrom(client1_sock, buffer1, 20, 0, (struct sockaddr*) &addr1, 0);
    printf("--- client 1: %s\n\n", buffer1);

    char* buffer2 = (char*) malloc(100);
    recvfrom(client2_sock, buffer2, 20, 0, (struct sockaddr*) &addr2, 0);
    printf("--- client 2: %s\n\n", buffer2);
    recvfrom(client2_sock, buffer2, 20, 0, (struct sockaddr*) &addr2, 0);
    printf("--- client 2: %s\n\n", buffer2);
    recvfrom(client2_sock, buffer2, 20, 0, (struct sockaddr*) &addr2, 0);
    printf("--- client 2: %s\n\n", buffer2);
    recvfrom(client2_sock, buffer2, 20, 0, (struct sockaddr*) &addr2, 0);
    printf("--- client 2: %s\n\n", buffer2);

    shutdown();
}
