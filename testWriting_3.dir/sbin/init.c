#include "libc.h"

void appendFile(char* file) {
    int fd = open(file,0);
    uint32_t data_len = len(fd);

    printf("*** --- RUNNING ON %s: ---\n", file);
    printf("*** Current file size: %ld\n", data_len);
    if (data_len >= 40) {
        char* buf = (char*) malloc(41);
        seek(fd, data_len - 40);
        // int charsToRead = data_len - 40;
        read(fd, buf, 40);
        buf[40] = '\0';
        printf("*** End of current file:\n*** %s\n", buf);
        free(buf);
    }
    seek(fd, data_len);
    char* tmp = "My cat really likes sleeping in shoeboxes. Little weird.";
    write(fd, tmp, 57);
    int start = data_len - 20;
    if (start < 0) start = 0;
    seek(fd, start);
    data_len = len(fd);
    char* buffer = (char*) malloc(data_len - start);
    read(fd, buffer, data_len - start);
    printf("*** Appending to the end something about my cat:\n*** %s\n", buffer);
    printf("*** New size: %ld\n", data_len);
    printf("*** Writing to the next new block:\n");
    char* newStuff = (char*) malloc(1025);
    for (uint32_t i = 0; i < 1025; i++) {
        newStuff[i] = 'A';
    }
    seek(fd, len(fd));
    write(fd, newStuff, 1025);
    char* scream = (char*) malloc(50);
    seek(fd, len(fd) - 50);
    read(fd, scream, 50);
    printf("*** Checking out the end (should be screaming):\n*** %s\n", scream);

    char* tmp_file = "/data/tmpFile.txt";
    uint32_t fd_tmp = open("/data/tmpFile.txt", 1);
    printf("*** Size should be 0 for new file: %d\n", len(fd_tmp));
    char* file_content = (char*) malloc(len(data_len));
    read(fd, file_content, len(fd));
    write(fd_tmp, file_content, len(fd));
    printf("*** Size should be %d for new file: %d\n", len(fd), len(fd_tmp));
    close(fd);
    close(fd_tmp);
    unlink(file);
    unlink(tmp_file);

    fd = open(file,0);
    printf("*** File should be -1 : %d\n", fd);
    fd = open(tmp_file,0);
    printf("*** File should be -1 : %d\n", fd);
    printf("*** Done!\n\n");
}

int main(int argc, char** argv) {
    printf("*** Let's append to some files:\n");
    printf("*** Blank file:\n");
    appendFile("/data/blankFile.txt");
    printf("*** 0-level file:\n");
    appendFile("/data/smallFile.txt");
    printf("*** 1-level file:\n");
    appendFile("/data/bigFile.txt");
    printf("*** 2-level file:\n");
    appendFile("/data/veryBigFile.txt");
    shutdown();
    return 0;
}
