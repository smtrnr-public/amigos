#include "libc.h"
#include "sys.h"

int strlen(const char* str) {
    int len = 0;
    while (str[len]) {
        len++;
    }
    return len;
}

uint8_t getByte(char* text) {
    uint8_t res = 0;
    if ('0' <= text[0] && text[0] <= '9') {
        res = text[0] - 0x30;
    } else if ('A' <= text[0] && text[0] <= 'F') {
        res = text[0] - 0x37;
    } else if ('a' <= text[0] && text[0] <= 'f') {
        res = text[0] - 0x57;
    } else {
        printf("invalid MAC address\n");
    }
    res <<= 4;
    if ('0' <= text[1] && text[1] <= '9') {
        res |= text[1] - 0x30;
    } else if ('A' <= text[1] && text[1] <= 'F') {
        res |= text[1] - 0x37;
    } else if ('a' <= text[1] && text[1] <= 'f') {
        res |= text[1] - 0x57;
    } else {
        printf("invalid MAC address\n");
    }
    return res;
}

struct sockaddr_mac* textToMac(char* text) {
    struct sockaddr_mac* res = (struct sockaddr_mac*) malloc(sizeof(struct sockaddr_mac));
    int i = 0;
    int j = 0;
    while (i < 6) {
        res->smac_addr.smac_addr[i] = getByte(text + j);
        i++;
        j+=2;
        if (text[j] == ':') {
            j++;
        } else if (i != 6 && !text[j]) {
            printf("invalid MAC address\n");
        }
    }
    if (text[j]) {
        uint16_t port = getByte(text + j);
        port <<= 8;
        port |= getByte(text + j + 2);
        res->smac_port = port;
    } else {
        res->smac_port = 0;
    }
    return res;
}

struct message {
    uint32_t len;
    char message[];
} __attribute__((packed));

int main(int argc, char** argv) {
    int sock = socket(0,0,0);
    struct sockaddr_mac* addr = textToMac("52:54:00:12:34:51");

    char* str1 = "hello from client 2";
    char* str2 = "this is another tes";
    char* str3 = "str3 is here dklsaj";
    char* str4 = "!!!!!!!!!!!!!!!!!!!";
    
    printf("sending two messages\n");
    sendto(sock, (void*) str1, strlen(str1) + 1, 0, (struct sockaddr*) addr, 0);
    sendto(sock, (void*) str2, strlen(str2) + 1, 0, (struct sockaddr*) addr, 0);
    sendto(sock, (void*) str3, strlen(str3) + 1, 0, (struct sockaddr*) addr, 0);
    sendto(sock, (void*) str4, strlen(str4) + 1, 0, (struct sockaddr*) addr, 0); 

    shutdown();
}
