#ifndef _arp_h_
#define _arp_h_

#include "protocols.h"
#include "machine.h"
#include "libk.h"
#include "queue.h"
#include "atomic.h"
#include "network.h"
#include "net_consts.h"

// definitions

#define ARP_ETHERNET    0x0001
#define ARP_IPV4        0x0800
#define ARP_REQUEST     0x0001
#define ARP_REPLY       0x0002

#define cachefree		0
#define cachewaiting	1
#define cachetaken		2

#define ARPcachelength 32

namespace networking {
	// cache entry
	struct ARP_cache_entry{
		uint16_t hardwareType;
		uint32_t senderIP;
		unsigned char senderMAC[6];
		unsigned int state;
	};

	struct ARPFrame {
		uint16_t linkType;
		uint16_t protocolType;
		unsigned char linkSize;
		unsigned char protocolSize;
		uint16_t ARPtype;
		unsigned char payload[];
	}__attribute__((packed));

	struct ARP_IPV4Payload{
		unsigned char senderMAC[6];
		uint32_t senderIP;
		unsigned char recieverMAC[6];
		uint32_t recieverIP;
	} __attribute__((packed));

	struct arp {
		uint16_t htype; // Hardware type
		uint16_t ptype; // Protocol type
		uint8_t  hlen; // Hardware address length (Ethernet = 6)
		uint8_t  plen; // Protocol address length (IPv4 = 4)
		uint16_t opcode; // ARP Operation Code
		uint8_t  srchw[MAC_LEN]; // Source hardware address - hlen bytes (see above)
		uint8_t  srcpr[IPV4_LEN]; // Source protocol address - plen bytes (see above). If IPv4 can just be a "u32" type.
		uint8_t  dsthw[MAC_LEN]; // Destination hardware address - hlen bytes (see above)
		uint8_t  dstpr[IPV4_LEN]; // Destination protocol address - plen bytes (see above). If IPv4 can just be a "u32" type.
	};

	extern ARP_cache_entry arp_cache[];
	extern int ARP_cache_lastoverritten;

	extern uint8_t mac_addr[];
	extern uint8_t ipv4_addr[];
}

using namespace networking;		

class ARP{
public:
	// initialize cache and zero it out
	static void init();

	// insert into the arp cache
	// false would mean the cache is full and nothing was updated
	static bool insert_arp_cache(ARPFrame* frame, ARP_IPV4Payload* data, bool override);
	

	// try and get a mac address from the cache
	// nullptr would mean it isn't in the table and we need to request it
	static bool get_MAC_arp(uint16_t targetHWType, uint32_t targetIP, unsigned char* mac);

	// arp request
	static void arp_request(uint8_t req_ivp4_addr[]);

	// arp reply
	// all we need to do is put our own mac address and flip sender and reciever data
	static void arp_reply(ARPFrame* frame, ARP_IPV4Payload* data);

	// recieving an arp packet
	static int receivePacket(uint32_t* in, uint32_t len);
};

#endif