#include "ext2.h"
#include "libk.h"


void Node::read_block(uint32_t number, char* buffer) {
    if (number < 12) {
        // direct block
        uint32_t offset = data[number] * block_size;
        ide->read_all(offset, block_size, buffer);
        return;
    }
    number -= 12;
    const uint32_t pc1 = block_size / sizeof(uint32_t);
    if (number < pc1) {
        // singly indirect block
        uint32_t offset = (data[12] * block_size) + (number * sizeof(uint32_t));
        char* tmp = new char[4];
        ide->read_all(offset, 4, tmp);
        ide->read_all(offset, sizeof(uint32_t), (char*) &offset);
        offset *= block_size;
        ide->read_all(offset, block_size, buffer);
        delete[] tmp;
        return;
    }
    number -= pc1;
    const uint32_t pc2 = pc1 * pc1;
    if (number < pc2) {
        // doubly indirect block
        uint32_t offset = (data[13] * block_size) + (number / pc1 * sizeof(uint32_t));
        char* tmp = new char[4];
        ide->read_all(offset, 4, tmp);
        offset = *((uint32_t*)tmp);
        offset *= block_size;
        offset += (number % pc1) * 4;
        ide->read_all(offset, sizeof(uint32_t), (char*) &offset);
        offset *= block_size;
        ide->read_all(offset, block_size, buffer);
        delete[] tmp;
        return;
    }
    number -= pc2;
    {
        // trebly indirect block
        uint32_t offset = (data[14] * block_size) + (number / pc2 * sizeof(uint32_t));
        ide->read_all(offset, sizeof(uint32_t), (char*) &offset);
        offset *= block_size;
        offset += ((number % pc2) / pc1) * 4;
        ide->read_all(offset, sizeof(uint32_t), (char*) &offset);
        offset *= block_size;
        offset += (number % pc1) * 4;
        ide->read_all(offset, sizeof(uint32_t), (char*) &offset);
        offset *= block_size;
        ide->read_all(offset, block_size, buffer);
        return;
    }
}
// structure of directory entry
struct DirectoryEntry {
    uint32_t inode;
    uint16_t rec_len;
    uint16_t name_len;
    uint32_t placeholder;
    const char* getName() {
	return (const char*) &placeholder;
    }
};

Shared<Node> Ext2::create_file(Shared<Node> dir, const char* name) {
    // make sure we are starting from a directory
    ASSERT(dir->is_dir());
    // allocate an inode
    uint32_t inode_num = first_unallocated_inode;
    alloc(first_unallocated_inode, 1, true);
    Shared<Node> fileNode = getInode(inode_num);
    // set fields for the directory entry
    uint32_t nameLen = K::strlen(name);
    uint32_t sizeofdir = (sizeof(DirectoryEntry) + nameLen - 4);
    sizeofdir = sizeofdir % 4 == 0 ? sizeofdir : sizeofdir + (4 - (sizeofdir % 4)); // round up for padding
    DirectoryEntry* newEntry = (DirectoryEntry*) malloc(sizeofdir);
    newEntry -> inode = inode_num;
    newEntry -> name_len = nameLen;
    memcpy((void*)&newEntry -> placeholder, name, nameLen); // copy name to the directory entry
    newEntry -> rec_len = sizeofdir;

    // parse directory entries until we get to the last one
    auto buffer = new char[dir -> block_size];
    // uint32_t total_read = 0;
    uint32_t highestI = dir -> size / block_size - 1;
    uint32_t currOffset = 0;
    // jump to the last block
	dir->read_block(highestI, buffer);
	uint32_t togo = block_size;
	DirectoryEntry* entry = (DirectoryEntry*) &buffer[0];
    // parse the last block until we find the last dir entry
	while (togo > 0) {
        uint32_t recordLength = entry->rec_len;
        if (entry -> inode == 0) {
            goto next;
        }
        if(togo == recordLength){
            // calc size of last entry to only encompass its own information
            uint32_t sizeofLastEntry = (sizeof(DirectoryEntry) + entry -> name_len - 4);
            sizeofLastEntry = sizeofLastEntry % 4 == 0 ? sizeofLastEntry : sizeofLastEntry + (4 - (sizeofLastEntry % 4));
            // not enough space for the new directory entry
            if (newEntry -> rec_len > togo - sizeofLastEntry)
                goto allocBlock;
            entry -> rec_len = sizeofLastEntry;
            char* lastEntrySize = new char[2];
            *((uint16_t*)lastEntrySize) = entry -> rec_len;
            // modify the size of the last entry in memory
            memcpy((void*)&buffer[currOffset+4], (void*)lastEntrySize, 2);
            newEntry -> rec_len = togo - entry -> rec_len;
            // allocate the new last entry in memory
            uint32_t toAllocate = newEntry -> rec_len;
            memcpy((void*)&buffer[currOffset + entry -> rec_len], (void*)newEntry, toAllocate);
            // write the changed info to disk
            dir -> write_block(highestI, buffer);
            free(newEntry);
            delete[] lastEntrySize;
            delete[] buffer;
            return fileNode;
        }
	next:
        // go to the next directory entry
        currOffset += entry -> rec_len;
	    togo -= entry->rec_len;
	    entry = (DirectoryEntry*) ((uintptr_t) entry + entry->rec_len);
    }
    allocBlock:
        // hit the end of the directory entries
        highestI++;
        newEntry -> rec_len = block_size;
        delete[] buffer;
        // allocate block
        buffer = new char[block_size];
        memcpy((void*)buffer, (void*)newEntry, block_size);
        ide -> write_block(highestI, buffer);
        // set size
        dir -> set_size(dir -> size + block_size);
        delete[] buffer;
        return fileNode;
}

bool Ext2::clear_block(uint32_t block_num, uint32_t num_levels) {
    if (!block_num) {
        return false;
    }
    // deallocate the block
    alloc(block_num, 0, false);
    if (num_levels > 0) {
        // read block addresses into memory
        uint32_t block_total = block_size / sizeof(uint32_t);
        uint32_t* blocks = new uint32_t[block_total];
        ide->read_all(block_num * block_size, block_size, (char*) blocks);
        // clear blocks recursively
        for (uint32_t ind = 0; ind < block_total && clear_block(blocks[ind], num_levels - 1); ind++) {}
        delete[] blocks;
    }

    // Zero out current block
    char* zeroes = new char[block_size];
    bzero(zeroes, block_size);
    ide->write_all(block_num * block_size, block_size, zeroes);
    delete[] zeroes;

    return true;
}

bool Ext2::delete_file(Shared<Node> dir, const char* name) {
    // get the parent node of the file we want to delete
    Shared<Node> parent = Shared<Node>{};
    Shared<Node> dest = open(dir, name, parent);
    // ensure file exists
    if (dest == nullptr) {
        return false;
    }
    // get the filename from the filepath
    char* childBuffer = new char[K::strlen(name) + 1];
    auto childName = getChild(name, childBuffer);

    // find the file we're looking for
    auto buffer = new char[block_size];
    uint32_t total_togo = parent->size;
    uint32_t total_read = 0;
    uint32_t currOffset = 0;
    // keep track of the node that comes before the node we want to find
    DirectoryEntry* preventry = nullptr;
    DirectoryEntry* entry = nullptr;
    for (uint32_t i = 0; total_read < total_togo; i++) {
        parent->read_block(i, buffer);
        uint32_t togo = block_size;
        preventry = nullptr;
        entry = (DirectoryEntry*) &buffer[0];
        while (togo > 0) {
            uint32_t j = 0;
            const char* lhs = entry->getName();
            if (entry -> inode == 0) {
                goto next;
            }
            for (; j < entry->name_len; j++)
            if (lhs[j] != childName[j]) goto next;
            // found the inode we're looking for
            {
                uint32_t oldInode = entry -> inode;
                // not the first entry in the block
                if (preventry != nullptr) {
                    uint32_t prevRecLen = preventry -> rec_len;
                    preventry -> rec_len += entry -> rec_len;
                    // zero out the directory entry
                    bzero((void*)&buffer[currOffset], entry -> rec_len);
                    memcpy((void*)&buffer[currOffset - prevRecLen], (void*)preventry, preventry -> rec_len);
                }
                // first entry in the block
                else {
                    // only inode in the block, just deallocate the block entirely
                    if (entry -> rec_len == block_size) {
                        bzero(buffer, block_size);
                        parent -> set_size(parent -> size_in_bytes() - block_size);
                        // deallocate data block (we don't handle indirection in this case)
                        if (i < 12) {
                            alloc(parent -> data[i], 0, false);
                            parent -> data[i] = 0;
                        }
                    }
                    // not the only inode in the block, set inode to 0 so it won't be read
                    else {
                        entry -> inode = 0;
                        entry -> name_len = 0;
                        memcpy((void*)&buffer[currOffset],(void*)entry, entry -> rec_len);
                    }
                }
                parent -> write_block(i, buffer);
                // clear data blocks
                uint32_t index = 0;
                uint32_t lid = 0;
                while (index < 15 && clear_block(dest->data[index], lid)) {
                    index += 1;
                    if (index >= 12) {
                        lid += 1;
                    }
                }
                // zero inode in the disk
                auto deleteBuffer = new char[128];
                bzero(deleteBuffer, 128);
                uint32_t offset = dest -> offset;
                ide -> write_all(offset, 128, deleteBuffer);
                alloc(oldInode, 0, true); // deallocate the block
                delete[] deleteBuffer;
                delete[] childBuffer;
                return true;
            }
        next:
            total_read += entry -> rec_len;
            currOffset += entry -> rec_len;
            togo -= entry->rec_len;
            preventry = entry;
            entry = (DirectoryEntry*) ((uintptr_t) entry + entry->rec_len);

        }
            currOffset = 0;
    }
    return false;
}

uint32_t Node::entry_count() {
    if (entries == 0) {
	auto buffer = new char[block_size];
	uint32_t total_togo = size;
	for (uint32_t i = 0; total_togo > 0; i++, total_togo -= block_size) {
	    read_block(i, buffer);
	    uint32_t togo = block_size;
        // While there's another directory to read increment the count
	    DirectoryEntry* entry = (DirectoryEntry*) &buffer[0];
	    while (togo > 0) {
		if (entry->inode != 0) entries++;
		togo -= entry->rec_len;
		entry = (DirectoryEntry*) ((uintptr_t) entry + entry->rec_len);
	    }
	}
	delete[] buffer;
    }
    return entries;
}

struct BlockGroupDescriptor {
    uint32_t block_usage_bitmap;      // 4 bytes
    uint32_t inode_usage_bitmap;      // 8 bytes
    uint32_t inode_table;             // 12 bytes
    uint16_t unallocated_block_count; // 14 bytes
    uint16_t unallocated_inode_count; // 16 bytes
    uint16_t directory_count;         // 18 bytes
    uint16_t unused[7];               // 32 bytes
};

Shared<Node> Ext2::getInode(uint32_t inode) {
    // extract raw data into buffer
    uint32_t block_group = (inode-1) / inode_count_per_group;
    ASSERT(block_group < block_group_count);

    auto bgd = group_table[block_group];
    uint32_t table_offset = bgd.inode_table * block_size;
    uint32_t index = (inode-1) % inode_count_per_group;
    uint32_t inode_offset = table_offset + (index * get_inode_size());
    auto buffer = new char[get_inode_size()];
    auto cnt = ide->read_all(inode_offset, get_inode_size(), buffer);
    ASSERT(cnt >= 0);
    
    // read relevent inode fields
    auto node = Shared<Node>::make(block_size, inode, inode_offset);
    node->ide = ide;
    node->fs = this;
    node->mode = *((uint16_t*) &buffer[0]);
    node->size = *((uint32_t*) &buffer[4]);
    node->link_count = *((uint16_t*) &buffer[26]);
    memcpy(&node->data[0], &buffer[40], sizeof(uint32_t[15]));
    delete[] buffer;
    return node;
}

void Ext2::alloc(uint32_t id, uint32_t flag, bool inode_mode) {
    uint32_t per_group = inode_mode ? inode_count_per_group : block_count_per_group;
    id = inode_mode ? id - 1 : id;
    // Get offsets for the bitmap, byte in bitmap, and bit for inode in byte
    uint32_t block_group = id / per_group;
    uint32_t byte_offset = id % per_group / 8;
    uint32_t bit_offset = id % 8;
    auto bgd = group_table[block_group];

    uint32_t bitmap = inode_mode ? bgd.inode_usage_bitmap : bgd.block_usage_bitmap;
    uint32_t bitmap_offset = bitmap * block_size;

    // Get current byte
    char* line = new char[1];
    ide->read_all(bitmap_offset + byte_offset, 1, line);
    // Slot in the 1 and write back
    ASSERT(flag || line[0] & 1 << bit_offset);
    line[0] = line[0] | flag << bit_offset;
    ide->write_all(bitmap_offset + byte_offset, 1, line);

    bool next = false;
    if (bit_offset < 7) {
        next = line[bit_offset + 1];
    } else {
        ide->write_all(bitmap_offset + byte_offset + 1, 1, line);
        next = line[0];
    }
    // Other memory stuff
    // Most of the time fast -- check if the next node is available
    // If it isn't then find the next available node from the start (quite slow)
    // Have to do this chekc in case we reboot computer w/o recompiling disk image and
    // first unnalocated was previously deleted
    if (inode_mode) {
        first_unallocated_inode = next ? first_unallocated_inode + 1 : Ext2::find_unalloc(true);
        unalloc_inode_count -= 1;
    } else {
        first_unallocated_block = next ? first_unallocated_block + 1 : Ext2::find_unalloc(false);
        unalloc_block_count -= 1;
    }
    delete[] line;
}

void Ext2::writeGroup(uint32_t id, char* buffer, bool inode_mode) {
    int32_t per_group = inode_mode ? inode_count_per_group : block_count_per_group;
    id = inode_mode ? id - 1 : id;
    // Get offsets for the bitmap, byte in bitmap, and bit for inode in byte
    uint32_t block_group = id / per_group;
    auto bgd = group_table[block_group];
    ASSERT(block_group < block_group_count);
    uint32_t bitmap = inode_mode ? bgd.inode_usage_bitmap : bgd.block_usage_bitmap;
    uint32_t bitmap_offset = bitmap * block_size;
    uint32_t bitmap_bytes = per_group / 8;
    auto cnt = ide->read_all(bitmap_offset, bitmap_bytes, buffer);
    ASSERT(cnt == bitmap_bytes);
}

void Ext2::printGroup(uint32_t id, bool inode_mode) {
    // Get the necessary information if you're an inode or block
    const char* type = inode_mode ? "i-nodes" : "blocks";
    int32_t per_group = inode_mode ? inode_count_per_group : block_count_per_group;
    uint32_t bitmap_bytes = per_group / 8;
    auto buffer = new char[bitmap_bytes];
    writeGroup(id, buffer, inode_mode);

    // For each 8 bits print out if each corresponding inode/block is allocated
    id = inode_mode ? id - 1 : id;
    uint32_t block_group = id / per_group;
    Debug::printf("%s per group : %d\n", type, per_group);
    uint32_t first = block_group * per_group;
    for (uint32_t i = 0; i < bitmap_bytes; i++) {
        uint32_t first_line = first + i * 8 + 1;
        Debug::printf("%s %d -> %d: ", type, first_line, first_line + 7);
        for (uint32_t j = 0; j < 8; j++) {
            Debug::printf("%d", (buffer[i] & (1 << j)) >> j);
        }
        Debug::printf("\n");
    }
    delete[] buffer;
}

// Assuming that first-unalloc node is in first group
// Should change for n-group (shouldn't be bad)
uint32_t Ext2::find_unalloc(bool inode_mode) {
    // No inodes after compiling directory? This shouldn't happen even
    // if it's possible. Let's crash for now.
    uint32_t unalloc_count = inode_mode ? unalloc_inode_count : unalloc_block_count;
    ASSERT(unalloc_count > 0);
    uint32_t start = inode_mode ? 1 : 0;
    uint32_t byte_offset = 0;
    uint32_t ret = 0;
    bool done = false;

    // Allocate space for bitmap
    uint32_t per_group = inode_mode ? inode_count_per_group : block_count_per_group;
    uint32_t bitmap_bytes = per_group / 8;
    auto buffer = new char[bitmap_bytes];
    // Could check that i < last block but I put an assert above to
    // make sure we have an alloc-able inode somewhere
    while (!done) {
        // Given current i-node group copy into the buffer
        writeGroup(start, buffer, inode_mode);
        // Find the first line with a zero bit 
        for (byte_offset = 0; !done && byte_offset < bitmap_bytes; byte_offset++) {
            done = ~buffer[byte_offset];
        }
        // If you couldn't find one then move onto the next group
        if (!done) {
            start += inode_mode ? inode_count_per_group : block_count_per_group;
        }
    }
    // Kinda gross -- offset by one because of my for loop
    byte_offset -= 1;
    // Find the first bit with a 0 -- map this bit to its inode num
    for (uint32_t bit_offset = 0; !ret && bit_offset < 8; bit_offset++) {
        if (!((buffer[byte_offset] & (1 << bit_offset)) >> bit_offset)) {
            ret = start + byte_offset * 8 + bit_offset;
        }
    }

    delete[] buffer;
    // Make sure we actually found a bit in the line we selected
    ASSERT(ret != 0);
    return ret;
}

Ext2::Ext2(Shared<Ide> ide) : ide{ide} {    
    // read superblock (sector 2)
    auto buffer = new char[ide->block_size];
    ide->read_block(2, buffer);

    // extract superblock information    
    inode_count = *((uint32_t*) &buffer[0]);
    block_count = *((uint32_t*) &buffer[4]);

    unalloc_inode_count = *((uint32_t*) &buffer[12]);
    unalloc_block_count = *((uint32_t*) &buffer[16]);
    
    block_size = 1024 << *((uint32_t*) &buffer[24]);

    block_count_per_group = *((uint32_t*) &buffer[32]);
    inode_count_per_group = *((uint32_t*) &buffer[40]);

    // number of block groups
    block_group_count = ((inode_count-1) / inode_count_per_group) + 1;
    ASSERT(block_group_count == ((block_count-1) / block_count_per_group) + 1);

    delete[] buffer;
    
    // read block group descriptor table
    uint32_t bgdt_offset = block_size * (block_size == 1024 ? 2 : 1);
    uint32_t bgdt_size = block_group_count * sizeof(BlockGroupDescriptor);

    buffer = new char[bgdt_size];
    ide->read_all(bgdt_offset, bgdt_size, buffer);
    
    // initialize group table in memory
    group_table = new BGD[block_group_count];
    for (uint32_t i = 0; i < block_group_count; i++) {
	auto bgd = &group_table[i];
    bgd -> block_usage_bitmap = *((uint32_t*) &buffer[(sizeof(BlockGroupDescriptor) * i)]);
    bgd -> inode_usage_bitmap = *((uint32_t*) &buffer[(sizeof(BlockGroupDescriptor) * i) + 4]);
    bgd -> inode_table = *((uint32_t*) &buffer[(sizeof(BlockGroupDescriptor) * i) + 8]);
    bgd -> unallocated_block_count = *((uint16_t*) &buffer[(sizeof(BlockGroupDescriptor) * i) + 12]);
    bgd -> unallocated_inode_count = *((uint16_t*) &buffer[(sizeof(BlockGroupDescriptor) * i) + 14]);
    bgd -> directory_count = *((uint16_t*) &buffer[(sizeof(BlockGroupDescriptor) * i) + 16]);
    }
    delete[] buffer;

    root = getInode(2);    

    first_unallocated_inode = find_unalloc(true);
    first_unallocated_block = find_unalloc(false);
}

Shared<Node> Ext2::find(Shared<Node> dir, const char* name) {
    ASSERT(dir->is_dir());
    // Create buffer for reading block -- start with all the block to read
    auto buffer = new char[block_size];
    uint32_t total_togo = dir->size;
    for (uint32_t i = 0; total_togo > 0; i++, total_togo -= block_size) {
	dir->read_block(i, buffer);
	uint32_t togo = block_size;
    // Start with first directory
	DirectoryEntry* entry = (DirectoryEntry*) &buffer[0];
	while (togo > 0) {
	    const char* lhs = entry->getName();
        // Check if current directory matches the desired directory
	    uint32_t i = 0;
	    for (; i < (uint32_t) entry->name_len; i++) {
            if (lhs[i] != name[i]) goto next;
            if(i + 1 == (uint32_t) entry->name_len) {
                auto node = getInode(entry->inode);
                // Perfect match
                if (name[i + 1] == 0) { delete[] buffer; return node; }
                // Good so far -- check for subdirectory match
                if (name[i + 1] == '/') { delete[] buffer; return find(node, (const char*)&name[i+2]); }
            }
        }
	next:
        // Iterate to next
	    togo -= entry->rec_len;
	    entry = (DirectoryEntry*) ((uintptr_t) entry + entry->rec_len);
	}
    }
    delete[] buffer;
    return Shared<Node>{};
}

static constexpr uint32_t MAX_DEPTH = 10;

Shared<Node> Ext2::open(Shared<Node> dir, const char* name, Shared<Node>& setParent) {
    ASSERT(dir->is_dir());
     
    // Set temp variables for easier returns and modification
    Shared<Node> cd = dir;
    Shared<Node> file;
    char* path;
    char* base;
    uint32_t depth = MAX_DEPTH;
    auto buffer = new char[block_size];
    {
	int sz = K::strlen(name);
	base = new char[sz+1];
	memcpy(base, name, sz+1);
    }

follow_path:

    if (depth == 0)
	goto fail;

    // If an absolute path go to root
    path = base;
    if (path[0] == '/') {
	cd = root;
	path++;
    }

next_link:

    // path is invalid
    if (!cd->is_dir())
	goto fail;

    // iterate through entries
    for (uint32_t b = 0; b < (cd->size_in_bytes() / block_size); b++) {
	cd->read_block(b, buffer);
	uint32_t togo = block_size;
	DirectoryEntry* entry = (DirectoryEntry*) buffer;

	while (togo > 0) {
	    const char* lhs = entry->getName();
        ASSERT(entry -> rec_len != 0);
        // Check if strings are equal
	    uint32_t i = 0;
	    for (; i < entry->name_len; i++)
		if (lhs[i] != path[i]) goto next_entry;
	    
        // If we reach the end 
	    if (path[i] == 0) {
		file = getInode(entry->inode);
		goto parse_file;
        //  If we hit a new directory start with next and change current directory
	    } else if (path[i] == '/') {
		cd = getInode(entry->inode);
		path = &path[i+1];
		goto next_link;
	    }
	
    // Iterates to next directory
	next_entry:
	    togo -= entry->rec_len;
	    entry = (DirectoryEntry*) ((uintptr_t) entry + entry->rec_len);
	}
    }

    goto fail;

parse_file:    
    
    delete[] base;
    // Copy sym link string
    if (file->is_symlink()) {
	uint32_t sz = file->size_in_bytes();
	base = new char[sz+1];
	base[sz] = 0;
	file->get_symbol(base);
	depth--;
	goto follow_path;
    } else if (file->is_dir()) {
	file = Shared<Node>{};
    }
    
    delete[] buffer;
    setParent = cd;
    return file;
    
fail:
    
    delete[] buffer;
    delete[] base;
    return Shared<Node>{};
}

Shared<Node> Ext2::getParent(const char* path, Shared<Node> cd) {
    auto cwd = Shared<Node>{};
    // If an absolute path
    if (path[0] == '/') {
	    cwd = root;
	    path++;
    }
    else
        cwd = cd;
    bool readingParent = false;
    bool done = false;
    uint32_t count = 0;
    uint32_t numSlashes = 0;
    int startingIndex = -1;
    uint32_t i = K::strlen(path) - 1;
    // Find end of the first directory in the string
    while(i > 0 && !done){
        if(path[i] == '/'){
            readingParent = true;
            startingIndex = 0;
            numSlashes++;
        }
        if(readingParent){
            count++;
        }
        i--;
    }
    // If never found a directory then return current
    if(startingIndex == -1){
        if (numSlashes == 0) // relative path from pwd
            return cwd;
        else
            return Shared<Node>{};
    }
    // Make string of just directory
    uint32_t j = 0;
    char* parentName = new char[count + 1];
    while(j < count){
        parentName[j] = path[startingIndex++];
        j++;
    }
    parentName[count] = 0;
    // Find path of first directory
    Shared<Node> toReturn = find(cwd, (const char*)parentName);
    delete[] parentName;
    return toReturn;

}

const char* Ext2::getChild(const char* path, char* buffer) {
    uint32_t length = K::strlen(path);
    int i = length - 1;
    // Find the first /
    while (i >= 0 && path[i] != '/')
        i--;
    if (i < 0)
        return path;
    else {
        // Copies string from after first directory (after the /)
        memcpy((void*)buffer, (void*)(&path[i + 1]), length - i);
        return (const char*) buffer;
    }
}

void Node::allocDataBlock(uint32_t location, uint32_t* buffer) {
    // find first unallocated block
    uint32_t addr = fs -> first_unallocated_block;
    // allocate the block
    fs ->alloc(addr, 1, false);
    // write block address to the specified location in disk
    *buffer = addr;
    auto cnt = ide->write_all(location, 4, (char*) buffer);
    ASSERT(cnt >= 0);
}

void Node::write_block(uint32_t number, char* buffer) {
    uint32_t* tempBuffer = (uint32_t*) malloc(sizeof(uint32_t));
    // direct block
    if (number < 12) {
        // unallocated block
        if (data[number] == 0) {
            allocDataBlock(this->offset + 40 + (4 * number), tempBuffer);
            data[number] = *tempBuffer;
        }
        uint32_t off = data[number] * block_size;
        ide->write_all(off, block_size, buffer);
        free(tempBuffer);
        return;
    }
    uint32_t off = 0;
    number -= 12;
    const uint32_t pc1 = block_size / sizeof(uint32_t);
    // singly indirect block
    if (number < pc1) {
        // allocate data inode
        if (data[12] == 0) {
            allocDataBlock(this->offset + 40 + (4 * 12), tempBuffer);
            data[12] = *tempBuffer;
        }
        // read from 1st level
        off = (data[12] * block_size) + (number * sizeof(uint32_t));
        ide->read_all(off, sizeof(uint32_t), (char*) tempBuffer);

        // allocate first level
        if (*tempBuffer == 0)
            allocDataBlock(off, tempBuffer);
        off = *tempBuffer * block_size;

        // write into disk
        ide->write_all(off, block_size, buffer);
        free(tempBuffer);
        return;
    }
    number -= pc1;
    const uint32_t pc2 = pc1 * pc1;
    // doubly indirect block
    if (number < pc2) {
        // allocate data inode
        if (data[13] == 0) {
            // allocate inode block
            allocDataBlock(this->offset + 40 + (4 * 13), tempBuffer);
            data[13] = *tempBuffer;
        }
        // read 1st level
        off = (data[13] * block_size) + (number / pc1 * sizeof(uint32_t));
        ide->read_all(off, sizeof(uint32_t), (char*) tempBuffer);
        // allocate 1st level of indirection
        if (*tempBuffer == 0)
            allocDataBlock(off, tempBuffer);
        // read 2nd level
        off = (*tempBuffer * block_size) + (number % pc1) * 4;     
        ide->read_all(off, sizeof(uint32_t), (char*) tempBuffer);
        // allocate 2nd level
        if (*tempBuffer == 0)
            allocDataBlock(off, tempBuffer);
        off = *tempBuffer * block_size;
        ide->write_all(off, block_size, buffer);
        free(tempBuffer);
        return;
    }
    number -= pc2;
    {
        // trebly indirect block
        // allocate data inode
        if (data[14] == 0) {
            // allocate inode block
            allocDataBlock(this->offset + 40 + (4 * 14), tempBuffer);
            data[14] = *tempBuffer;
        }
        // read 1st level
        off = (data[14] * block_size) + (number / pc2 * sizeof(uint32_t));
        ide->read_all(off, sizeof(uint32_t), (char*) tempBuffer);
        // allocate 1st level
        if (*tempBuffer == 0)
            allocDataBlock(off, tempBuffer);
        // first = *tempBuffer;
        // read 2nd level
        off = *tempBuffer * block_size + ((number % pc2) / pc1) * 4;
        ide->read_all(off, sizeof(uint32_t), (char*) tempBuffer);
        // allocate 2nd level
        if (*tempBuffer == 0)
            allocDataBlock(off, tempBuffer);
        // second = *tempBuffer;
        // read 3rd level
        off = *tempBuffer * block_size + (number % pc1) * 4;
        ide->read_all(off, sizeof(uint32_t), (char*) tempBuffer);
        // allocate 3rd level
        if (*tempBuffer == 0)
            allocDataBlock(off, tempBuffer);
        off = *tempBuffer * block_size;
        ide->write_all(off, block_size, buffer);
        free(tempBuffer);
        return;
    }
}