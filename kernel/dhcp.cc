#include "dhcp.h"
#include "machine.h"
#include "heap.h"
#include "network.h"

void DHCP::init() {

}

void DHCP::discovery() {
    using namespace networking;

    DHCPPayload payload;
    payload.op = DHCP_GENERAL_REQUEST;
    payload.htype = DHCP_ETHERNET;
    payload.hlen = MAC_LEN;
    payload.hops = 0;
    payload.xid = DHCP_XID;
    payload.secs = 0;
    payload.flags = 0;
    bzero(payload.ciaddr, IPV4_LEN);
    bzero(payload.yiaddr, IPV4_LEN);
    bzero(payload.siaddr, IPV4_LEN);
    bzero(payload.giaddr, IPV4_LEN);
    bzero(payload.chaddr, MAX_DHCP_CHADDR_LENGTH);
    memcpy(payload.chaddr, mac_addr, MAC_LEN);
    bzero(payload.sname, MAX_DHCP_SNAME_LENGTH);
    bzero(payload.file, MAX_DHCP_FILE_LENGTH);

    uint8_t magic_cookie[4] = {0x63,0x82,0x53,0x63};
    uint8_t dhcp_discover[3] = {0x35,0x01,0x01};
    uint8_t parameter_req[4] = {0x37,0x02,0x01,0x03};

    uintptr_t options = (uintptr_t) payload.options;
    memcpy((void*) (options + 0), magic_cookie, 4);
    memcpy((void*) (options + 4), dhcp_discover, 3);
    memcpy((void*) (options + 7), parameter_req, 4);

    *(char*) (options + 11) = 0xff;

    //UDP::sendPacket(saddr=0.0.0.0,daddr=255.255.255.255,sport=68,dport=67,payload=&payload,size=sizeof(DHCPPayload));
}

void DHCP::request() {

}

void DHCP::receivePacket() {

}
