#include "hda.h"
#include "afg.h"

#define HW_WAIT_MS 100

namespace audio {
    static PCIHeader0* head;
    static void* ioAddr;
    static void* memAddr;
    static HDA::hdaDeviceRegs* regs;
    static uint32_t corbsz_bytes;
    static uint32_t rirbsz_bytes;
}

/* calloc() basically */
void clear_buff(uint32_t ptr, uint32_t sz) {
    uint8_t* buff = (uint8_t*) ptr;
    for (uint32_t i = 0; i < sz; ++ i) buff[i] = 0;
}

/* purely for debugging */
void print_corb(uint32_t b, uint32_t sz) {
    Debug::printf("~~~ CORB ~~~\n");
    sz = sz / 4;
    uint32_t* buff = (uint32_t*) b;
    int newline_every = 8;
    for(uint32_t i = 0; i < sz; i++) {
        Debug::printf("%08x ", buff[i]);
        if (i % newline_every == 7) Debug::printf("\n");
    }
}

void print_rirb(uint32_t b, uint32_t sz) {
    Debug::printf("~~~ RIRB ~~~\n");
    sz = sz / 8;
    uint64_t* buff = (uint64_t*) b;
    int newline_every = 8;
    for(uint32_t i = 0; i < sz; i++) {
        Debug::printf("%016x ", buff[i]);
        if (i % newline_every == 7) Debug::printf("\n");
    }
}

void print_buffers() {
    using namespace audio;
    print_corb(regs->CORBLBASE, corbsz_bytes);
    print_rirb(regs->RIRBLBASE, rirbsz_bytes);
}

void HDA::init() {    
    using namespace audio;

    Debug::printf("-------------------------------\n");
    Debug::printf("| Initializing Intel HD Audio Device\n");

    // find the PCI device
    findDevice();
    ASSERT(memAddr != nullptr);

    discoverCodec();
    ASSERT(regs->STATESTS > 0);

    initCORB();
    initRIRB();
    initDMA();
    startDMA();
        
    AFG afg (0);

    //print_buffers();

    Debug::printf("-------------------------------\n");
}

uint32_t HDA::commandResponse(uint8_t codec, uint8_t node, uint32_t command, uint32_t data) {
    using namespace audio;

    HDA::issueCommand(codec, node, command, data);
    waitms(1);
    uint64_t* response = ((uint64_t*) regs->RIRBLBASE) + regs->RIRBWP;
    uint8_t tempWP = regs->RIRBWP;
    //while response is unsolicited
    while(*response & 0x0010000000000000) {
        tempWP--;
        response = (uint64_t*) regs->RIRBLBASE + tempWP * 8;
    }
    
    return (uint32_t) *response;
}

void HDA::issueCommand(uint8_t codec, uint8_t node, uint32_t command, uint32_t data) {
    using namespace audio;
   
    size_t CORB_max_entries = 0;     // number of entries supported by CORB
    uint8_t CORB_size_bytes = regs->CORBSIZE & 0x3;

    // configure the buffer registers by loading into memory
    if(CORB_size_bytes == 0) CORB_max_entries = 2;
    else if(CORB_size_bytes == 1) CORB_max_entries = 16;
    else if(CORB_size_bytes == 2) CORB_max_entries = 256;
    else Debug::panic("Audio CORB size not supported.\n");

    uint32_t* entry = (uint32_t*) (regs->CORBLBASE + (((regs->CORBWP + 1) % CORB_max_entries) * sizeof(uint32_t)));

    uint32_t space_used = regs->CORBWP - regs->CORBRP;
    loop: // Check for ample space
    if(space_used >= CORB_max_entries - 1) {
        pause();
        goto loop;
    }
    
    uint32_t verb_payload = command << 8 | data;
    if (command < 0x10) {
        verb_payload = command << 16 | data;
    }
    // init the entry and issue the command
    *entry += codec << 28;
    *entry += node << 20;
    *entry += verb_payload;

    regs->CORBWP = (regs->CORBWP + 1) % (CORB_max_entries);
}

void waitms(uint16_t ms) {
    using namespace audio;
    uint32_t targetTime = regs->WALCLK + 24000 * ms;
    while(regs->WALCLK < targetTime) pause();
}

void printMem(void* start, uint32_t length) {
    uint32_t* pointer = (uint32_t*) start;
    for(uint32_t i = 0; i < length; i++)
        Debug::printf("4Byte %d: %x\n", i, pointer[i]);
}

void HDA::findDevice() {
    using namespace audio;

    Debug::printf("| Finding PCI device with VID: 0x8086 and DID: 0x293e\n");

    PCIHeader0* header = (PCIHeader0*) malloc(sizeof(PCIHeader0));
    PCI::discoverICH9IntelHDA(header);

    PCI::enableBusmastering(header, 0x8086, 0x293e);

    PCI::discoverICH9IntelHDA(header);

    head = header;
    int io_bar = -1;
    int mem_bar = -1;

    // Identify the address of the memory space belonging to this device
    for (int i = 0; i < 6; i++) {
        if(head->BAR[i] & 0x1) io_bar = i;
        else if(head->BAR[i]) mem_bar = i;
    }

    //Set to 0xffffffff if no bar exists
    ioAddr = (void*) (io_bar == -1 ? -1 : head->BAR[io_bar] & (0xfffffffc));
    memAddr = (void*) (mem_bar == -1 ? -1 : head->BAR[mem_bar] & (0xfffffff0));

    regs = (hdaDeviceRegs*) memAddr;

    Debug::printf("| Device regs mapped at 0x%08x\n", memAddr);
}

void HDA::discoverCodec() {
    using namespace audio;

    Debug::printf("| Issuing reset\n");

    //Set CRST bit to 0 for reset of CODEC
    regs->GCTL = regs->GCTL & 0xfffffffe;
    while(regs->GCTL & 0x1) pause();
    waitms(1);

    //Set CRST bit to 1 after reset is complete
    regs->GCTL = regs->GCTL | 0x1;
    //Debug::printf("GCTL: %x\n", regs->GCTL);
    while((regs->GCTL & 0x1) == 0) pause();
    waitms(1);

    Debug::printf("| Codecs: %04x\n", regs->STATESTS);
}

void HDA::initCORB() {
    using namespace audio;
    ASSERT((regs->CORBCTL & 0x2) == 0); // Run bit must be 0 before initializing
    /* if assert fails: disable the control bit and then initialize the CORB, and then re-enable it */

    // Find CORB size cap
    // uint8_t CORBSZCAP =  (regs->CORBSIZE & 0xF0) >> 4;

    // Set CORB size to 256 entries
    corbsz_bytes = 256 * 4;     // 256 entries * 4 bytes size of each entry
    regs->CORBSIZE = (regs->CORBSIZE & 0xFC) | 0x2;

    regs->CORBLBASE = (uint32_t) malloc(2048);
    regs->CORBLBASE = regs->CORBLBASE + ((1024 - (regs->CORBLBASE % 1024)) % 1024);
    clear_buff(regs->CORBLBASE, corbsz_bytes);
    ASSERT(regs->CORBLBASE % 1024 == 0);

    regs->CORBUBASE = 0;
    
    regs->CORBWP = 0; // should always be 0
    regs->CORBRP = regs->CORBRP | 0x8000; // Set 1 to RP's reset bit
    // tricky, because this is not memory (so setting the flag will not immediately set it)
    while (!(regs->CORBRP & 0x8000)) pause();
    
    regs->CORBRP = regs->CORBRP & 0x7fff;
    while (regs->CORBRP & 0x8000) pause();

    Debug::printf("| CORB is at 0x%08x\n", regs->CORBLBASE);
}

void HDA::initRIRB() {
    using namespace audio;

    ASSERT((regs->RIRBCTL & 0x2) == 0); // Run bit must be 0 before initializing

    // Find RIRB size cap
    // uint8_t RIRBSZCAP =  (regs->RIRBSIZE & 0xF0) >> 4;

    // Set RIRB size to 256 entries
    rirbsz_bytes = 256 * 8;     // similar calculation
    regs->RIRBSIZE = (regs->RIRBSIZE & 0xFC) | 0x2;

    regs->RIRBLBASE = (uint32_t) malloc(4096);
    regs->RIRBLBASE = regs->RIRBLBASE + ((rirbsz_bytes - (regs->RIRBLBASE % rirbsz_bytes)) % rirbsz_bytes);
    clear_buff(regs->RIRBLBASE, rirbsz_bytes);

    regs->RIRBUBASE = 0;
    
    regs->RIRBWP |= 0x8000;

    // ghetto read pointer
    regs->RIRBCNT = 0xFF; // idk I DO KNOW KINDA SORTA NVM I DONT LOL 

    regs->INTCTL |= 0xc000003f;
    waitms(HW_WAIT_MS);

    Debug::printf("| RIRB is at 0x%08x\n", regs->RIRBLBASE);
}

void HDA::initDMA() {
    using namespace audio;

    regs->DPLBASE = (uint32_t) malloc(2048);
    regs->DPLBASE = regs->DPLBASE + ((1024 - (regs->DPLBASE % 1024)) % 1024);
    regs->DPUBASE = 0;
    waitms(HW_WAIT_MS);
    ASSERT(regs->DPLBASE % 1024 == 0);

    Debug::printf("| DMA  is at 0x%08x\n", regs->DPLBASE);
}

void HDA::startDMA() {
    using namespace audio;

    regs->CORBCTL |= 0x2;
    regs->RIRBCTL |= 0x3;
    waitms(HW_WAIT_MS);
    
    Debug::printf("| DMA engines started\n");
}

void HDA::printRegs() {
    using namespace audio;

    Debug::printf("\n~~~~~~~~~~~~ REGISTERS ~~~~~~~~~~~~\n");
    Debug::printf("GCAP: %04x\n", regs->GCAP);
    Debug::printf("VMIN: %04x\n", regs->VMIN);
    Debug::printf("VMAJ: %04x\n", regs->VMAJ);
    Debug::printf("OUTPAY: %04x\n", regs->OUTPAY);
    Debug::printf("INPAY: %04x\n", regs->INPAY);
    Debug::printf("GCTL: %04x\n", regs->GCTL);
    Debug::printf("WAKEEN: %04x\n", regs->WAKEEN);
    Debug::printf("STATESTS: %04x\n", regs->STATESTS);
    Debug::printf("OUTSTRMPAY: %04x\n", regs->OUTSTRMPAY);
    Debug::printf("INSTSMPAY: %04x\n", regs->INSTRMPAY);
    Debug::printf("INTCTL: %04x\n", regs->INTCTL);
    Debug::printf("INTSTS: %04x\n", regs->INTSTS);
    Debug::printf("WALCLK: %04x\n", regs->WALCLK);
    Debug::printf("SSYNC: %04x\n", regs->SSYNC);
    Debug::printf("CORBLBASE: %04x\n", regs->CORBLBASE);
    Debug::printf("CORBUBASE: %04x\n", regs->CORBUBASE);
    Debug::printf("CORBWP: %04x\n", regs->CORBWP);
    Debug::printf("CORBRP: %04x\n", regs->CORBRP);
    Debug::printf("CORBCTL: %04x\n", regs->CORBCTL);
    Debug::printf("CORBSTS: %04x\n", regs->CORBSTS);
    Debug::printf("CORBSIZE: %04x\n", regs->CORBSIZE);
    Debug::printf("RIRBLBASE: %04x\n", regs->RIRBLBASE);
    Debug::printf("RIRBUBASE: %04x\n", regs->RIRBUBASE);
    Debug::printf("RIRBWP: %04x\n", regs->RIRBWP);
    Debug::printf("RIRBCNT: %04x\n", regs->RIRBCNT);
    Debug::printf("RIRBCTL: %04x\n", regs->RIRBCTL);
    Debug::printf("RIRBSTS: %04x\n", regs->RIRBSTS);
    Debug::printf("RIRBSIZE: %04x\n", regs->RIRBSIZE);
    Debug::printf("ICOI: %04x\n", regs->ICOI);
    Debug::printf("ICII: %04x\n", regs->ICII);
    Debug::printf("ICIS: %04x\n", regs->ICIS);
    Debug::printf("DPLBASE: %04x\n", regs->DPLBASE);
    Debug::printf("DPUBASE: %04x\n", regs->DPUBASE);
    Debug::printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
}