#ifndef _dhcp_h_
#define _dhcp_h_

#include "stdint.h"
#include "net_consts.h"

#define MAX_DHCP_CHADDR_LENGTH  16
#define MAX_DHCP_SNAME_LENGTH   64
#define MAX_DHCP_FILE_LENGTH    128
#define MAX_DHCP_OPTIONS_LENGTH 312

// OP
#define DHCP_GENERAL_REQUEST 1
#define DHCP_GENERAL_REPLY   2

// HTYPE
#define DHCP_ETHERNET 1

#define DHCP_XID          0x3903F326
#define DHCP_MAGIC_COOKIE 0x63825363

// option codes
#define DHCP_MSG_TYPE       53 
#define DHCP_ADDR_REQUEST   50
#define DHCP_PARAMETER_LIST 55

// option parameters
#define DHCP_DISCOVER 1
#define REQUEST_SUBNET_MASK 1
#define REQUEST_ROUTER 3
#define REQUEST_DOMAIN_NAME 15
#define REQUEST_DNS 6

struct DHCPPayload {
    uint8_t op;
    uint8_t htype;
    uint8_t hlen;
    uint8_t hops;
    uint32_t xid;
    uint16_t secs;
    uint16_t flags;
    uint8_t ciaddr[IPV4_LEN];
    uint8_t yiaddr[IPV4_LEN];
    uint8_t siaddr[IPV4_LEN];
    uint8_t giaddr[IPV4_LEN];
    uint8_t chaddr[MAX_DHCP_CHADDR_LENGTH];
    uint8_t sname[MAX_DHCP_SNAME_LENGTH];
    uint8_t file[MAX_DHCP_FILE_LENGTH];
    uint8_t options[MAX_DHCP_OPTIONS_LENGTH];
}__attribute__((packed));

class DHCP {
private:
    static int stage;

    //static void write_option(uint8_t code, uint8_t*& write_buffer, uint8_t* read_buffer, uint8_t bytes) {
        //*write_buffer = code;

        //write_buffer += bytes;
    //};

    //static void write_type(uint8_t type, ) {

    //}
public:
    static void init();
    static void discovery();
    static void request();
    static void receivePacket();
};

#endif