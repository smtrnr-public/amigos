#ifndef _pci_h_
#define _pci_h_

#include "stdint.h"
#include "ports.h"

// fields are flipped for endianness
// ref: https://wiki.osdev.org/PCI#Header_Type_0x00
typedef struct __attribute__((__packed__))  {
    uint16_t vendor_id;
    uint16_t device_id;
    uint16_t command;
    uint16_t status;
    uint8_t  revision_id;
    uint8_t  prog_if;
    uint8_t  subclass;
    uint8_t  class_code;
    uint8_t  cache_line_size;
    uint8_t  latency_timer;
    uint8_t  header_type;
    uint8_t  BIST;
    uint32_t BAR[6];
    uint32_t cardbus_cis_ptr;
    uint16_t subsystem_vendor_id;
    uint16_t subsystem_id;
    uint32_t expansion_rom_ba;
    uint8_t  capabilities_ptr;
    uint8_t  reserved1[3];
    uint32_t reserved2;
    uint8_t  interrupt_line;
    uint8_t  interrupt_pin;
    uint8_t  min_grant;
    uint8_t  max_latency;


} PCIHeader0;


class PCI {

public:
    static uint16_t pciConfigReadWord (uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset);
    static void pciConfigWriteWord(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset, uint16_t value);
    static void readToHeader(PCIHeader0* head, uint16_t bus, uint8_t slot, uint8_t offset);
    static void readHeader(PCIHeader0* head, uint16_t bus, uint8_t slot, uint32_t bytes);
    static void checkAllBuses();
    static uint32_t getBusDevice(uint16_t vendor_id, uint16_t device_id);
    static void enableBusmastering(PCIHeader0* header, uint16_t vendor_id, uint16_t device_id);
    static void discoverE1000(PCIHeader0* head);
    static uint32_t discoverRTL8139(PCIHeader0* head);
    static uint32_t discoverICH9IntelHDA(PCIHeader0* head);

};


#endif