#ifndef _afg_h_
#define _afg_h_

#include "machine.h"
#include "debug.h"
#include "config.h"
#include "heap.h"

namespace node_commands {
    const uint16_t get_parameter = 0xf00;
    const uint16_t get_selected_input = 0xf01;
    const uint16_t get_connection_entry = 0xf02;
    const uint16_t set_selected_input = 0x701;
    const uint16_t get_stream_channel = 0xf06;
    const uint16_t set_stream_channel = 0x706;
    const uint16_t get_pin_widget_control = 0xf07;
    const uint16_t set_pin_widget_control = 0x707;
    const uint16_t get_volume_control = 0xf0f;
    const uint16_t set_volume_control = 0x70f;
    const uint16_t get_configuration_default = 0xf1c;
    const uint16_t get_converter_channel_count = 0xf2d;
    const uint16_t set_converter_channel_count = 0x72d;
    const uint16_t function_reset = 0x7ff;
    const uint16_t set_amplifier_gain = 0x3;
    const uint16_t get_amplifier_gain = 0xB;
    const uint16_t set_converter_format = 0x2;
}

class AFG {
public: 
    uint32_t codec;
    uint32_t AFGNode;
    uint32_t numWidgets;
    uint8_t* DACs;
    uint8_t numDACs;
    uint8_t* ADCs;
    uint8_t numADCs;
    uint8_t* pinComplexes;
    uint8_t numPinComplexes;
    uint8_t* outputPins;
    uint8_t numOutputPins;
    uint8_t* inputPins;
    uint8_t numInputPins;

    AFG(uint32_t codec_param) {
        Debug::printf("| Setting up AFG\n");
        codec = codec_param;
        findAFG();
        mapAFG();
    }

    void afgInit() {
        DACs = (uint8_t*) malloc(numWidgets);
        ADCs = (uint8_t*) malloc(numWidgets);
        pinComplexes = (uint8_t*) malloc(numWidgets);
        outputPins = (uint8_t*) malloc(numWidgets);
        inputPins = (uint8_t*) malloc(numWidgets);
        numDACs = 0; 
        numADCs = 0;
        numPinComplexes = 0;
        numOutputPins = 0;
        numInputPins = 0;
    }

    void mapAFG();
    void findAFG();
    void getWidgetInfo(uint32_t widget);
    void mute(uint32_t widget, bool mute);
    void ampDetails(uint32_t widget);
    uint16_t getAmpGain(uint32_t widget, uint8_t type, uint32_t connectionIndex);
    void setAmpGain(uint32_t widget, uint32_t gain, bool mute);
};

#endif