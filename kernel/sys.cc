  #include "sys.h"
#include "stdint.h"
#include "idt.h"
#include "debug.h"
#include "process.h"
#include "threads.h"
#include "semaphore.h"
#include "future.h"
#include "descriptor.h"
#include "elf.h"
#include "libk.h"
#include "socket.h"
#include "network.h"

using namespace Descriptor;

class FileDescriptor : public FD {
    Shared<Node> node;
    uint32_t offset = 0;
public:
    FileDescriptor(Shared<Node> node) : node{node} {}

    int len() override { return node->size_in_bytes(); }

    int read(char* buffer, int len) override {
        if (offset > node->size_in_bytes()) { return -1; }
        auto cnt = node->read_all(offset, len, buffer);
        offset += cnt;
        return cnt;
    }

    int write(char* buffer, int n) override {
        if (offset > node -> size_in_bytes())
            return -1;
        if (offset == node -> size_in_bytes()) {
            // we know we're appending to a file
            node -> set_size(node -> size_in_bytes() + n);
        }
        else if(offset + n > node -> size_in_bytes()){
            node -> set_size(node -> size_in_bytes() + ((offset + n) - node -> size_in_bytes()));
        }
        auto cnt = node->write_all(offset, n, buffer);
        offset += cnt;
        return cnt;
    }

    int seek(int offset) override { return this->offset = offset; }
};

class ProcessDescriptor : public PD {
    Shared<Future<int>> exit_status;
public:
    ProcessDescriptor(Shared<Future<int>> exit_status) : exit_status{exit_status} {}

    int wait(uint32_t* status) override {
        *status = exit_status->get();
        return 0;
    }
};

class SemaphoreDescriptor : public SD {
    Semaphore sem;
public:
    SemaphoreDescriptor(uint32_t count) : sem{count} {}

    int up() override {
        sem.up();
        return 0;
    }

    int down() override {
        sem.down();
        return 0;
    }
};

/**
 * Stores time :)
 * 
 * @author Database Function Team
 */
struct Time {
    time_t sec;
    time_t min;
    time_t hour;
    time_t day;
    time_t month;
    time_t year;
};

/**
 * Class for the time system call
 * 
 * @author Database Function Team
 */
class RTC {
private:
    /**
     * Returns CMOS register's value
     * 
     * @param regNum Register number
     * @return       Value in register
     * 
     * @author Database Function Team
     */
    uint8_t getReg(int regNum) {
        outb(0x70, (1 << 7) | regNum);
        pause();
        return (uint8_t) inb (0x71);
    }

    /**
     * Get the number of days given a month; assume not leap year
     * 
     * @param month Month, 1 for January, etc
     * @return      Number of days in the month
     * 
     * @author Database Function Team
     */
    uint32_t daysInMonth(uint32_t month) {
        switch(month) {
            case  1: 
            case  3: 
            case  5:
            case  7:
            case  8:
            case 10:
            case 12: return 31;
            case  2: return 28; // Handles leap year outside
            default: return 30;
        }
    }
    
    /**
     * Gets the current time as a Time struct, in UTC, and binary and 24-hour modes.
     * Binary mode means that if sec == 48, then the time is 48 seconds past.
     * This is in contrast to BCD mode, where it would be denoted as 0x48.
     * 
     * @return The current time as a Time struct
     * 
     * @author Database Function Team
     */
    Time getTime() {
        Time one;
        Time two;
        uint8_t regB = getReg(0x0B);
        bool sync = false;
        while(!sync) {
            uint8_t updateFlag; 
            do { // Checks if time is currently updating
                updateFlag = getReg(0x0A);
            } while(updateFlag & 0x80);
            
            // Update time
            one.sec   = getReg(0x00);
            one.min   = getReg(0x02);
            one.hour  = getReg(0x04);
            one.day   = getReg(0x07);
            one.month = getReg(0x08);
            one.year  = getReg(0x09);
            
            do { // Checks if time is currently updating
                updateFlag = getReg(0x0A);
            } while(updateFlag & 0x80);

            // Update time
            two.sec   = getReg(0x00);
            two.min   = getReg(0x02);
            two.hour  = getReg(0x04);
            two.day   = getReg(0x07);
            two.month = getReg(0x08);
            two.year  = getReg(0x09);

            sync = one.sec == two.sec && one.min == two.min && one.hour == two.hour 
                && one.day == two.day && one.month == two.month && one.year == two.year;
        }
        bool pm_flag = one.hour & 0x80; // Store 24-hour flag
        one.hour = one.hour & 0x7F; // Get rid of 24-hour flag from hour

        if((regB & 0x4) == 0) { // If BCD mode, convert from hex to decimal
            one.sec   = ((one.sec   & 0xF0) >> 1) + ((one.sec   & 0xF0) >> 3) + (one.sec   & 0xF);
            one.min   = ((one.min   & 0xF0) >> 1) + ((one.min   & 0xF0) >> 3) + (one.min   & 0xF);
            one.hour  = ((one.hour  & 0xF0) >> 1) + ((one.hour  & 0xF0) >> 3) + (one.hour  & 0xF); 
            one.day   = ((one.day   & 0xF0) >> 1) + ((one.day   & 0xF0) >> 3) + (one.day   & 0xF);
            one.month = ((one.month & 0xF0) >> 1) + ((one.month & 0xF0) >> 3) + (one.month & 0xF);
            one.year  = ((one.year  & 0xF0) >> 1) + ((one.year  & 0xF0) >> 3) + (one.year  & 0xF);
        }
        
        // Increment year
        one.year += 2000;

        if((regB & 0x2) == 0) { // If 12 hour mode, convert to 24-hour time
            one.hour = pm_flag ? ((one.hour & 0x7F) % 12) + 12 : ((one.hour & 0x7F) % 12);
        }

        return one;
    }
    
public:
    RTC()  = default;
    ~RTC() = default;

    /**
     * Returns the current time as Epoch time, defined as the number
     * of seconds since January 1st, 1970 12:00:00AM UTC
     * 
     * @return The current Epoch time
     * 
     * @author Database Function Team
     */
    time_t getEpochTime() {
        Time t = getTime();
        time_t epochTime = 0;
        time_t leap_days = (t.year - 1968) / 4; // Leap days since 1970 incl. current year
        epochTime += leap_days * 24 * 60 * 60;
        epochTime += (t.year - 1970) * 60 * 60 * 24 * 365; // Years
        epochTime += (t.day - 1) * 24 * 60 * 60; // Days
        epochTime += t.hour * 60 * 60; // Hours
        epochTime += t.min * 60; // Minutes
        epochTime += t.sec; // Seconds
        // Months
        for(int i = 1; i < t.month; i++) {
            epochTime += (time_t) (daysInMonth(i) * 24 * 60 * 60);
        }
        return epochTime;
    }
};



class SocketDescriptor : public SO {
    Socket sock;
public:
    SocketDescriptor(int domain, int type, int protocol) : sock(domain, type, protocol) {}

    // helper function for to find the right socket
    bool mac_match(m_addr* other) override {
        return sock.mac_match(other); 
    }
    int read_data(void* dest, uint32_t data_length) override{
        return sock.read_data(dest, data_length);
    }

    void append_data(void* data, uint32_t data_length) override {
        sock.append_data(data, data_length);
    }

    int bind(const sockaddr* addr, size_t addrlen) override	{ 
        return sock.bind(addr, addrlen); 
    }
	int listen(int backlog) override {
        return sock.listen(backlog);
    }
    int connect(const sockaddr *addr, size_t addrlen) override {
        return sock.connect(addr, addrlen);
    }
	int accept(sockaddr* addr, size_t* addrlen) override {
        return sock.accept(addr, addrlen);
    }
	ssize_t recvfrom(void* buf, size_t len, int flags, 
                     sockaddr* src_addr, size_t* addrlen) override {
        return sock.recvfrom(buf, len, flags, src_addr, addrlen);                 
    }
	ssize_t sendto(const void* buf, size_t len, int flags,
                   const sockaddr* dest_addr, size_t addrlen) override {
        return sock.sendto(buf, len, flags, dest_addr, addrlen);               
    }
	int shutdown(int how) override {
        return sock.shutdown(how);
    }
};

namespace SYS {

inline bool check_range(uint32_t start, uint32_t size) {
    uint32_t lower = start;
    uint32_t upper = start+size;
    return (lower <= upper) && (lower >= 0x80000000) && (upper <= kConfig.localAPIC || lower >= kConfig.localAPIC+4096)	&& (upper <= kConfig.ioAPIC    || lower >= kConfig.ioAPIC+4096);
}

#define GEN(fun) int fun(Shared<PCB>& pcb, uint32_t* stack)

inline uint32_t* getargs(uint32_t* stack) { return (uint32_t*) stack[3] + 1; }

GEN(exit) {
    auto args = getargs(stack);
    pcb->process()->exit_status->set(args[0]);
    pcb.~Shared<PCB>();
    stop();
    return -1;
}

GEN(write) {
    auto args = getargs(stack);
    if (!check_range(args[1], args[2])) return -1;
    return pcb->process()->get_fd(args[0])->write((char*) args[1], (int) args[2]);
}

GEN(fork) {
    return pcb->process()->set_pd([=] {
        auto p = pcb->process()->clone();
        uint32_t pc = stack[0];
        uint32_t esp = stack[3];
        thread(p, [=] { switchToUser(pc, esp, 0); });
        return Shared<PD>{ new ProcessDescriptor{p->process()->exit_status} };
    });
}

GEN(sem) {
    auto args = getargs(stack);
    return pcb->process()->set_sd([=] { return Shared<SD>{new SemaphoreDescriptor{args[0]}}; });
}

GEN(up) {
    auto args = getargs(stack);
    return pcb->process()->get_sd(args[0])->up();
}

GEN(down) {
    auto args = getargs(stack);
    return pcb->process()->get_sd(args[0])->down();
}

GEN(close) {
    auto args = getargs(stack);
    return pcb->process()->close(args[0]);
}

GEN(shutdown) {
    pcb.~Shared<PCB>();
    Debug::shutdown();
    return -1;
}

GEN(wait) {
    auto args = getargs(stack);
    return pcb->process()->wait(args[0], (uint32_t*) args[1]);
}

GEN(execl) {
    auto args = getargs(stack);
    auto proc = pcb->process();
    auto fs = proc->fs;
    auto setCd = Shared<Node>{};
    Shared<Node> file = fs->open(proc->cd, (const char*) args[0], setCd);
    if (file == nullptr) return -1;
    return exec(pcb, file, (const char**) &args[1], setCd);
}

GEN(open) {
    auto args = getargs(stack);
    auto proc = pcb->process();
    auto fs = proc->fs;
    auto dummy = Shared<Node>{};
    Shared<Node> file = fs->open(proc->cd, (const char*) args[0], dummy);
    if (file == nullptr) {
        if (args[1] == 1) {
            
            // // preprocess the path to get the parent directory
            // // call find on the parent directory (exit with error if it doesn't exist)
            // // call create_file on the parent directory
            // create the file
            // change total # of unallocated blocks/inodes (superblock)
            // change # of unallocated blocks/inodes in BGDT
            // make directory entry in parent directory (proc -> cd)
            // allocate the actual block/inode
            const char* name = (const char*)args[0];
            Shared<Node> parentDirectory = fs -> getParent(name, proc -> cd);
            if(parentDirectory == nullptr){
                Debug::printf("***FAILED***\n");
                return -1;
            }
            char* buffer = new char[K::strlen(name) + 1];
            name = fs -> getChild(name, buffer);
            Shared<Node> newFileNode = fs -> create_file(parentDirectory, name);
            delete[] buffer;
            file = newFileNode;
        }
        else
            return -1;
    }
    return proc->set_fd([=] { return Shared<FD>{new FileDescriptor{file}}; });
}

GEN(len) {
    auto args = getargs(stack);
    return pcb->process()->get_fd(args[0])->len();
}

GEN(read) {
    auto args = getargs(stack);
    if (!check_range(args[1], args[2])) return -1;
    return pcb->process()->get_fd(args[0])->read((char*) args[1], (int) args[2]);
}

GEN(seek) {
    auto args = getargs(stack);
    return pcb->process()->get_fd(args[0])->seek((int) args[1]);
}

/**
 * Time system call
 * 
 * Notes:
 * - RTC is already in UTC
 * - Epoch time counts from January 1st, 1970 12:00:00AM UTC
 * - Source: https://wiki.osdev.org/CMOS
 * 
 * @return The current Epoch time
 *
 * @author Database Function Team
 */

GEN(time) {
    RTC rtc{};
    auto theTime = rtc.getEpochTime();

    // Store into buffer, if given
    auto args = getargs(stack);
    time_t *buf = (time_t*) args[0];

    // Security check
    if (buf != nullptr && (uint32_t) buf > 0x80000000 && (uint32_t) buf != kConfig.localAPIC && (uint32_t) buf != kConfig.ioAPIC)
        *buf = theTime;

    return theTime;
}

GEN(unlink) {
    auto args = getargs(stack);
    auto proc = pcb->process();
    auto fs = proc->fs;
    int res = fs->delete_file(proc->cd, (const char*) args[0]) ? 1 : -1;
    return res;
}
GEN(socket) {
    auto args = getargs(stack);
    return set_so((int) args[0], (int) args[1], (int) args[2]);
}

GEN(connect) {
    auto args = getargs(stack);
    return get_so(args[0])->connect((sockaddr*) args[1], (size_t) args[2]);
}

GEN(accept) {
    auto args = getargs(stack);
    return get_so(args[0])->accept((sockaddr*) args[1], (size_t*) args[2]);
}

GEN(sendto) {
    auto args = getargs(stack);
    return get_so(args[0])->sendto((const void*) args[1], 
        (size_t) args[2], (int) args[3], (const sockaddr*) args[4], (size_t) args[5]);
}

GEN(recvfrom) {
    auto args = getargs(stack);
    return get_so(args[0])->recvfrom((void*) args[1], 
        (size_t) args[2], (int) args[3], (sockaddr*) args[4], (size_t*) args[5]);
}

GEN(bind) {
    auto args = getargs(stack);
    return get_so(args[0])->bind((const sockaddr*) args[1], (size_t) args[2]);
}

GEN(listen) {
    auto args = getargs(stack);
    return get_so(args[0])->listen((int) args[1]);
}

GEN(getMAC) {
    auto args = getargs(stack);
    m_addr* addr = (m_addr*) args[0];
    if (addr == nullptr) return -1;
    memcpy(addr, networking::mac_addr, MAC_LEN);
    return 0;
}

#undef GEN
}

typedef int (*syscall)(Shared<PCB>&, uint32_t*);
syscall* syscall_table;

static constexpr uint32_t NUM_SYSCALLS = 63;


extern "C" int sysHandler(uint32_t num, uint32_t stack) {
    if (num >= NUM_SYSCALLS) return -1;
    auto pcb = gheith::current()->pcb;
    auto func = syscall_table[num];
    if (!func) return -1;
    return func(pcb, (uint32_t*) stack);
}

static uint32_t user_stack;

void SYS::init(void) {

    // Initalize syscall table
    syscall_table = new syscall[NUM_SYSCALLS];
    syscall_table[0] = exit;
    syscall_table[1] = write;
    syscall_table[2] = fork;
    syscall_table[3] = sem;
    syscall_table[4] = up;
    syscall_table[5] = down;
    syscall_table[6] = close;
    syscall_table[7] = shutdown;
    syscall_table[8] = wait;
    syscall_table[9] = execl;
    syscall_table[10] = open;
    syscall_table[11] = len;
    syscall_table[12] = read;
    syscall_table[13] = seek;

    syscall_table[14] = time;
    syscall_table[17] = unlink;
    
    syscall_table[41] = socket;
    syscall_table[42] = connect;
    syscall_table[43] = accept;
    syscall_table[44] = sendto;
    syscall_table[45] = recvfrom;
    syscall_table[49] = bind;
    syscall_table[50] = listen;
    syscall_table[51] = getMAC;
    

    user_stack = (kConfig.localAPIC < kConfig.ioAPIC) ? kConfig.localAPIC : kConfig.ioAPIC;

    // Register syscall handler
    IDT::trap(48,(uint32_t)sysHandler_,3);
}

int exec(Shared<PCB>& pcb,
         Shared<Node>& file,
         uint32_t argc,
         const char** argv,
         uint32_t sz,
         Shared<Node> setCd) {

    ELF::Header eh;
    if (!ELF::read_header(file, eh)) { return -1; }

    const uint32_t offset = (argc+1) * sizeof(uint32_t);
    sz += offset;
    char* buffer = new char[sz];
    char* string = buffer + offset;
    for (uint32_t i = 0; i < argc; i++) {
        uint32_t len = K::strlen(argv[i])+1;
        memcpy(string, argv[i], len);
        ((uint32_t*) buffer)[i] = (uint32_t) string - (uint32_t) buffer;
        string += len;
    }
    ((uint32_t*) buffer)[argc] = 0;

    ASSERT(pcb->cr3 == getCR3());
    pcb->process()->reset();
    ASSERT(setCd != nullptr);
    pcb->process()->cd = setCd;

    auto entry = ELF::load(file, eh);

    uint32_t esp = user_stack;
    esp = ((esp-sz-8) & 0xfffffff0)+8;
    memcpy((void*) esp, buffer, sz);
    for (uint32_t i = 0; i < argc; i++) { ((uint32_t*) esp)[i] += esp; }

    ((uint32_t*) esp)[-2] = argc;
    ((uint32_t*) esp)[-1] = esp;

    delete[] buffer;
    pcb.~Shared<PCB>();
    file.~Shared<Node>();
    switchToUser(entry, esp-8, 0);
    return -1;
}

int SYS::exec(Shared<PCB>& pcb, Shared<Node>& file, const char** argv, Shared<Node> setCd) {
    uint32_t argc = 0;
    uint32_t sz = 0;
    for (;; argc++) {
        const char* str = argv[argc];
        if (str) { sz += K::strlen(str)+1; }
        else break;
    }
    return exec(pcb, file, argc, argv, sz, setCd);
}

int SYS::exec(Shared<Node> setCd, Shared<Node>& file, const char* arg, ...) {
    auto pcb = gheith::current()->pcb;
    return exec(pcb, file, &arg, setCd);
}

int SYS::exit(int status) {
    auto pcb = [] { return gheith::current()->pcb; }();
    pcb->process()->exit_status->set(status);
    pcb.~Shared<PCB>();
    stop();
    return -1;
}
