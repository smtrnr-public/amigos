#include "protocols.h"
#include "socket.h"
#include "process.h"

namespace networking {

    uint16_t processed_packets_buffer[PROCESSED_BUFFER_SIZE] = {0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff};
    int buff_index = 0;

    // return false -> already processed, just give up
    // return true  -> in the buffer now, proceed
    bool processPacket(uint16_t id) {
        return true;
        for (int i = 0; i < PROCESSED_BUFFER_SIZE; i++) {
            if (processed_packets_buffer[i] == id) {
                return false;
            }
        }
        // for (int i = 0; i < PROCESSED_BUFFER_SIZE; i++) {
        //     Debug::printf("%d ", processed_packets_buffer[i]);

        // }
        // Debug::printf(" -> ");
        processed_packets_buffer[buff_index] = id;
        buff_index = (buff_index + 1) % PROCESSED_BUFFER_SIZE;
        // for (int i = 0; i < PROCESSED_BUFFER_SIZE; i++) {
        //     Debug::printf("%d ", processed_packets_buffer[i]);

        // }
        // Debug::printf("\n");
        return true;
    }



    uint16_t netChecksum(void *addr, int count)
    {
        uint32_t sum = 0;
        uint16_t* ptr = (uint16_t*) addr;

        while (count > 1)  {
            sum += *ptr++;
            count -= 2;
        }

        if (count > 0) {
            sum += *(uint8_t*) ptr;
        }

        while (sum >> 16) {
            sum = (sum & 0xffff) + (sum >> 16);
        }
        
        return ~sum;
    }


    /* FOR LITTLE-ENDIAN HOST, CHANGE IF HOST BIG-ENDIAN */
    uint16_t htons(uint16_t host) {
        return (host >> 8) | ((host << 8) & 0xff00);
    }

    uint16_t ntohs(uint16_t host) {
	    return htons(host);
    }

    uint32_t htonl(uint32_t host) {
        return ((host & 0x000000ff) << 24) | ((host & 0x0000ff00) << 8) | ((host & 0x00ff0000) >> 8) | ((host & 0xff000000) >> 24);
    }

    uint32_t ntohl(uint32_t host) {
	    return htonl(host);
    }

    uint16_t IPPacketID = 0;
    uint16_t AOSPPacketID = 0;
}

int UDP::receivePacket(void* packet, uint32_t length) {
    using namespace networking;

    UDPFrame* frame = (UDPFrame*) packet;

    auto checksum = frame->checksum;
    frame->checksum = 0;
    if (frame->checksum && frame->checksum != netChecksum(packet, frame->length)) {
        return -1;
    }
    frame->checksum = checksum;

    MISSING();
    return (int) frame->data;
}

int UDP::sendPacket(void* payload, uint32_t length, uint16_t src, uint16_t dst) {
    using namespace networking;

    UDPFrame* head = (UDPFrame*) malloc(length + UDP_HEADER_SIZE);
    char* addr     = (char*) head;
    memcpy((void*)(addr + UDP_HEADER_SIZE), payload, length);
    free(payload);

    head->srcPort  = src;
    head->dstPort  = dst;
    head->length   = length; 
    head->checksum = 0; // i think this is fine

    return IPV4::sendPacket((void*)head, length + UDP_HEADER_SIZE);
}


int IPV4::receivePacket(void* packet, uint32_t length) {
    using namespace networking;
    
    IPV4Frame* frame = (IPV4Frame*) packet;
    
    if (frame->version != 4) {
        return -1;
    }

    auto checksum = frame->checksum;
    frame->checksum = 0;
    if (frame->headerLen < 5 || checksum != netChecksum(packet, frame->headerLen)) {
        return -1;
    }
    frame->checksum = checksum;

    if (frame->protocol == ICMP_TYPE) {
        return ICMP::receivePacket(((uint32_t*) frame) + frame->headerLen, length - frame->headerLen);
    } else if (frame->protocol == UDP_TYPE) {
        return UDP::receivePacket(((uint32_t*) frame) + frame->headerLen, length - frame->headerLen);
    } else {
        return -1;
    }
}


int IPV4::sendPacket(void* payload, uint32_t length) {
    using namespace networking;

    IPV4Frame* frame = (IPV4Frame*) payload;
    // Assign a unique ID to the outgoing packet.
    auto id = IPPacketID++;
    frame->ident = id;
    frame->version = 4;
    frame->headerLen = 5;
    frame->serviceType = 0x00;
    frame->dataLen = length + frame->headerLen;
    frame->ttl = 0x40;
    frame->checksum = netChecksum(frame, frame->headerLen);
    // TODO: get MAC from ARP if not in table already
    // After arp, uint16_t targetHWType, uint32_t targetIP, unsigned char* mac
    unsigned char MAC[6];
    bool validMac = ARP::get_MAC_arp(ETH_TYPE, frame -> destAddr, (unsigned char*) MAC);
    if (validMac){
        Ethernet::sendPacket(IPV4_TYPE, (uint8_t*) MAC, frame, frame->dataLen);
    }
    else{
        // wait for MAC through ARP request?
        // need to get IP of the one we want an request it then wait
    }
    return frame->dataLen;
}


int AOSP::receivePacket(void* packet, uint32_t length) {
    using namespace networking;
    // interpret the AOSP ethernet message
    AOSPMessage* msg = (AOSPMessage*)packet;

    // Debug::printf("Dest: %x:%x:%x:%x:%x:%x\n", msg->macDestination[0], msg->macDestination[1], msg->macDestination[2], msg->macDestination[3], msg->macDestination[4], msg->macDestination[5]);
    // Debug::printf("Src:  %x:%x:%x:%x:%x:%x\n", msg->macSource[0], msg->macSource[1], msg->macSource[2], msg->macSource[3], msg->macSource[4], msg->macSource[5]);
    // Debug::printf("Type: %d\n", msg->ethernetType);
    // Debug::printf("Src Port: %d, Dest Port: %d\n", msg->aospFrame.sourcePort, msg->aospFrame.destPort);
    // Debug::printf("Checksum: %d\n", msg->aospFrame.checksum);
    // Debug::printf("Packet ID: %d\n", msg->aospFrame.packetID);
    // Debug::printf("Data: %s\n\n", (uint32_t*)msg->aospFrame.data);

    // LOOKUP SOCKET IN PORT TABLE AND WRITE TO IT

    // auto pcb = gheith::current()->pcb;
    // auto proc = pcb->process();
        // Debug::printf("HERE %x %x\n", pcb, proc);

    bool res = processPacket(msg->aospFrame.packetID);
    if (res) {
        // Debug::printf("processed packet %d", msg->aospFrame.packetID);
        add_to_socket((m_addr*)(msg->macSource), (void*) msg->aospFrame.data, msg->aospFrame.dataLength);
    } else {
        return -1;
    }

    return (int) packet;
}

/* NEED TO GET PORTS AT SOME POINT */
int AOSP::sendPacket(void* payload, uint32_t length, uint8_t* dest_mac) {
    using namespace networking;

    AOSPMessage* msg = (AOSPMessage*) malloc(sizeof(AOSPMessage) + length);
    for (int i = 0; i < MAC_LEN; i++) {
        msg->macSource[i] = mac_addr[i];
        msg->macDestination[i] = dest_mac[i];
    }
    msg->ethernetType = AOSP_TYPE;
    msg->aospFrame.sourcePort = msg->aospFrame.destPort = 0; // unused atm
    msg->aospFrame.checksum = 0; // zeroing out memory so it doesn't affect checksum
    msg->aospFrame.packetID = AOSPPacketID++;
    msg->aospFrame.dataLength = length;
    memcpy(msg->aospFrame.data, payload, length);
    msg->aospFrame.checksum = netChecksum(msg, sizeof(AOSPMessage) + length);

    // Debug::printf("Dest: %x:%x:%x:%x:%x:%x\n", msg->macDestination[0], msg->macDestination[1], msg->macDestination[2], msg->macDestination[3], msg->macDestination[4], msg->macDestination[5]);
    // Debug::printf("Src:  %x:%x:%x:%x:%x:%x\n", msg->macSource[0], msg->macSource[1], msg->macSource[2], msg->macSource[3], msg->macSource[4], msg->macSource[5]);
    // Debug::printf("Type: %d\n", msg->ethernetType);
    // Debug::printf("Src Port: %d, Dest Port: %d\n", msg->aospFrame.sourcePort, msg->aospFrame.destPort);
    // Debug::printf("Checksum: %d\n", msg->aospFrame.checksum);
    // Debug::printf("Packet ID: %d\n", msg->aospFrame.packetID);
    // Debug::printf("Data: %s\n", (uint32_t*)msg->aospFrame.data);
    // Debug::printf("Length: %u\n\n", msg->aospFrame.dataLength);

    RTL8139::sendPacket((uint32_t) msg, sizeof(AOSPMessage) + length);
    free(msg);
    return length;
}

int Ethernet::receivePacket(void* packet, uint32_t length) {
    using namespace networking;
    // interpret the ethernet frame
    EthernetFrame* frame = (EthernetFrame*)packet;

    // (48 + 48 + 16) bits / 8 bits-per-byte = 14 bytes
    uint32_t payload_start = ((uint32_t) packet) + 14;

    // Debug::printf("Dest: %x:%x:%x:%x:%x:%x\n", frame->macDestination[0], frame->macDestination[1], frame->macDestination[2], frame->macDestination[3], frame->macDestination[4], frame->macDestination[5]);
    // Debug::printf("Src:  %x:%x:%x:%x:%x:%x\n", frame->macSource[0], frame->macSource[1], frame->macSource[2], frame->macSource[3], frame->macSource[4], frame->macSource[5]);
    // Debug::printf("Type: %d\n", frame->ethernetType);
    // Debug::printf("Data: %s\n\n", (uint32_t*)payload_start);

    // an assertion to check if the mac address matches this OS'?
    if (frame->ethernetType == ARP_TYPE) {
        return ARP::receivePacket((uint32_t*)payload_start, length - 14);
    } else if (frame->ethernetType == IPV4_TYPE) {
        return IPV4::receivePacket((uint32_t*)payload_start, length - 14);
    } else if (frame->ethernetType == AOSP_TYPE) {
        return AOSP::receivePacket(packet, length);
    } else if (frame->ethernetType == ETH_TYPE) {
        add_to_socket((m_addr*)(frame->macSource), (uint32_t*)payload_start, length - 14);
    } else {
        // ignore the protocol
        // MISSING();
    }
    return -1;
}

int Ethernet::sendPacket(uint16_t ether_type, uint8_t* dest_mac, void* payload, uint32_t bytes) {
    using namespace networking;

    uint32_t packet_size = ETHERNET_HEADER_SIZE + bytes;
    EthernetFrame* frame = (EthernetFrame*) malloc(packet_size);

    memcpy(frame->macDestination, dest_mac, MAC_LEN);
    memcpy(frame->macSource, mac_addr, MAC_LEN);
    frame->ethernetType = htons(ether_type);
    memcpy(frame->payload, payload, bytes);

    RTL8139::sendPacket((uint32_t) frame, packet_size);
    // TODO: ensure packet is sent before freeing
    free(frame);

    return packet_size;
}

void Ethernet::waitForMAC(uint8_t* source_mac) {
    // TODO
}
