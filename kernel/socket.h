#ifndef _SOCKET_H_
#define _SOCKET_H_

#include "stdint.h"
#include "net_consts.h"
#include "queue.h"
#include "atomic.h"
#include "protocols.h"
#include "libk.h"
#include "blocking_lock.h"

struct sockaddr {
    uint32_t  sa_family;
    char      sa_data[14];
};

struct m_addr {
    uint8_t smac_addr[MAC_LEN];
};

struct sockaddr_mac {
    int16_t smac_family;
    uint16_t smac_port = 0;
    m_addr smac_addr;
    uint8_t smac_zero[6];
};

sockaddr_mac* textToMac(const char* text);



class Socket {

    uint16_t port;
    m_addr addr;
    void* data;
    uint32_t offset;

    //BlockingLock recieve_lock;
    Semaphore dataLock;
    volatile uint32_t bytes_avail;
    void* start_of_avail;
    volatile uint32_t needed;
public:
    Socket(int domain, int type, int protocol);


    // helper method to see if two mac addresses are the same
    bool mac_match(m_addr* other);

    void append_data(void* data, uint32_t data_length);

    int read_data(void* dest, uint32_t data_length);

    int bind(const sockaddr* addr, size_t addrlen);

    int listen(int backlog);
	int connect(const sockaddr *addr, size_t addrlen);
	int accept(sockaddr* addr, size_t* addrlen);
	ssize_t recvfrom(void* buf, size_t len, int flags, sockaddr* src_addr, size_t* addrlen);
	ssize_t sendto(const void* buf, size_t len, int flags,
                   const sockaddr* dest_addr, size_t addrlen);
	int shutdown(int how);    
};

int set_so(int domain, int type, int protocol);
Socket* get_so(int sockfd);
void sock_init();
int find_socket(m_addr* arrived);
int find_none_socket();
int add_to_socket(m_addr* arrived, void* data, uint32_t data_length);
int read_from_socket(void* buf, uint32_t len, m_addr* source_mac);

extern Socket** sot;

#endif

