#ifndef _hda_h_
#define _hda_h_

#include "pci.h"
#include "machine.h"
#include "debug.h"
#include "config.h"
#include "heap.h"

struct RIRBEntry {
    uint32_t response; 
    uint32_t respEx;
};

class HDA {
public:
    static void init(); 
    static void findDevice();
    static void discoverCodec();
    static void initCORB();
    static void initRIRB();
    static void initDMA();
    static void startDMA();
    static void printRegs();
    static uint32_t commandResponse(uint8_t codec, uint8_t node, uint32_t command, uint32_t data);
    static void issueCommand(uint8_t codec, uint8_t node, uint32_t command, uint32_t data);

    struct hdaDeviceRegs {
        
        uint16_t GCAP;
        uint8_t VMIN;
        uint8_t VMAJ;
        uint16_t OUTPAY;
        uint16_t INPAY;

        uint32_t GCTL;
        uint16_t WAKEEN;
        uint16_t STATESTS;
        uint16_t GSTS;
        uint8_t reserved1[6];
    
        uint16_t OUTSTRMPAY;
        uint16_t INSTRMPAY;
        uint32_t reserved2;

        uint32_t INTCTL;
        uint32_t INTSTS;
        uint64_t reserved3;

        uint32_t WALCLK;
        uint32_t reserved4;
        uint32_t SSYNC;
        uint32_t reserved5;

        uint32_t CORBLBASE;
        uint32_t CORBUBASE;
        uint16_t CORBWP;
        uint16_t CORBRP;
        uint8_t CORBCTL; 
        uint8_t CORBSTS;
        uint8_t CORBSIZE;
        uint8_t reserved6;

        uint32_t RIRBLBASE;
        uint32_t RIRBUBASE;
        uint16_t RIRBWP;
        uint16_t RIRBCNT;
        uint8_t RIRBCTL;
        uint8_t RIRBSTS;
        uint8_t RIRBSIZE;
        uint8_t reserved7;

        uint32_t ICOI;
        uint32_t ICII;
        uint16_t ICIS;
        uint8_t reserved8[6];
        uint32_t DPLBASE;
        uint32_t DPUBASE;
        uint64_t reserved9;
    };
};

extern void printMem(void* start, uint32_t length);
extern void waitms(uint16_t ms);

#endif