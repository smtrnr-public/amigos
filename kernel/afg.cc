#include "afg.h"
#include "hda.h"

void AFG::findAFG() {
    uint32_t response = HDA::commandResponse(codec, 0, node_commands::get_parameter, 0x04);   
    //Debug::printf("Response: %x\n", response);
    uint32_t functionGroups = response & 0xFF;
    uint32_t startingNode = (response >> 16) & 0xFF;
    AFGNode = 0;
    for (unsigned i = 0; i < functionGroups; i++) {
        response = HDA::commandResponse(codec, startingNode + i, node_commands::get_parameter, 0x05);
        uint32_t nodeType = response & 0xFF;
        //Debug::printf("Node %d is of type: %x\n", startingNode + i, nodeType);
        if(nodeType == 1) {
            AFGNode = startingNode + i;
            break;
        }
    }

    if(AFGNode == 0) Debug::panic("AFG not found");
}

void AFG::mapAFG() {
    uint32_t response = HDA::commandResponse(codec, AFGNode, node_commands::get_parameter, 0x04);
    numWidgets = response & 0xFF;
    afgInit();
    uint32_t startingWidget = (response >> 16) & 0xFF;
    //Debug::printf("numWidgets: %x, startingWidget: %x\n", numWidgets, startingWidget);
    Debug::printf("| Num widgets: %d\n", numWidgets);
    for (unsigned i = 0; i < numWidgets; i++) {
        getWidgetInfo(startingWidget + i);
    }
}

void AFG::setAmpGain(uint32_t widget, uint32_t gain, bool mute) {
    uint32_t payload = 0xf000;
    payload |= mute << 7;
    payload += gain;
    HDA::commandResponse(codec, widget, node_commands::set_amplifier_gain, payload);
}

uint16_t AFG::getAmpGain(uint32_t widget, uint8_t type, uint32_t connectionIndex) { // type == 1 for DAC and 0 for ADC
    uint32_t payload = 0;
    if(type == 1) {
        payload = 0x2 << 14;
    }
    else {
        payload |= connectionIndex & 0xf;
    }
    uint32_t rightChannel = HDA::commandResponse(codec, widget, node_commands::get_amplifier_gain, payload);
    // Debug::printf("Right Channel Gain: %x, Right Channel Mute(1 == mute): %d\n", rightChannel & 0x7f, rightChannel >> 7);
    payload |= 0x1 << 13;
    uint32_t leftChannel = HDA::commandResponse(codec, widget, node_commands::get_amplifier_gain, payload);
    // Debug::printf("Left Channel Gain: %x, Left Channel Mute(1 == mute): %d\n", leftChannel & 0x7f, leftChannel >> 7);
    return (leftChannel << 8) | (rightChannel & 0xff);
}

void AFG::getWidgetInfo(uint32_t widget) {
    //Debug::printf("-------------------------------\n");
    //Debug::printf("widget info for node: %d\n", widget);
    // widget type 9h
    uint32_t response = HDA::commandResponse(codec, widget, node_commands::get_parameter, 0x09);
    //Debug::printf("Audio Widget Capabilities: %08x\n", response);
    
    uint32_t widgetType = (response >> 20) & 0xF;
    //Debug::printf("Widget Type: %01x\n", widgetType);

    switch (widgetType) {
        // output 
        case 0: {
            DACs[numDACs] = widget;
            numDACs++;

            // output amplifier details 12h
            response = HDA::commandResponse(codec, widget, node_commands::get_parameter, 0x12);
            //Debug::printf("output amplifier details: %x\n", response);

            // output format set
            HDA::commandResponse(codec, widget, node_commands::set_converter_format, 0x4110);
            response = HDA::commandResponse(codec, widget, 0xA, 0);
            //Debug::printf("output format details: %x\n", response);

            uint16_t gain = getAmpGain(widget, 1, 0);
            uint16_t leftGain = gain >> 8;
            //uint16_t rightGain = gain & 0xff; ?????
            //Debug::printf("Left Channel Gain: %x, Left Channel Mute(1 == mute): %d\n", leftGain & 0x7f, leftGain >> 7);
            //Debug::printf("Right Channel Gain: %x, Right Channel Mute(1 == mute): %d\n", rightGain & 0x7f, rightGain >> 7);

            setAmpGain(widget, leftGain & 0x7f, 0);

            gain = getAmpGain(widget, 1, 0);
            leftGain = gain >> 8;
            //rightGain = gain & 0xff;
            //Debug::printf("Left Channel Gain: %x, Left Channel Mute(1 == mute): %d\n", leftGain & 0x7f, leftGain >> 7);
            //Debug::printf("Right Channel Gain: %x, Right Channel Mute(1 == mute): %d\n", rightGain & 0x7f, rightGain >> 7);

            // output stream channel set
            HDA::issueCommand(codec, widget, node_commands::set_stream_channel, 0x10);
            response = HDA::commandResponse(codec, widget, node_commands::get_stream_channel, 0x0);
            //Debug::printf("output stream channel: %x\n", response);
            break;   
        }
        
        // input
        case 1: {
            ADCs[numADCs] = widget;
            numADCs++;

            // input amplifier details Dh
            response = HDA::commandResponse(codec, widget, node_commands::get_parameter, 0xD);
            //Debug::printf("input amplifier details: %x\n", response);

            // input format set
            HDA::issueCommand(codec, widget, node_commands::set_converter_format, 0x4110);
            response = HDA::commandResponse(codec, widget, 0xA, 0);
            //Debug::printf("input format details: %x\n", response);

            uint16_t gain = getAmpGain(widget, 0, 0);
            uint16_t leftGain = gain >> 8;
            //uint16_t rightGain = gain & 0xff;
            //Debug::printf("Left Channel Gain: %x, Left Channel Mute(1 == mute): %d\n", leftGain & 0x7f, leftGain >> 7);
            //Debug::printf("Right Channel Gain: %x, Right Channel Mute(1 == mute): %d\n", rightGain & 0x7f, rightGain >> 7);

            setAmpGain(widget, leftGain & 0x7f, 0);

            gain = getAmpGain(widget, 0, 0);
            leftGain = gain >> 8;
            //rightGain = gain & 0xff;
            //Debug::printf("Left Channel Gain: %x, Left Channel Mute(1 == mute): %d\n", leftGain & 0x7f, leftGain >> 7);
            //Debug::printf("Right Channel Gain: %x, Right Channel Mute(1 == mute): %d\n", rightGain & 0x7f, rightGain >> 7);

            // input stream channel set
            HDA::issueCommand(codec, widget, node_commands::set_stream_channel, 0x22);
            response = HDA::commandResponse(codec, widget, node_commands::get_stream_channel, 0x0);
            //Debug::printf("input stream channel: %x\n", response);
            break;
        }

        // pin complex
        case 4: {
            pinComplexes[numPinComplexes] = widget;
            numPinComplexes++;

            // pin capabilities Ch
            response = HDA::commandResponse(codec, widget, node_commands::get_parameter, 0xC);
            //Debug::printf("pin capabilities: %x\n", response);

            // configuration F1Ch
            response = HDA::commandResponse(codec, widget, node_commands::get_configuration_default, 0);
            //Debug::printf("configuration: %x\n", response);

            // config details
            //Debug::printf("devices attached: %x\n", response >> 30);
            //Debug::printf("integrated device/device to plug in socket: %x\n", (response >> 20) & 0xF);

            // output
            if (((response >> 20) & 0xF) == 1) {
                outputPins[numOutputPins] = widget;
                numOutputPins++;
                HDA::issueCommand(codec, widget, node_commands::set_pin_widget_control, 0x20);
                response = HDA::commandResponse(codec, widget, node_commands::get_pin_widget_control, 0x0);
                //Debug::printf("output pin enabled: %x\n", (response >> 6) & 0x1);
            }
            
            // mic in
            if (((response >> 20) & 0xF) == 0xA) {
                inputPins[numInputPins] = widget;
                numInputPins++;
                HDA::issueCommand(codec, widget, node_commands::set_pin_widget_control, 0x10);
                response = HDA::commandResponse(codec, widget, node_commands::get_pin_widget_control, 0x0);
                //Debug::printf("input pin enabled: %x\n", (response >> 5) & 0x1);
            }

            // enable BTL and EAPD 
            HDA::issueCommand(codec, widget, 0x70c, 0x3);
            response = HDA::commandResponse(codec, widget, 0xf0c, 0x3);
            //Debug::printf("EAPD + BTL: %x\n", response);       

            break;
        }
        
        default: {
            //Debug::printf("unknown widget type: not input/output/pin complex\n");
            break;
        }
    }

    // volume knob capabilities 13h
    response = HDA::commandResponse(codec, widget, node_commands::get_parameter, 0x13);
    //Debug::printf("volume knob capabilities: %x\n", response);

    // connection list length Eh
    uint32_t numEntries = HDA::commandResponse(codec, widget, node_commands::get_parameter, 0xE);
    //Debug::printf("connection list length: %x\n", numEntries);

    for (unsigned i = 0; i < numEntries; i++) {
        // connections F02h
        response = HDA::commandResponse(codec, widget, node_commands::get_connection_entry, i);
        //Debug::printf("connections: %x\n", response);
    }
}
