#ifndef _network_h_
#define _network_h_

#include "u8250.h"
#include "pci.h"
#include "ports.h"
#include "idt.h"
#include "machine.h"
#include "debug.h"

#define MAX_SIZE_PACKET 1792

namespace networking {
    extern uint8_t mac_addr[];
}

extern "C" void RTL8139_handler();


class RTL8139 {
public:

    static void init(); 

    static void init_mac_addr();
    static void sendPacket(uint32_t addr, uint32_t bytes);
    static void sendPayloadTo(uint16_t protocol, uint8_t* dest_mac, void* payload, uint32_t bytes);
    static void broadcastPayload(uint16_t protocol, void* payload, uint32_t bytes);
    static void receivePacket();
};


#endif