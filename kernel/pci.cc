#include "pci.h"
#include "debug.h"

uint16_t PCI::pciConfigReadWord (uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset) {
    uint32_t address;
    uint32_t lbus  = (uint32_t)bus;
    uint32_t lslot = (uint32_t)slot;
    uint32_t lfunc = (uint32_t)func;
    uint16_t tmp = 0;
 
    /* create configuration address as per Figure 1 */
    address = (uint32_t)((lbus << 16) | (lslot << 11) |
              (lfunc << 8) | (offset & 0xfc) | ((uint32_t)0x80000000));
 
    /* write out the address */
    outl(0xCF8, address);
    /* read in the data */
    /* (offset & 2) * 8) = 0 will choose the first word of the 32 bits register */
    tmp = (uint16_t)((inl(0xCFC) >> ((offset & 2) * 8)) & 0xffff);
    return (tmp);
}

void PCI::pciConfigWriteWord(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset, uint16_t value) {
    uint32_t address;
    uint32_t lbus  = (uint32_t)bus;
    uint32_t lslot = (uint32_t)slot;
    uint32_t lfunc = (uint32_t)func;
 
    /* create configuration address as per Figure 1 */
    address = (uint32_t)((lbus << 16) | (lslot << 11) |
              (lfunc << 8) | (offset & 0xfe) | ((uint32_t)0x80000000));

    Ports::outportl(0xCF8, address);

    Ports::outportw(0xCFC, value);
}

void PCI::readToHeader(PCIHeader0* head, uint16_t bus, uint8_t slot, uint8_t offset) {
    uint32_t address;
    uint32_t lbus    = (uint32_t)bus;
    uint32_t lslot   = (uint32_t)slot;
    uint32_t loffset = (uint32_t)offset;
    uint32_t tmp     = 0;

    address = (uint32_t)((lbus << 16) | (lslot << 11) |
              (loffset & 0xfc) | ((uint32_t)0x80000000));
    
    outl(0xCF8, address);
    tmp = inl(0xCFC);
    
    uint32_t headAddr = (uint32_t) (head ) + loffset;
    // Debug::printf("writing %d to %x\n", tmp, headAddr);
    *((uint32_t*)(headAddr)) = tmp;

}

void PCI::readHeader(PCIHeader0* head, uint16_t bus, uint8_t slot, uint32_t bytes) {
    for (uint32_t i = 0; i < bytes && i < 256; i+=4) {
        readToHeader(head, bus, slot, (uint8_t) i);
    }
}

void PCI::checkAllBuses() {
    for(uint16_t bus = 0; bus < 256; bus++) {
        for(uint8_t device = 0; device < 32; device++) {
            uint16_t vendorID = pciConfigReadWord(bus, device, 0, 0);
            if (vendorID != 0xFFFF) {
                uint16_t deviceID = pciConfigReadWord(bus, device, 0, 2);
                // Debug::printf("bus: %d, device: %d\n", bus, device);
                Debug::printf("vendor: 0x%04x, device: 0x%04x\n", vendorID, deviceID);
            }
        }
    }
}

// bytes 0-7 are the device, 8-23 are the bus
uint32_t PCI::getBusDevice(uint16_t vendor_id, uint16_t device_id) {
    for(uint16_t bus = 0; bus < 256; bus++) {
        for(uint8_t device = 0; device < 32; device++) {
            uint16_t vendorID = pciConfigReadWord(bus, device, 0, 0);
            if (vendorID != 0xFFFF) {
                uint16_t deviceID = pciConfigReadWord(bus, device, 0, 2);
                if (vendor_id == vendorID && device_id == deviceID) {
                    //Debug::printf("Found PCI device(vendor_id: 0x%x, device_id: 0x%x)\n", vendor_id, device_id);
                    uint32_t value = (bus << 8) | device;
                    return value;
                }
            }
        }
    }
    Debug::panic("Could not find PCI device(vendor_id: 0x%x, device_id: 0x%x)\n", vendor_id, device_id);
    return 0;
}

void PCI::enableBusmastering(PCIHeader0* header, uint16_t vendor_id, uint16_t device_id) {
    uint32_t busdev = PCI::getBusDevice(vendor_id, device_id);

    uint16_t bus = busdev >> 8;
    uint8_t device = busdev & 0xFF;

    if(!(header->command & 0x4))
        PCI::pciConfigWriteWord(bus, device, 0, 0x4, PCI::pciConfigReadWord(bus, device, 0, 0x4) | 0x4);
}

void PCI::discoverE1000(PCIHeader0* head) {
    uint32_t busdev = PCI::getBusDevice(0x8086, 0x100e);
    uint16_t bus   = busdev >> 8;
    uint8_t device = busdev & 0xFF;
    PCI::readHeader(head, bus, device, sizeof(PCIHeader0));

    //Debug::printf("device_id: %x  vendor_id: %x\nstatus: %x  command: %x\nclass_code: %x  subclass: %x  prog_if: %x  revision_id: %x\nBIST: %x  header_type: %x  latency_timer: %x  cache_line_size: %x\n", 
    //        head->device_id, head->vendor_id, head->status, head->command, head->class_code, head->subclass, head->prog_if, head->revision_id, head->BIST, head->header_type, head->latency_timer, head->cache_line_size);
    //for (int i = 0; i < 6; i++) {
    //    Debug::printf("bar[%d] = %x\n", i, head->BAR[i]);
    //}
}

uint32_t PCI::discoverRTL8139(PCIHeader0* head) {
    uint32_t busdev = PCI::getBusDevice(0x10ec, 0x8139);
    uint16_t bus   = busdev >> 8;
    uint8_t device = busdev & 0xFF;
    PCI::readHeader(head, bus, device, sizeof(PCIHeader0));

    //Debug::printf("device_id: %x  vendor_id: %x\nstatus: %x  command: %x\nclass_code: %x  subclass: %x  prog_if: %x  revision_id: %x\nBIST: %x  header_type: %x  latency_timer: %x  cache_line_size: %x\n", 
    //        head->device_id, head->vendor_id, head->status, head->command, head->class_code, head->subclass, head->prog_if, head->revision_id, head->BIST, head->header_type, head->latency_timer, head->cache_line_size);
    //Debug::printf("interrupt_pin: %x  interrupt_line: %x\n", head->interrupt_pin, head->interrupt_line);
    //for (int i = 0; i < 6; i++) {
    //    Debug::printf("bar[%d] = %x\n", i, head->BAR[i]);
    //}
    return busdev;
}

uint32_t PCI::discoverICH9IntelHDA(PCIHeader0* head) {
    uint32_t busdev = PCI::getBusDevice(0x8086, 0x293e);
    //uint32_t busdev = PCI::getBusDevice(0x8086, 0x2668);
    uint16_t bus = busdev >> 8;
    uint8_t device = busdev & 0xFF;
    PCI::readHeader(head, bus, device, sizeof(PCIHeader0));
    /*
    Debug::printf("device_id: %x  vendor_id: %x\nstatus: %x  command: %x\nclass_code: %x  subclass: %x  prog_if: %x  revision_id: %x\nBIST: %x  header_type: %x  latency_timer: %x  cache_line_size: %x\n", 
            head->device_id, head->vendor_id, head->status, head->command, head->class_code, head->subclass, head->prog_if, head->revision_id, head->BIST, head->header_type, head->latency_timer, head->cache_line_size);
    Debug::printf("interrupt_pin: %x  interrupt_line: %x\n", head->interrupt_pin, head->interrupt_line);
    for (int i = 0; i < 6; i++) {
        Debug::printf("bar[%d] = %x\n", i, head->BAR[i]);
    }
    */
    return busdev;
}