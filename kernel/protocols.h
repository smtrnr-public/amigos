#ifndef _protocols_h_
#define _protocols_h_

#include "u8250.h"
#include "pci.h"
#include "ports.h"
#include "idt.h"
#include "machine.h"
#include "debug.h"
#include "arp.h"
#include "heap.h"

#define ETH_TYPE 0x0000
#define ARP_TYPE  0x0806
#define IPV4_TYPE 0x0800
#define AOSP_TYPE 0xDEAD //lmao
#define ICMP_TYPE 0x01
#define UDP_TYPE 0x11
#define TCP_TYPE 0x06

namespace networking {
    struct IPV4Frame {
        uint8_t headerLen : 4;
        uint8_t version : 4;
        uint8_t serviceType;
        uint16_t dataLen;
        uint16_t ident;
        uint16_t flags : 3;
        uint16_t fragOffset : 13;
        uint8_t ttl;
        uint8_t protocol;
        uint16_t checksum;
        uint32_t sourceAddr;
        uint32_t destAddr;
        uint8_t data[];
    }__attribute__((packed));

    struct ICMPFrame {
        uint8_t type;
        uint8_t code;
        uint16_t checksum;
        uint8_t data[];
    }__attribute__((packed));

    struct UDPFrame {
        uint16_t srcPort;
        uint16_t dstPort;
        uint16_t length;
        uint16_t checksum;
        uint8_t data[];
    }__attribute__((packed));

    struct EthernetFrame {
        uint8_t macDestination[6];
        uint8_t macSource[6];
        uint16_t ethernetType;
        uint8_t payload[];
    }__attribute__((packed));

    struct AOSPFrame {
        uint16_t sourcePort; // PORTS ARE OPTIONAL, NOT USED RN BUT MAY BE USEFUL LATER
        uint16_t destPort;
        uint16_t checksum;  // CHECKSUM OF HEADER AND DATA, NOT USED RN BUT MAY BE USEFUL LATER
        uint16_t packetID;  // UNIQUE ID OF PACKET
        uint16_t dataLength;  // LENGTH OF DATA (NOT INCLUDING HEADER)
        uint8_t data[];  // PAYLOAD
    }__attribute__((packed));

    struct AOSPMessage {
        uint8_t macDestination[6];
        uint8_t macSource[6];
        uint16_t ethernetType;
        AOSPFrame aospFrame;
    }__attribute__((packed));

    /*
     * Compute Internet Checksum for "count" bytes beginning at location "addr".
     * Taken and modified from https://tools.ietf.org/html/rfc1071
     */
    uint16_t netChecksum(void *addr, int count);

    /* FOR LITTLE-ENDIAN HOST, CHANGE IF HOST BIG-ENDIAN */
    uint16_t htons(uint16_t host);
    uint16_t ntohs(uint16_t host);
    uint32_t htonl(uint32_t host);
    uint32_t ntohl(uint32_t host);
}

class DHCP{
private:
public:
    static int receivePacket(void* packet, uint32_t length){ MISSING(); return -1; }
    static int sendPacket(void* payload, uint32_t length)  { MISSING(); return -1; }
}; 

class UDP{
private:
public:
    static int receivePacket(void* packet, uint32_t length);
    static int sendPacket(void* payload, uint32_t length, uint16_t src, uint16_t dst);
};

class ICMP {
private:
public:
    static int receivePacket(void* packet, uint32_t length) {
        MISSING();
        return -1;
    }
};

class IPV4 {
private:
public:
    static int receivePacket(void* packet, uint32_t length);
    static int sendPacket(void* payload, uint32_t length);
};

// amigOS Protocol
class AOSP {
private:
public:
    static int receivePacket(void* packet, uint32_t length);
    static int sendPacket(void* payload, uint32_t length, uint8_t* dest_mac);
    static void init();
};

class Ethernet {
private:
public:
    static int receivePacket(void* packet, uint32_t length);
    static int sendPacket(uint16_t protocol, uint8_t* dest_mac, void* payload, uint32_t bytes);
    static void waitForMAC(uint8_t* source_mac);
};

#endif