#ifndef _net_consts_h
#define _net_consts_h

#define RTL8139_ioAPIC_entry 11
#define RTL8139_vector 36

#define ETHERNET_HEADER_SIZE 14
#define UDP_HEADER_SIZE 8

#define PROCESSED_BUFFER_SIZE 10
#define RX_BUFFER_SIZE 9708

#define SOT_SIZE 100

#define MAC_LEN 6
#define IPV4_LEN 4
#define DHCP_CLIENT_PORT 68
#define DHCP_SERVER_PORT 67
#define UDP_DEFAULT_PORT 1234

// protocols
#define ARP_TYPE 0x0806

#endif