#include "arp.h"

namespace networking {
    ARP_cache_entry arp_cache[ARPcachelength];
	int ARP_cache_lastoverritten = 0;
    extern uint8_t broadcast_mac[];
}

using namespace networking;

// initialize cache and zero it out
void ARP::init(){
    bzero((void*) arp_cache, ARPcachelength * sizeof(ARP_cache_entry));
}

// insert into the arp cache
// false would mean the cache is full and nothing was updated

bool ARP::insert_arp_cache(ARPFrame* frame, ARP_IPV4Payload* data, bool override){
    
    ARP_cache_entry* entry;

    for (int i = 0; i < ARPcachelength; i++){
        entry = &arp_cache[i];

        // if it's free, take it
        if (entry -> state == cachefree){
            entry -> state = cachetaken;
            entry -> hardwareType = frame -> linkType;
            entry -> senderIP = data -> senderIP;
            // copy the sender mac from the incoming data to the entry
            memcpy((void*) entry -> senderMAC, (void*) data -> senderMAC, sizeof(entry -> senderMAC));
            return true;
        }
        // we already have it in the cache, but this is an update
        else if (entry -> state == cachetaken && entry -> hardwareType == frame -> linkType && entry -> senderIP == data -> senderIP){
            memcpy((void*) entry -> senderMAC, (void*) data -> senderMAC, sizeof(entry -> senderMAC));
            return true;
        }
    }
    // we were the target, so don't care if it was full just change the entries
    if (override){
        if (ARP_cache_lastoverritten >= ARPcachelength){
            ARP_cache_lastoverritten = 0;
        }
        entry = &arp_cache[ARP_cache_lastoverritten];

        entry -> state = cachetaken;
        entry -> hardwareType = frame -> linkType;
        entry -> senderIP = data -> senderIP;
        // copy the sender mac from the incoming data to the entry
        memcpy((void*) entry -> senderMAC, (void*) data -> senderMAC, sizeof(entry -> senderMAC));
        ARP_cache_lastoverritten++;
        return true;
    }

    // our cache is full... what do we want to do?
    // naive is just to return and let it go, could implement more efficient ways
    // such as ARP aging, based on time
    return false;
}

// try and get a mac address from the cache
// nullptr would mean it isn't in the table and we need to request it

bool ARP::get_MAC_arp(uint16_t targetHWType, uint32_t targetIP, unsigned char* mac){
    
    if (mac == nullptr) return false;
    struct ARP_cache_entry* entry;

    for (int i = 0; i < ARPcachelength; i++){
        entry = &arp_cache[i];

        // if it's taken and it is the same hwtype + ip, we have it
        if (entry -> state == cachetaken && entry -> hardwareType == targetHWType && entry -> senderIP == targetIP){
            memcpy((void*) mac, (void*) entry -> senderMAC, sizeof(entry -> senderMAC));
            return true;
        }

    }

    return false;
}

// arp request
void ARP::arp_request(uint8_t req_ivp4_addr[]) {
    arp payload;
    payload.htype = htons(ARP_ETHERNET);
    payload.ptype = htons(ARP_IPV4);
    payload.hlen = MAC_LEN;
    payload.plen = IPV4_LEN;
    payload.opcode = htons(ARP_REQUEST);
    memcpy(payload.srchw, mac_addr, MAC_LEN);
    memcpy(payload.srcpr, ipv4_addr, IPV4_LEN);
    bzero(payload.dsthw, MAC_LEN);
    memcpy(payload.dstpr, req_ivp4_addr, IPV4_LEN);

    Ethernet::sendPacket(ARP_TYPE, broadcast_mac, &payload, sizeof(arp));
}

// arp reply
// all we need to do is put our own mac address and flip sender and reciever data
void ARP::arp_reply(ARPFrame* frame, ARP_IPV4Payload* data){
    frame -> ARPtype = htons(ARP_REPLY);
    frame -> linkType = htons(frame -> linkType);
    frame -> protocolType = htons(frame -> protocolType);

    memcpy((void*)data -> recieverMAC, (void*) data -> senderMAC, 6);
    data -> recieverIP = htons(data -> senderIP);
    memcpy((void*)data -> senderMAC, (void*) mac_addr, 6);
    // TODO:set sender IP

    // this may send more bytes than normal, check this.
    Ethernet::sendPacket(ARP_TYPE, (uint8_t*) data -> recieverMAC, frame, sizeof(struct ARPFrame) + sizeof(struct ARP_IPV4Payload));
}

// recieving an arp packet
int ARP::receivePacket(uint32_t* in, uint32_t len){

    // if there is a problem with ARP, it is most likely here.
    // I have no idea if the right way to create a struct is to mask the pointer to the in, but let's do it anyways.
    struct ARPFrame* frame = (struct ARPFrame*) in;

    // might need to do some bit stuff here bc of endianness
    frame -> linkType = ntohs(frame -> linkType);
    frame -> protocolType = ntohs(frame -> protocolType);
    frame -> ARPtype = ntohs(frame -> ARPtype);

    // check hardware type, make sure it's ethernet
    if (frame -> linkType != ARP_ETHERNET){
        return -1;
    }
    // check protocol type, make sure it's IPv4
    if (frame -> protocolType != ARP_IPV4){
        return -1;
    }


    struct ARP_IPV4Payload* data = (struct ARP_IPV4Payload*) frame -> payload;
    data -> senderIP = ntohl(data -> senderIP);
    data -> recieverIP = ntohl(data -> recieverIP);
    // TODO:need to check our own ip vs the ip requested
    // could be a targeted request or a targeted reply, either way we need to save
    // only time we don't is if it's a non-targeted request
    bool override = false;
    // cache it
    insert_arp_cache(frame, data, override);

    // if it's a request, we need to reply
    if (frame -> ARPtype == ARP_REQUEST){
        arp_reply(frame, data);
    }

    return 0;
}