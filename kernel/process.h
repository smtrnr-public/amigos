#ifndef _process_h_
#define _process_h_

#include "shared.h"
#include "pcb.h"
#include "future.h"
#include "ext2.h"
#include "descriptor.h"
#include "vmm.h"
#include "io.h"
#include "u8250.h"
#include "socket.h"

struct Process : public PCB {
    static constexpr uint32_t FDT_SIZE = 10;
    static constexpr uint32_t PDT_SIZE = 10;
    static constexpr uint32_t SDT_SIZE = 10;
    // static constexpr uint32_t SOT_SIZE = 10; // Num Socket Decriptors - Networking Team
    
    Shared<Future<int>> exit_status;
    Shared<Ext2> fs;
    Shared<Node> cd;
    Shared<OutputStream<char>> io;
    Shared<Descriptor::FD> fdt[FDT_SIZE];
    Shared<Descriptor::PD> pdt[PDT_SIZE];
    Shared<Descriptor::SD> sdt[SDT_SIZE];
	// Shared<Descriptor::SO> sot[SOT_SIZE]; // Socket Descriptors - Networking Team

    Process(Shared<Ext2> fs) : PCB(VMM::copy_kpd()), exit_status(new Future<int>()), fs(fs), cd(fs->root), io(new U8250()) {
		using namespace Descriptor;
		fdt[0] = Shared<FD>{new StdIn{}};
		fdt[1] = Shared<FD>{new StdOut{io}};
		fdt[2] = Shared<FD>{new StdErr{io}};	
		for (uint32_t i = 3; i < FDT_SIZE; i++) { fdt[i] = FD::empty; }
		for (uint32_t i = 0; i < PDT_SIZE; i++) { pdt[i] = PD::empty; }
		for (uint32_t i = 0; i < SDT_SIZE; i++) { sdt[i] = SD::empty; }
		// for (uint32_t i = 0; i < SOT_SIZE; i++) { sot[i] = SO::empty; }
    }

    ~Process() {
		ASSERT(cr3 != (uint32_t) VMM::kpd);
		VMM::destroy_pd(cr3);
	}

#define GEN(n1, n2)					\
    Shared<Descriptor::n2> get_##n1(uint32_t des) {	\
	using namespace Descriptor;			\
	if ((des & TYPE_MASK) != TYPE_##n2)		\
	    return n2::empty;				\
	uint32_t index = des & NUM_MASK;		\
	if (index >= n2##T_SIZE)			\
	    return n2::empty;				\
	return n1##t[index];				\
    }							\
							\
    template<typename F>				\
    int set_##n1(F fun) {				\
	using namespace Descriptor;			\
	for (uint32_t i = 0; i < n2##T_SIZE; i++)	\
	    if (n1##t[i] == n2::empty) {		\
		n1##t[i] = fun();			\
		return i | TYPE_##n2;			\
	    }						\
	return -1;					\
    }

    GEN(fd, FD);
    GEN(pd, PD);
    GEN(sd, SD);
	// GEN(so, SO);
#undef GEN    
    
    int close(uint32_t); 
    int wait(uint32_t, uint32_t*);    
    
    Process* process() override {
	return this;
    }    

    void reset();
    Shared<PCB> clone();

	// see if the socket matches an existing mac address
	// int find_socket(m_addr* arrived) {
	// 	// Debug::printf("arrived: %x\n", arrived);
	// 	// Debug::printf("%s\n", Descriptor::SO::empty == Descriptor::SO::empty ? "true" : "false");
	// 	if (sot == nullptr) {
	// 		Debug::printf("sot null kslsjalkjfl;kjALKJFDLAKJDFLKAJDL:FKJ\n");
	// 		return -1;
	// 	}
	// 	// Debug::printf("find_socket\n");
	// 	for (uint32_t i = 0; i < SOT_SIZE; i++) {
	// 		// Debug::printf("%s\n", sot[i] == Descriptor::SO::empty ? "empty" : "not empty");
	// 		if (sot[i] != Descriptor::SO::empty && sot[i]->mac_match(arrived) == true){
	// 			// Debug::printf("found at index %d\n", i);
	// 			return i;
	// 		}
	// 	}
	// 	// Debug::printf("didn't find socket :(\n");
	// 	return -1;
	// }

	// int find_none_socket() {
	// 	uint8_t zero_mac[MAC_LEN] = {0,0,0,0,0,0};
	// 	for (uint32_t i = 0; i < SOT_SIZE; i++) {
	// 		if (sot[i]->mac_match((m_addr*) zero_mac) == true){
	// 			return i;
	// 		}
	// 	}
	// 	return -1;
	// }

	// int add_to_socket(m_addr* arrived, void* data, uint32_t data_length) {
	// 	// Debug::printf("add_to_socket\n");
	// 	// uint8_t* arr = arrived->smac_addr;
	// 	// Debug::printf("arr: %02x:%02x:%02x:%02x:%02x:%02x\n", arr[0], arr[1], arr[2], arr[3], arr[4], arr[5]);
	// 	// Debug::printf("data_length: %u\n", data_length);
	// 	// Debug::printf("data: %x\n", data);
	// 	// for (int i = 0; i < 18; i++) {
	// 	// 	Debug::printf("%x", ((char*)(data))[i]);
	// 	// }
	// 	// Debug::printf("\n");
	// 	int sock_index = find_socket(arrived);
	// 	// Debug::printf("sock_index: %d\n", sock_index);
	// 	if (sock_index == -1) {
	// 		// check to see if data matches magic number?
	// 		if ( *((uint32_t*) data) == (uint32_t) 501) {
	// 			// create a new socket descriptor cuz new connection
	// 			/*for (uint32_t i = 0; i < SOT_SIZE; i++) {
	// 				if (sot[i] == Descriptor::SO::empty) {
	// 					// get the arguments in the data but since they don't matter,
	// 					// i'll leave them with all 0s for now
	// 					set_so([=] { return Shared<Descriptor::SO>{new SocketDescriptor{0, 0, 0}}; });
	// 				}
	// 			}*/
	// 			int none_sock_index = find_none_socket();
	// 			if (none_sock_index != -1) {
	// 				sockaddr_mac s_addr;
	// 				s_addr.smac_addr = *arrived;
	// 				sot[none_sock_index]->bind((const sockaddr*) &s_addr, 0);
	// 			} else {
	// 				return -1; //return -1 if not enough room
	// 			}
	// 		}
	// 		else {
	// 			return -1; // magic number ain't here fam
	// 		}
	// 	}
	// 	else {
	// 		// yay we found the socket!
	// 		// Debug::printf("found socket\n");
	// 		sot[sock_index]->append_data(data, data_length);
	// 	}
	// 	// success!
	// 	return 0;
	// }

	// int read_from_socket(void* buf, uint32_t len, m_addr* source_mac) {
	// 	int sock_index = find_socket(source_mac);
	// 	if (sock_index == -1) {
	// 		return -1;
	// 	}
	// 	// yay! found the right socket
	// 	return sot[sock_index]->read_data(buf, len);
	// }
    
private:
    Process(const Process& p) :
	PCB(VMM::copy_pd((uint32_t*) p.cr3)),
	exit_status(new Future<int>),
	fs(p.fs),
	cd(p.cd),
	io(p.io),
	fdt(p.fdt),
	sdt(p.sdt) {
	for (uint32_t i = 0; i < PDT_SIZE; i++) { pdt[i] = Descriptor::PD::empty; }
    }
};

#endif
