#include "network.h"
#include "debug.h"
#include "heap.h"
#include "u8250.h"
#include "libk.h"
#include "machine.h"
#include "protocols.h"
#include "config.h"
#include "smp.h"
#include "semaphore.h"
#include "arp.h"
#include "net_consts.h"

namespace networking {
    SpinLock interruptLock{};
    PCIHeader0 head;
    uint16_t bus;
    uint8_t device;

    uint32_t memaddr;
    uint32_t ioaddr;

    uint32_t counter = 0;

    uint8_t mac_addr[MAC_LEN];
    uint8_t ipv4_addr[IPV4_LEN] = {10,0,2,15};

    uint8_t broadcast_mac[MAC_LEN] = {0xff,0xff,0xff,0xff,0xff,0xff};

    uint32_t curr_packet = 0;

    uint32_t rx_buffer;
    uint32_t tx_buffer;

    // packet header for reading packets
    struct R_Packet_Header {
        int MAR:1; // bit 0
        int PAM:1; // bit 1, etc
        int BAR:1;
        int reserved:7;
        int ISE:1;
        int RUNT:1;
        int LONG:1;
        int CRC:1;
        int FAE:1;
        int ROK:1;
    };
}

extern "C" void RTL8139_handler() {
    using namespace networking;
    uint16_t status = Ports::inportw(networking::ioaddr + 0x3E);

    auto pcb = gheith::current()->pcb;
    auto proc = pcb->process();
    if( proc == 0) {
        return;
    }
    interruptLock.lock();
    // Debug::printf("IN RTL8139_handler\n");
    

    Ports::outportw(networking::ioaddr + 0x3E, 0x5);

    if (status & 0x1) {
        // Debug::printf("Packet received\n");
        RTL8139::receivePacket();
        
        // TODO: increment an offset in the rx_buffer so we get can receive multiple packets
        //uint16_t packet_size = *(uint16_t*)(rx_buffer + sizeof(uint16_t));
        //uint32_t offset = 2 * sizeof(uint16_t) + ETHERNET_HEADER_SIZE;
        //for (unsigned i = offset; i < packet_size; i++) {
        //    Debug::printf("%c", *(char*)(rx_buffer + i));
        //}
        //Debug::printf("\n");
    } 
    if (status & 0x4) {
        // Debug::printf("Packet sent\n");
    }

    SMP::eoi_reg.set(0);
    interruptLock.unlock();
}

uint32_t read_ioapic_register(const uint8_t offset) {
    /* tell IOREGSEL where we want to read from */
    *(volatile uint32_t*)(kConfig.ioAPIC) = offset;
    /* return the data from IOWIN */
    return *(volatile uint32_t*)(kConfig.ioAPIC + 0x10);
}

void write_ioapic_register(const uint8_t offset, uint32_t val) {
    /* tell IOREGSEL where we want to read from */
    *(volatile uint32_t*)(kConfig.ioAPIC) = offset;
    /* write the data to IOWIN */
    *(volatile uint32_t*)(kConfig.ioAPIC + 0x10) = val;
}

void RTL8139::init() {
    using namespace networking;

    uint32_t busdev = PCI::discoverRTL8139(&(networking::head));

    bus    = busdev >> 8;
    device = busdev & 0xFF;

    Debug::printf("| RTL8139 found at PCI bus %u, slot %u\n", bus, device);

    if ((head.command & 0x4) == 0) { // if bus mastery isn't on, we must fix that

        PCI::pciConfigWriteWord(bus, device, 0, 4, head.command | 0x4);

    }

    // discover memaddr and ioaddr
    uint32_t mem_idx;
    uint32_t io_idx;

    for (int i = 0; i < 6; i++) {
        if (head.BAR[i]) {
            if ((head.BAR[i] & 0x1) == 0) {
                mem_idx = i;
            } else {
                io_idx = i;
            }
        }
    }

    // gets everything but the last 4 bits
    memaddr = head.BAR[mem_idx] & (0xFFFFFFF0);
    // gets everything but the last 2 bits
    ioaddr  = head.BAR[io_idx]  & (0xFFFFFFFC);
    networking::ioaddr = ioaddr;


    // power on the device
    Ports::outportb( ioaddr + 0x52, 0x0);

    // software reset: clears out buffers and sets things to default
    Ports::outportb( ioaddr + 0x37, 0x10);
    while( (inb(ioaddr + 0x37) & 0x10) != 0) { }


    // initialize receive buffer
    tx_buffer = (uint32_t) malloc(8192 + 16);
    rx_buffer = (uint32_t) malloc(RX_BUFFER_SIZE);
    bzero((void*) rx_buffer, RX_BUFFER_SIZE);
    Ports::outportl(ioaddr + 0x30, (uintptr_t)rx_buffer); // send uint32_t memory location to RBSTART (0x30)

    // set IMR and ISR
    Ports::outportw(ioaddr + 0x3C, 0x0005); // Sets the TOK and ROK bits high

    // configure RCR
    // this will only accept broadcast/multicast packets and packets addressed to this card's MAC
    Ports::outportl(ioaddr + 0x44, 0xe | (1 << 7));
    // this will accept all packets
    //Ports::outportl(ioaddr + 0x44, 0xf | (1 << 7));

    // enable receiver and transmitter
    Ports::outportb(ioaddr + 0x37, 0x0C);


    // populate ioAPIC redirection entry for RTL8139
    uint32_t entry_offset = 0x10 + RTL8139_ioAPIC_entry * 2;
    write_ioapic_register(entry_offset, RTL8139_vector);
    // write_ioapic_register(entry_offset + 1, 0x00000000); // interrupts sent only to core 0
    write_ioapic_register(entry_offset + 1, 0xFF000000);

    // register ioAPIC in IDT
    IDT::interrupt(RTL8139_vector, (uint32_t) RTL8139_handler_);


    // initialize and print MAC address
    init_mac_addr();
    Debug::printf("| MAC address ");
    for (int i = 0; i < 5; i++) {
        Debug::printf("%02x:", mac_addr[i]);
    }
    Debug::printf("%02x\n", mac_addr[5]);

    // initialize arp
    ARP::init();
}

void RTL8139::init_mac_addr() {
    using namespace networking;
    uint32_t mac = Ports::inportl(ioaddr);

    mac_addr[0] = (uint8_t)  mac;
    mac_addr[1] = (uint8_t) (mac >> 8);
    mac_addr[2] = (uint8_t) (mac >> 16);
    mac_addr[3] = (uint8_t) (mac >> 24);

    mac = Ports::inportw(ioaddr + 0x4);

    mac_addr[4] = (uint8_t)  mac;
    mac_addr[5] = (uint8_t) (mac >> 8);


}

void RTL8139::sendPacket(uint32_t addr, uint32_t bytes) {
    using namespace networking;
    Ports::outportl(ioaddr + 0x20 + (counter << 2), addr);
    Ports::outportl(ioaddr + 0x10 + (counter << 2), bytes);

    counter = (counter + 1) % 4;
}

// sends a payload pointed to by <payload> of size <bytes> to <dest_mac>
void RTL8139::sendPayloadTo(uint16_t protocol, uint8_t* dest_mac, void* payload, uint32_t bytes) {
    using namespace networking;
    // TODO: add checksum

    uint32_t packet_size = ETHERNET_HEADER_SIZE + bytes;
    char* packet = (char*) malloc(packet_size);

    memcpy(packet, dest_mac, MAC_LEN); // destination MAC
    memcpy(packet + MAC_LEN, mac_addr, MAC_LEN); // source MAC
    *((uint16_t*)(packet + ETHERNET_HEADER_SIZE - sizeof(uint16_t))) = htons(protocol); // protocol (none)
    memcpy(packet + ETHERNET_HEADER_SIZE, payload, bytes); // payload

    sendPacket((uint32_t) packet, packet_size);
    free(packet);
}

void RTL8139::broadcastPayload(uint16_t protocol, void* payload, uint32_t bytes) {
    sendPayloadTo(protocol, broadcast_mac, payload, bytes);
}


void RTL8139::receivePacket() {
    using namespace networking;
    // Debug::printf("HERE\n");
    uint32_t start_of_packet = rx_buffer + curr_packet;
    
    uint16_t* newPacket = (uint16_t*) (start_of_packet);
    // Debug::printf("newPacket header: %d\n", *newPacket);
    if (*newPacket == 0) {
        return;
    }

    // header, length, packet
    //R_Packet_Header* header = (R_Packet_Header*)(start_of_packet);
    uint16_t* length_ptr = (uint16_t*)(start_of_packet);

    // *(start_of_packet + 1) = length of the packet?
    uint16_t length_of_packet = *(length_ptr + 1);

    void* packet = malloc(length_of_packet);
    memcpy(packet, (void*)(length_ptr + 2), length_of_packet);

    // Debug::printf("len() = %d\n", length_of_packet);
    // Debug::printf("curr 0x%x\n", curr_packet);

    //PROTOCOL PART
    Ethernet::receivePacket(packet, length_of_packet - 4);

    // i looked at two different sources and they had this. i dunno why we dont add 32 in addition to 7 but it's fine
    // ik it's 4 byte aligned but i dunno why you don't account for the header + length fields (aka 32)
    curr_packet = (curr_packet + length_of_packet + 7) & ~3;

    // oh no! curr_packet length has exceeded. time to make it a ring buffer
    curr_packet = curr_packet % 8192;
    // Debug::printf("curr_packet: %d\n", curr_packet);

    // change the CAPR register
    Ports::outportw(ioaddr + 0x38, curr_packet - 16);
    start_of_packet = rx_buffer + curr_packet - 16;

    newPacket = (uint16_t*) (start_of_packet);
    // Debug::printf("newPacket header: %d\n", *newPacket);
    if (*newPacket) {
        RTL8139::receivePacket();
    }
}