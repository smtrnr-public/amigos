#include "socket.h"
#include "threads.h"
#include "process.h"


Socket** sot;

uint8_t getByte(char* text) {
    uint8_t res = 0;
    if ('0' <= text[0] && text[0] <= '9') {
        res = text[0] - 0x30;
    } else if ('A' <= text[0] && text[0] <= 'F') {
        res = text[0] - 0x37;
    } else if ('a' <= text[0] && text[0] <= 'f') {
        res = text[0] - 0x57;
    } else {
        Debug::printf("invalid MAC address\n");
    }
    res <<= 4;
    if ('0' <= text[1] && text[1] <= '9') {
        res |= text[1] - 0x30;
    } else if ('A' <= text[1] && text[1] <= 'F') {
        res |= text[1] - 0x37;
    } else if ('a' <= text[1] && text[1] <= 'f') {
        res |= text[1] - 0x57;
    } else {
        Debug::printf("invalid MAC address\n");
    }
    return res;
}

sockaddr_mac* textToMac(char* text) {
    auto res = new sockaddr_mac;
    int i = 0;
    int j = 0;
    while (i < MAC_LEN) {
        res->smac_addr.smac_addr[i] = getByte(text + j);
        i++;
        j+=2;
        if (text[j] == ':') {
            j++;
        } else if (i != MAC_LEN && !text[j]) {
            Debug::printf("invalid MAC address\n");
        }
    }
    if (text[j]) {
        uint16_t port = getByte(text + j);
        port <<= 8;
        port |= getByte(text + j + 2);
        res->smac_port = port;
    } else {
        res->smac_port = 0;
    }
    return res;
}

Socket::Socket(int domain, int type, int protocol) : dataLock(1) {
    data = malloc(8192);
    start_of_avail = data;
    offset = 0;
    bytes_avail = 0;
}

int set_so(int domain, int type, int protocol) {
    for (int i = 0; i < SOT_SIZE; i++) {
        if (sot[i] == nullptr) {
            sot[i] = new Socket(domain, type, protocol);
            return i;
        }
    }
    return -1;
}

Socket* get_so(int sockfd) {
    return sot[sockfd];
}



void sock_init() {
    // Debug::printf("mallocing %d\n", SOT_SIZE * sizeof(Socket*));
    sot = (Socket**) malloc(SOT_SIZE * sizeof(Socket*));
    // Debug::printf("sot: %x\n", sot);
    for (uint32_t i = 0; i < SOT_SIZE; i++) { 
        sot[i] = nullptr; 
    }
    // Debug::printf("Done Socket init\n");
}


int find_socket(m_addr* arrived) {
    // Debug::printf("arrived: %x\n", arrived);
    // Debug::printf("%s\n", Descriptor::SO::empty == Descriptor::SO::empty ? "true" : "false");
    if (sot == nullptr) {
        Debug::printf("sot null kslsjalkjfl;kjALKJFDLAKJDFLKAJDL:FKJ\n");
        return -1;
    }
    // Debug::printf("find_socket\n");
    for (uint32_t i = 0; i < SOT_SIZE; i++) {
        // Debug::printf("%s\n", sot[i] == Descriptor::SO::empty ? "empty" : "not empty");
        if (sot[i] && sot[i]->mac_match(arrived) == true){
            // Debug::printf("found at index %d\n", i);
            return i;
        }
    }
    // Debug::printf("didn't find socket :(\n");
    return -1;
}

int find_none_socket() {
    uint8_t zero_mac[MAC_LEN] = {0,0,0,0,0,0};
    for (uint32_t i = 0; i < SOT_SIZE; i++) {
        if (sot[i]->mac_match((m_addr*) zero_mac) == true){
            return i;
        }
    }
    return -1;
}

int add_to_socket(m_addr* arrived, void* data, uint32_t data_length) {
    // Debug::printf("add_to_socket\n");
    // uint8_t* arr = arrived->smac_addr;
    // Debug::printf("arr: %02x:%02x:%02x:%02x:%02x:%02x\n", arr[0], arr[1], arr[2], arr[3], arr[4], arr[5]);
    // Debug::printf("data_length: %u\n", data_length);
    // Debug::printf("data: %x\n", data);
    // for (int i = 0; i < 18; i++) {
    // 	Debug::printf("%x", ((char*)(data))[i]);
    // }
    // Debug::printf("\n");
    int sock_index = find_socket(arrived);
    // Debug::printf("sock_index: %d\n", sock_index);
    if (sock_index == -1) {
        // check to see if data matches magic number?
        if ( *((uint32_t*) data) == (uint32_t) 501) {
            // create a new socket descriptor cuz new connection
            /*for (uint32_t i = 0; i < SOT_SIZE; i++) {
                if (sot[i] == Descriptor::SO::empty) {
                    // get the arguments in the data but since they don't matter,
                    // i'll leave them with all 0s for now
                    set_so([=] { return Shared<Descriptor::SO>{new SocketDescriptor{0, 0, 0}}; });
                }
            }*/
            int none_sock_index = find_none_socket();
            if (none_sock_index != -1) {
                sockaddr_mac s_addr;
                s_addr.smac_addr = *arrived;
                sot[none_sock_index]->bind((const sockaddr*) &s_addr, 0);
            } else {
                return -1; //return -1 if not enough room
            }
        }
        else {
            return -1; // magic number ain't here fam
        }
    }
    else {
        // yay we found the socket!
        // Debug::printf("found socket\n");
        sot[sock_index]->append_data(data, data_length);
    }
    // success!
    return 0;
}

int read_from_socket(void* buf, uint32_t len, m_addr* source_mac) {
    int sock_index = find_socket(source_mac);
    if (sock_index == -1) {
        return -1;
    }
    // yay! found the right socket
    return sot[sock_index]->read_data(buf, len);
}





// helper method to see if two mac addresses are the same
bool Socket::mac_match(m_addr* other) {
    // Debug::printf("mac_match\n");
    for (int i = 0; i < MAC_LEN; i++) {
        if (other->smac_addr[i] != addr.smac_addr[i]) {
            //Debug::printf("%u != %u\n", other->smac_addr[i], addr.smac_addr[i]);
            return false;
        }
    }
    return true;
}

void Socket::append_data(void* data, uint32_t data_length) {
    dataLock.down();
    // Debug::printf("append_data: dl %u\n", data_length);
    uint32_t adjusted_start = ((uint32_t)(this->data)) + offset;
    //TODO: account for wrapping with mod?
    memcpy((void*)adjusted_start, data, data_length);
    bytes_avail += data_length;
    // Debug::printf("bytes_avail: %u\n", bytes_avail);
    offset += data_length;
    //if (bytes_avail >= needed) {
        //receive_sem.up();
    //}
    dataLock.up();
}

int Socket::read_data(void* dest, uint32_t data_length) {
    // Debug::printf("read_data\n");
    RTL8139::receivePacket();
    dataLock.down();
    if (bytes_avail < data_length) {
        needed = data_length;
        //receive_sem.down();
        // Debug::printf("Interrupts disabled? %d\n", Interrupts::isDisabled());
        while (bytes_avail < data_length) {
            dataLock.up();
            pause();
            dataLock.down();
            //Debug::printf("ba: %u, dl: %u\n", bytes_avail, data_length);
        }
    }

    // Debug::printf("ba: %u, dl: %u\n", bytes_avail, data_length);
    memcpy(dest, start_of_avail, data_length);
    bzero(start_of_avail, data_length);
    // Debug::printf("read_data: %x\n", dest);
    // Debug::printf("start_of_avail: %p\n", start_of_avail);
    //TODO: account for wrapping with mod?
    start_of_avail = (void*) (((uint32_t)start_of_avail) + data_length);
    bytes_avail -= data_length;
    dataLock.up();
    return data_length;
}

int Socket::bind(const sockaddr* addr, size_t addrlen) {
    if (addr == nullptr) return -1;
    sockaddr_mac* addr_mac = (sockaddr_mac*) addr;
    this->addr = addr_mac->smac_addr;
    this->port = addr_mac->smac_port;
    return 0;
}

int Socket::listen(int backlog) {
    return 0;
}
int Socket::connect(const sockaddr *addr, size_t addrlen) {
    // we don't support connetion-based sockets, just bind
    return bind(addr, addrlen);
}
int Socket::accept(sockaddr* addr, size_t* addrlen) {
    return -1;
}
ssize_t Socket::recvfrom(void* buf, size_t len, int flags, sockaddr* src_addr, size_t* addrlen) {
    if (src_addr != nullptr) {
        sockaddr_mac* src_addr_mac = (sockaddr_mac*) src_addr;
        src_addr_mac->smac_addr = this->addr;
        src_addr_mac->smac_port = this->port;
    }

    return read_from_socket(buf, len, &this->addr);
}

ssize_t Socket::sendto(const void* buf, size_t len, int flags,
                const sockaddr* dest_addr, size_t addrlen) {
    if (dest_addr == nullptr) return -1;

    uint8_t* dest_mac = (uint8_t*) &((sockaddr_mac*)dest_addr)->smac_addr;
    //AOSP::sendPacket((void*) buf, len, dest_mac);
    // Debug::printf("%02x:%02x:%02x:%02x:%02x:%02x\n", dest_mac[0], dest_mac[1], dest_mac[2], dest_mac[3], dest_mac[4], dest_mac[5]);
    // Debug::printf("bytes: %u\n", len);
    // Ethernet::sendPacket(0x0000, dest_mac, (void*) buf, len);
    return AOSP::sendPacket((void*) buf, len, dest_mac);
    // int timeWaster = 0;
    // int value = 0;
    // while(timeWaster < 250) {
    //     value++;
    //     if (value == 1) {
    //     // pause();
    //         timeWaster++;
    //         // Debug::printf("%d\n", value);
    //     }
    // }

}
int Socket::shutdown(int how) {
    return -1;
}